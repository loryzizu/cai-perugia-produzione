CREATE TABLE IF NOT EXISTS `#__mi_iptable` (
  `ip` bigint(20) NOT NULL COMMENT 'ip to long',
  `firsthacktime` datetime NOT NULL,
  `lasthacktime` datetime NOT NULL,
  `hackcount` int(11) NOT NULL DEFAULT '1',
  `autodelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ip`)
);