function setOptions(str, selectId) {
	var select = document.getElementById(selectId);
	if (str.length < 3) {
		select.innerHTML = "";
		return;
	} else {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				select.innerHTML = "";
				select.innerHTML = select.innerHTML + xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET",
				"index.php?option=com_cai&task=ajax.listaUtenti&format=raw&text="
						+ str, true);
		xmlhttp.send();
	}
}

function replaceQuotesCalendarTitle(){
	//replace &#39; with ' in calendar titles
	var calendarTitles = document.getElementsByClassName("fc-title");
	for (i = 0; i < calendarTitles.length; i++) { 
		calendarTitles[i].innerHTML=calendarTitles[i].innerHTML.replace("&amp;#39;","'");
	}
}