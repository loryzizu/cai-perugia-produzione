<?php
// No direct access
defined ( '_JEXEC' ) or die ();
?>

<?php if($renderForm){?>

<table class="tabIntro" >
	<tr>
		<td style="padding-left: 23px">
<h1>Login</h1>
<form method="post">
	<input type="text" name="username" placeholder="Username" style="width:95%"/> <br/><br/><input type="password"
		name="password" placeholder="Password" style="width:95%"/> <input type="submit" name="submit" value="Invia"/> <input
		type="hidden" name="option" value="com_cai" /> <input type="hidden"
		name="task" value="login.login"  />
</form>
<br>
<a  href="index.php?option=com_cai&task=utente.displayRecuperoPassword">Non
	ricordi la tua username o password?</a>
</td></tr></table>
<?php
} else {
	?>
<h1>Ciao sei autenticato</h1>
<form>
	<input type="submit" name="submit" value="Logout" /> <input
		type="hidden" name="option" value="com_cai" /> <input type="hidden"
		name="task" value="login.logout" />
</form>

<div class="module_menu">
	<div>
		<div>
			<div>
				<h3>Menu Socio</h3>
				<ul>
					<li><a
						href="index.php?option=com_cai&view=profilo&stato=0&task=profilo.profilo">Profilo</a>
					</li>
					<li><a
						href="index.php?option=com_cai&view=proposta&task=proposta.display">Proponi
							attivit&agrave;</a></li>

					<li><a href="index.php?option=com_cai&view=profilo&stato=2&task=profilo.profilo">Attivit&agrave;
							a cui ho partecipato</a></li>
					<li><a href="index.php?option=com_cai&view=profilo&stato=3&task=profilo.profilo">Le mie
							prenotazioni</a></li>
					<li><a href="index.php?option=com_cai&view=profilo&stato=4&task=profilo.profilo">Le mie
							proposte di attivit&agrave;</a></li>

					<li><a href="index.php?option=com_cai&view=profilo&stato=1&task=profilo.profilo">Impostazioni</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php
	
if ($isSegretario=== true) {
		?>
	<div class="module_menu">
		<div>
			<div>
				<div>
					<h3>Menu Segretario</h3>
					<ul>
						<li><a
							href="index.php?option=com_cai&view=attivitaSvolteAdmin">Attivit&agrave; svolte</a>
						</li>
						<li><a
							href="index.php?option=com_cai&view=attivitaDaApprovareAdmin">Proposte da approvare</a></li>

						<li><a href="index.php?option=com_cai&view=attivitaApprovateAdmin">Proposte approvate</a></li>
						<li><a href="index.php?option=com_cai&view=rinnovoIscrizioniAdmin">Gestione utenti</a></li>
						<li><a href="index.php?option=com_cai&view=statisticheAdmin&task=statistiche.visualizza">Statistiche</a></li>
					</ul>
				</div>

			</div>
		</div>

	</div>

<?php
	}
}
?>
<br/>
<a href="index.php?option=com_users&view=login">Amministrazione</a>