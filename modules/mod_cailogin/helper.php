<?php

class ModCAILoginHelper
{
    public static function renderForm(){
    	$result=JRequest::getVar("result","false");
    	$session =JFactory::getSession();
    	$user_id=$session->get('user_id',null);
    	if(trim($result)=="true" || $user_id!=null){
    		return false;
    	}
    	return true;
    }
    
    public static function isSegretario(){
    	JModelLegacy::addIncludePath( JPATH_ROOT . '/components/com_cai/models' );
    	$model=JModelLegacy::getInstance( 'Utente', 'CAIModel' );
    	return $model->isSegretario();
    }

    public static function updateAttivitaSvolte(){
        JModelLegacy::addIncludePath( JPATH_ROOT . '/components/com_cai/models' );
        /**@var CAIModelProposta $proposta_model*/
        $proposta_model = JModelLegacy::getInstance( 'Proposta', 'CAIModel' );
        $proposta_model->updateAttivitaSvolte();
    }
}
?>