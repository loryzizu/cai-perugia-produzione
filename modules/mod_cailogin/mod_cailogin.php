<?php
// No direct access
defined('_JEXEC' ) or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';
JHtml::_('stylesheet', 'http://fonts.googleapis.com/css?family=Arvo');
JHtml::_('stylesheet', 'http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext');
JHtml::_('stylesheet', 'https://fonts.googleapis.com/icon?family=Material+Icons');
JHtml::_('stylesheet', JUri::root() . 'media/com_cai/css/style.css');
$renderForm= modCAILoginHelper::renderForm($params);
$isSegretario=modCAILoginHelper::isSegretario();
modCAILoginHelper::updateAttivitaSvolte();
require JModuleHelper::getLayoutPath('mod_cailogin', $params->get('layout', 'default'));