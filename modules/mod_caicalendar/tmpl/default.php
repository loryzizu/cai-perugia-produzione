<?php
// No direct access
defined ( '_JEXEC' ) or die ();


JHtml::_('jquery.framework', false);
JHtml::_('stylesheet', JUri::root() . 'media/com_cai/js/fullcalendar-2.4.0/fullcalendar.css', false, true, false);
JHtml::_('script', JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/lib/moment.min.js', false, true);
JHtml::_('script', JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/fullcalendar.min.js', false, true);
JHtml::_('script', JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/lang/it.js', false, true);
JHtml::_('script', JUri::base() .'media/com_cai/js/cai.js', false, true);
/*JHtml::stylesheet('http://fonts.googleapis.com/css?family=Arvo');
JHtml::stylesheet('http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext');
JHtml::stylesheet('https://fonts.googleapis.com/icon?family=Material+Icons');
JHtml::stylesheet(JURI::root(true) . 'media/com_cai/css/style.css');
JHtml::stylesheet(JUri::root() . 'media/com_cai/js/fullcalendar-2.4.0/fullcalendar.print.css');
JHtml::stylesheet(JUri::root() . 'media/com_cai/js/fullcalendar-2.4.0/fullcalendar.css');
JHtml::script(JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/lib/moment.min.js');
JHtml::script(JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/fullcalendar.min.js');
JHtml::script(JUri::base() .'media/com_cai/js/fullcalendar-2.4.0/lang/it.js');*/
?>




<script>
	
    jQuery(document).ready(function() {

    	jQuery('#calendarioAttivita').fullCalendar({
    		
             header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek'
                    
            },
            contentHeight: 800,
            aspectRatio:5.5,
            lang:'it',
            defaultDate: '<?php echo date("Y-m-d"); ?>',
            businessHours: true, // display business hours
            editable: true,
            eventLimit: true,
            eventColor: 'white',
            events:[
                <?php
                    foreach($eventi AS $evento){
                    	echo $evento." , ";
                    }
                ?>
            ]

        });

    	replaceQuotesCalendarTitle();
    });

</script>
<style>



    #calendar , #calendarioLegenda {
        max-width: 95%;

         margin-left: auto;
    	margin-right: auto;
    	left:100px;
    	position:relative;
    }
    #calendarioAttivita{
    	width:90%;
    	 margin-left: auto;
    	margin-right: auto;
    	position:relative;
    }
	.inlineDivLegenda{
		display:inline-block;
		width:30px;
		height:10px;
		margin:5px;
	}
	.inlineDivLegenda2{
		display:inline-block;

		margin:5px;
	}
	.nonInlineDivLegenda{
		display:block;
		width:30px;
		height:10px;
		margin:5px;
	}
</style>

    <div id="calendarioAttivita"></div>
    <div id="calendarioLegenda">
    	<div class="inlineDivLegenda2">
	    	<div class="inlineDivLegenda" style="background-color: grey;"></div> <span>Nulla</span>
	    	<br><div class="inlineDivLegenda" style="background-color: blue;"></div> <span>Alpinismo, Alp.Giovanile, SciAlpinismo</span>
	    	<br><div class="inlineDivLegenda" style="background-color: yellow;"></div> <span>Cultura, Eventi, CAI Perugia</span>
    	</div>

    	<div class="inlineDivLegenda2">
    		<div class="inlineDivLegenda" style="background-color: green;"></div> <span>Escursionismo, SciEscursionismo, CicloEscursionismo</span>
	    	<br><div class="inlineDivLegenda" style="background-color: red;"></div> <span>Speleologia, Arrampicata</span>
    	 	<br><div class="inlineDivLegenda" style="background-color: cyan;"></div> <span>Torrentismo</span>
    	 </div>
    </div>

