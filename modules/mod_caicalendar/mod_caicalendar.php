<?php
// No direct access
defined('_JEXEC' ) or die;

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$eventi=modCAICalendarHelper::getEventi();
$isSegretario=modCAICalendarHelper::isSegretario();
require(JModuleHelper::getLayoutPath('mod_caicalendar'));
?>