<?php

class ModCAICalendarHelper
{
	public static function getEventi(){
		JModelLegacy::addIncludePath( JPATH_ROOT . '/components/com_cai/models' );
		$model=JModelLegacy::getInstance( 'Calendario', 'CAIModel' );
		return $model->getEventi();
	}
    
    public static function isSegretario(){
    	JModelLegacy::addIncludePath( JPATH_ROOT . '/components/com_cai/models' );
    	$model=JModelLegacy::getInstance( 'Utente', 'CAIModel' );
    	return $model->isSegretario();
    }
}
?>