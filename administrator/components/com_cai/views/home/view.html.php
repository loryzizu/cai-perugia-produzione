<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewHome extends JViewLegacy{
	public $message1;
	public $message2;
	function display($tpl = null) {
		$this->message1=JRequest::getVar("message1", null);
		$this->message2=JRequest::getVar("message2", null);
		$this->addToolBar();
		parent::display($tpl);
	}

	protected function addToolBar()
	{
		JToolBarHelper::title("Joomla! Component del Club Alpino Italiano");
	}
}
