CREATE TABLE IF NOT EXISTS `#__cai_allegato` (
  `id_proposta` int(11) NOT NULL,
  `nome_allegato` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_familiare` (
  `id1` varchar(16) NOT NULL,
  `id2` varchar(16) NOT NULL,
  PRIMARY KEY (`id1`,`id2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_gruppo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `logo` varchar(250) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;



INSERT INTO `#__cai_gruppo` (`id`, `nome`, `catid`, `logo`, `description`) VALUES
(-1, 'Gruppi CAI', NULL, '', ''),
(0, 'Nessuna', NULL, 'media/com_cai/loghi/logo_cai.png', ''),
(1, 'Azimut', NULL, 'media/com_cai/loghi/logo_cai_azimut.png', ''),
(2, 'Amici di Manlio', NULL, 'media/com_cai/loghi/logo_cai_escursionismo.png', ''),
(3, 'Etruskanyoning', NULL, 'media/com_cai/loghi/logo_cai_etruskanyoning.png', ''),
(4, 'Baby CAI', NULL, 'media/com_cai/loghi/logo_cai_jup.png', ''),
(5, 'Tattaruledda', NULL, 'media/com_cai/loghi/logo_cai_escursionismo.png', ''),
(6, 'Seniores', NULL, 'media/com_cai/loghi/logo_cai_seniores.png', ''),
(7, 'GS Gruppo Speleologico', NULL, 'media/com_cai/loghi/logo_cai_speleo.png', ''),
(8, 'Fuori di roccia', NULL, 'media/com_cai/loghi/logo_cai_fuoridiroccia.png', ''),
(9, 'Rampichini', NULL, 'media/com_cai/loghi/logo_cai_rampichini.png', ''),
(10, 'I Rampanti', NULL, 'media/com_cai/loghi/logo_cai_escursionismo.png', '');


CREATE TABLE IF NOT EXISTS `#__cai_immagine` (
  `id_proposta` int(11) NOT NULL,
  `nome_immagine` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_iscrizioni` (
  `id_proposta` int(11) NOT NULL,
  `id_utente` int(11) NOT NULL,
  PRIMARY KEY (`id_proposta`,`id_utente`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_iscrizioni_non_socio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proposta` bigint(20) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `data_nascita` varchar(15) DEFAULT NULL,
  `cellulare` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cf` varchar(17) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `#__cai_iscrizioni_socio_esterno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proposta` bigint(20) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `data_nascita` varchar(15) DEFAULT NULL,
  `cellulare` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cf` varchar(17) DEFAULT NULL,
  `sezione` text,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1  ;

CREATE TABLE IF NOT EXISTS `#__cai_menu_segretario` (
  `id_modulo` int(11) NOT NULL,
  PRIMARY KEY (`id_modulo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_menu_socio` (
  `id_modulo` int(11) NOT NULL,
  PRIMARY KEY (`id_modulo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_proposta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ddg1` int(11) DEFAULT NULL,
  `ddg2` int(11) DEFAULT NULL,
  `ddg3` int(11) DEFAULT NULL,
  `ddg4` int(11) DEFAULT NULL,
  `ddg5` int(11) DEFAULT NULL,
  `tipologia` varchar(20) DEFAULT NULL,
  `gruppo` varchar(20) DEFAULT NULL,
  `titolo` varchar(80) DEFAULT NULL,
  `data_inizio` date DEFAULT NULL,
  `abilita_data_fine` tinyint(1) NOT NULL DEFAULT '0',
  `data_fine` date DEFAULT NULL,
  `appuntamento` mediumtext,
  `descrizione` mediumtext,
  `difficolta` mediumtext,
  `dislivello` mediumtext,
  `lunghezza` mediumtext,
  `indicazioni` mediumtext,
  `durata` mediumtext,
  `modalita` mediumtext,
  `costo` mediumtext,
  `contatti` mediumtext,
  `approfondimenti` mediumtext,
  `prenotazione` mediumtext,
  `id_articolo` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `data_proposta` date DEFAULT NULL,
  `stato` int(11) DEFAULT '0',
  `pubblicazione_homepage` varchar(10) NOT NULL DEFAULT '00/00/0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

INSERT INTO `iomhumec_cai_proposta` (`id`, `ddg1`, `ddg2`, `ddg3`, `ddg4`, `ddg5`, `tipologia`, `gruppo`, `titolo`, `data_inizio`, `abilita_data_fine`, `data_fine`, `appuntamento`, `descrizione`, `difficolta`, `dislivello`, `lunghezza`, `indicazioni`, `durata`, `modalita`, `costo`, `contatti`, `approfondimenti`, `prenotazione`, `id_articolo`, `data_proposta`, `stato`, `pubblicazione`) VALUES
(null, 1, NULL, NULL, NULL, NULL, '1', '1', 'Esempio attività di Alpinismo', '2015-09-01', 1, '2015-09-07', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', '721', NULL, 2, ''),
(null, 1, NULL, NULL, NULL, NULL, '3', '11', 'Esempio di evento culturale', '2015-09-03', 1, '2015-09-05', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', '722', NULL, 2, ''),
(null, 1, NULL, NULL, NULL, NULL, '5', '6', 'Esempio attività di Escursionismo', '2015-09-15', 1, '2015-09-17', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', '720', NULL, 4, ''),
(null, 1, NULL, NULL, NULL, NULL, '7', '6', 'Esempio di Arrampicata', '2015-09-07', 1, '2015-09-08', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', '724', NULL, 4, ''),
(null, 1, NULL, NULL, NULL, NULL, '2', '1', 'Esempio di attività non approvata', '2015-09-24', 1, '2015-09-27', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Questa attivit&agrave;, finch&egrave; non viene approvata e pubblicata, &egrave; visibile al solo amministratore.</strong></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, NULL, 0, '');



CREATE TABLE IF NOT EXISTS `#__cai_proposta_bkp` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ddg1` int(11) DEFAULT NULL,
  `ddg2` int(16) DEFAULT NULL,
  `ddg3` int(16) DEFAULT NULL,
  `ddg4` int(16) DEFAULT NULL,
  `ddg5` int(16) DEFAULT NULL,
  `tipologia` varchar(20) DEFAULT NULL,
  `gruppo` varchar(20) DEFAULT NULL,
  `titolo` varchar(80) DEFAULT NULL,
  `data_inizio` date DEFAULT NULL,
  `abilita_data_fine` tinyint(1) NOT NULL DEFAULT '0',
  `data_fine` date DEFAULT NULL,
  `appuntamento` varchar(300) DEFAULT NULL,
  `descrizione` varchar(10000) DEFAULT NULL,
  `difficolta` varchar(300) DEFAULT NULL,
  `dislivello` varchar(300) DEFAULT NULL,
  `lunghezza` varchar(1000) DEFAULT NULL,
  `indicazioni` varchar(2000) DEFAULT NULL,
  `durata` varchar(300) DEFAULT NULL,
  `modalita` varchar(300) DEFAULT NULL,
  `costo` varchar(300) DEFAULT NULL,
  `contatti` varchar(1000) DEFAULT NULL,
  `approfondimenti` varchar(1000) DEFAULT NULL,
  `prenotazione` varchar(3000) DEFAULT NULL,
  `data_proposta` date DEFAULT NULL,
  `pubblicazione_homepage` varchar(10) NOT NULL DEFAULT '00/00/0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

INSERT INTO `#__cai_proposta_bkp` (`id`, `ddg1`, `ddg2`, `ddg3`, `ddg4`, `ddg5`, `tipologia`, `gruppo`, `titolo`, `data_inizio`, `abilita_data_fine`, `data_fine`, `appuntamento`, `descrizione`, `difficolta`, `dislivello`, `lunghezza`, `indicazioni`, `durata`, `modalita`, `costo`, `contatti`, `approfondimenti`, `prenotazione`, `data_proposta`, `pubblicazione`) VALUES
(null, 1, NULL, NULL, NULL, NULL, '1', '1', 'Esempio attività di Alpinismo', '2015-09-01', 1, '2015-09-07', '', '<p><span style="font-size: 14pt;"><em><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></em></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, ''),
(null, 1, NULL, NULL, NULL, NULL, '3', '11', 'Esempio di evento culturale', '2015-09-03', 1, '2015-09-05', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, ''),
(null, 1, NULL, NULL, NULL, NULL, '5', '6', 'Esempio attività di Escursionismo', '2015-09-15', 1, '2015-09-17', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, ''),
(null, 1, NULL, NULL, NULL, NULL, '7', '6', 'Esempio di Arrampicata', '2015-09-07', 0, NULL, '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Buon divertimento!&nbsp;</strong></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, ''),
(null, 1, NULL, NULL, NULL, NULL, '2', '1', 'Esempio di attività non approvata', '2015-09-24', 1, '2015-09-27', '', '<p><span style="font-size: 14pt;"><strong>Questo articolo si riferisce ad un''attivit&agrave; di esempio per mostrare le nuove funzionalit&agrave; del nuovo sito. Questa attivit&agrave;, finch&egrave; non viene approvata e pubblicata, &egrave; visibile al solo amministratore.</strong></span></p>', '', '', '', '', '', '', '', '', '', '', NULL, '');


CREATE TABLE IF NOT EXISTS `#__cai_relazione_privata` (
  `id_proposta` int(11) NOT NULL,
  `relazione` text NOT NULL,
  PRIMARY KEY (`id_proposta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_relazione_pubblica` (
  `id_proposta` int(11) NOT NULL,
  `relazione` text NOT NULL,
  PRIMARY KEY (`id_proposta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__cai_segretario` (
  `id` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `#__cai_segretario` (`id`) VALUES
('1');


CREATE TABLE IF NOT EXISTS `#__cai_utente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CF` varchar(16) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `nome` tinytext NOT NULL,
  `cognome` tinytext NOT NULL,
  `titoli` text NOT NULL,
  `data_nascita` date NOT NULL,
  `telefono` tinytext NOT NULL,
  `cellulare` tinytext NOT NULL,
  `professione` tinytext NOT NULL,
  `note` text NOT NULL,
  `foto` tinytext NOT NULL,
  `stato` int(11) NOT NULL,
  `salt` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CF` (`CF`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

INSERT INTO `#__cai_utente` (`id`, `CF`, `username`, `password`, `email`, `nome`, `cognome`, `titoli`, `data_nascita`, `telefono`, `cellulare`, `professione`, `note`, `foto`, `stato`) VALUES
(1, 'CAIPERUGIA', 'cai.perugia', '$2y$10$nJxIOiXwCYY1yqXlXdVr7OoOZy1M59Sio24Ko8AE9yqP3XwMAixOG', 'caiperugia@gmail.com', 'Perugia', 'CAI', '', '1863-08-12', '075 573 0334', '', '', '', '', 1,);