<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelMenu extends JModelItem {
	public function crea(){
		//CREA MENU
		$idMenu1=$this->creaMenuTypes("menu-socio", "Menu Socio");
		if($idMenu1!==false){
			$idMenu2=$this->creaMenuTypes("menu-segretario", "Menu Segretario");
			if($idMenu1!==false){
				
				//CREA MODULI
				$creaModuloSocio=$this->creaModulo("Menu Socio", "menu-socio");
				if($creaModuloSocio!==false){
					$idMod1=$creaModuloSocio;
					$db=JFactory::getDbo();
					$db->truncateTable("#__cai_menu_socio");
					$obj= new stdClass();
					$obj->id_modulo=$idMod1;
					$db->insertObject('#__cai_menu_socio', $obj);
					$creaModuloAdmin=$this->creaModulo("Menu Segretario", "menu-segretario");
					if($creaModuloAdmin!==false){
						$idMod2=$creaModuloAdmin;
						$db->truncateTable("#__cai_menu_segretario");
						$obj= new stdClass();
						$obj->id_modulo=$idMod2;
						$db->insertObject('#__cai_menu_segretario', $obj);
						//CREA VOCI MENU
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/*private function creaVociMenuSocio(){
		//profilo, proponi, partecipato, proposte, prenotazioni, impostazioni
		JTable::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_menus/tables' );
		JModelLegacy::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_menus/models' );
		$mod_model=$this->getInstance( 'Menu', 'MenusModel' );
	
		$data= array(
				'id'=>NULL,
				'title'=>$title,
				'published'=>0,
				'module'=>'mod_menu',
				'access'=>1,
				'showtitle'=>1
		);
	
		if( !$mod_model->save( $data ) )
		{
			return false;
		}
		$db = JFactory::getDbo();
		$db->setQuery("SELECT MAX(id) FROM #__menu");
		return $db->loadResult();
	}
	
*/
	private function creaModulo($title, $menuType){
		JTable::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_modules/tables' );
		JModelLegacy::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_modules/models' );
		$mod_model=$this->getInstance( 'Module', 'ModulesModel' );

		$db=JFactory::getDbo();
		$db->setQuery("SELECT id FROM #__modules WHERE title='$title'");
		$id=$db->loadResult();
		if($id!==null){
			return $id;
		}

		$data= array(
				'id'=>NULL,
				'title'=>$title,
				'published'=>0,
				'module'=>'mod_menu',
				'access'=>1,
				'showtitle'=>1,
				'ordering'=>1,
				'params'=>'{"menutype":"'.$menuType.'",
				"startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"",
				"class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"_menu",
				"cache":"1","cache_time":"900","cachemode":"itemid"}'
		);

		if( !$mod_model->save( $data ) )
		{
			return false;
		}
		$db->setQuery("SELECT MAX(id) FROM #__modules ");
		return $db->loadResult();
	}

	private function creaMenuTypes($name, $title){
		$db=JFactory::getDbo();
		$db->setQuery("SELECT id FROM #__menu_types WHERE menutype='$name'");
		$id=$db->loadResult();
		if($id!==null){
			return $id;
		}

		$obj=new stdClass();
		$obj->id=null;
		$obj->menutype=$name;
		$obj->title=$title;
		$obj->description="";
		$db->insertObject("#__menu_types", $obj);
		$db->setQuery("SELECT id FROM #__menu_types WHERE menutype='$name'");
		$id=$db->loadResult();
		if($id!==null){
			return $id;
		}
		return false;
	}

	

}