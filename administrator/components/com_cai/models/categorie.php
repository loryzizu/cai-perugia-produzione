<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelCategorie extends JModelItem {
	
	
	
	
	
	
	public function crea(){
		$db=JFactory::getDbo();
		$db->setQuery("SELECT * FROM #__cai_gruppo WHERE id=-1 AND catid IS NULL");
		$parentGroup=$db->loadObject();
		$cnt=0;
		if($parentGroup!=null){
			$parent=$this->creaCategoriaParent($parentGroup);
			if($parent!==false){
				$parentGroup->catid=$parent->id;
				$db->updateObject("#__cai_gruppo", $parentGroup, 'id');
				$cnt++;
				$db->setQuery("SELECT * FROM #__cai_gruppo WHERE id != -1 AND catid IS NULL");
				$gruppi=$db->loadObjectList();
				if($gruppi!=null){
					foreach ($gruppi AS $gruppo){
						if($gruppo->catid == null){
							$id=$this->creaCategoria($gruppo, $parent->id , 2, $parent->alias );
							if($id!==false){
								$gruppo->catid=$id;
								$db->updateObject("#__cai_gruppo", $gruppo, 'id');
								$cnt++;
							}
						}
					}
				}
			}
		}
		return $cnt;
	}

	private function creaCategoria($gruppo, $parent_id=1, $level, $parentAlias=null){
		JTable::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_categories/tables' );
		JModelLegacy::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_categories/models' );
		$cat_model = $this->getInstance( 'Category', 'CategoriesModel' );

		$alias=str_replace(" ", "", $gruppo->nome);
		$alias=strtolower($alias);
		$dateC = date ( 'dmYhis', time () );
		$alias = str_replace ( " ", "-", $alias . $dateC );

		$data = array (
				'id' => NULL,
				'parent_id' => $parent_id,
				'extension' => 'com_content',
				'title' => $gruppo->nome,
				'alias' => $alias,
				'description' => '',
				'published' => '1',
				'access' => '1',
				'metadesc' => '',
				'metakey' => '',
				'created_user_id' => '0',
				'language' => '*',
				'rules' => array(
						'core.create' => array(),
						'core.delete' => array(),
						'core.edit' => array(),
						'core.edit.state' => array(),
						'core.edit.own' => array(),
				),
				'params' => array(
						'category_layout' => '',
						'image' => '',
				),
				'metadata' => array(
						'author' => '',
						'robots' => '',
				),
		);

		if( !$cat_model->save( $data ) )
		{
			return false;
		}

		$db = JFactory::getDbo();
		$db->setQuery("SELECT id FROM #__categories WHERE alias='$alias' ");
		return $db->loadResult();

	}

	private function creaCategoriaParent($gruppo){
		$db = JFactory::getDbo();

		// JTableCategory is autoloaded in J! 3.0, so...
		if (version_compare(JVERSION, '3.0', 'lt'))
		{
			JTable::addIncludePath(JPATH_PLATFORM . 'joomla/database/table');
		}

		// Initialize a new category
		$category = JTable::getInstance('Category');
		$category->extension = 'com_content';
		$category->title = $gruppo->nome;
		$category->description = $gruppo->description." ";
		$category->published = 1;
		$category->access = 1;
		$category->params = '{"target":"","image":""}';
		$category->metadata = '{"page_title":"","author":"","robots":""}';
		$category->language = '*';
		$alias=str_replace(" ", "", $gruppo->nome);
		$alias=strtolower($alias);
		$dateC = date ( 'dmYhis', time () );
		$category->alias = str_replace ( " ", "-", $alias . $dateC );


		// Set the location in the tree
		$category->setLocation(1, 'last-child');

		// Check to make sure our data is valid
		if (!$category->check())
		{
			JError::raiseNotice(500, $category->getError());
			return false;
		}

		// Now store the category
		if (!$category->store(true))
		{
			JError::raiseNotice(500, $category->getError());
			return false;
		}

		// Build the path for our category
		$category->rebuildPath($category->id);
		return $category;
	}
}