<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => 'FyE17HRALBB7sTVphj0KsLI3ihu1IEec',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => '/web/htdocs/www.caiperugia.it/home/tmp/Joomla_3.4.3_to_3.4.4-Stable-Patch_Package.zip',
	'kickstart.setup.destdir' => '/web/htdocs/www.caiperugia.it/home',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0');