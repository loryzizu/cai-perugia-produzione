<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerExportStatistiche extends JControllerLegacy{
	public function exportSingoloUtente(){
		$modelUtente=$this->getModel("Utente", "CaiModel");
		$modelIscrizione=$this->getModel("Iscrizione ", "CaiModel");
		$modelProposta=$this->getModel("Proposta", "CaiModel");
		$modelExport=$this->getModel("Export", "CaiModel");
		JRequest::setVar("view", "errorgeneric");
		if($modelUtente->isSegretario()){
			$id=JRequest::getVar("utente", null);
			if(isset($id)){
				JRequest::setVar("stato", 1);
				JRequest::setVar("utente", $id);
				JRequest::setVar("view", "exportstatistiche");
				$view=$this->getView("exportstatistiche", "raw");
				$view->setModel($modelProposta, true);
				$view->setModel($modelIscrizione,true);
				$view->setModel($modelUtente,true);
				$view->setModel($modelExport,true);
			}
		}
		parent::display();
	}

	public function exportSingolaAttivita(){
		$modelUtente=$this->getModel("Utente", "CaiModel");
		$modelIscrizione=$this->getModel("Iscrizione ", "CaiModel");
		$modelProposta=$this->getModel("Proposta", "CaiModel");
		$modelExport=$this->getModel("Export", "CaiModel");
		JRequest::setVar("view", "errorgeneric");
		if($modelUtente->isSegretario()){
			$id=JRequest::getVar("idProposta", null);
			if(isset($id)){
				JRequest::setVar("stato", 2);
				JRequest::setVar("idProposta", $id);
				JRequest::setVar("view", "exportstatistiche");
				$view=$this->getView("exportstatistiche", "raw");
				$view->setModel($modelProposta, true);
				$view->setModel($modelIscrizione,true);
				$view->setModel($modelUtente,true);
				$view->setModel($modelExport,true);
			}
		}
		parent::display();
	}


	public function exportCategoriaEDataUnici(){
		$modelUtente=$this->getModel("Utente", "CaiModel");
		$modelIscrizione=$this->getModel("Iscrizione ", "CaiModel");
		$modelProposta=$this->getModel("Proposta", "CaiModel");
		$modelExport=$this->getModel("Export", "CaiModel");
		if($modelUtente->isSegretario()){
			JRequest::setVar("stato", 3);
			JRequest::setVar("view", "exportstatistiche");
			JRequest::setVar("uniciTotali", "unici");
			$view=$this->getView("exportstatistiche", "raw");
			$view->setModel($modelProposta, true);
			$view->setModel($modelIscrizione,true);
			$view->setModel($modelUtente,true);
			$view->setModel($modelExport,true);
		}
		parent::display();
	}


	public function exportCategoriaEDataTotali(){
		$modelUtente=$this->getModel("Utente", "CaiModel");
		$modelIscrizione=$this->getModel("Iscrizione ", "CaiModel");
		$modelProposta=$this->getModel("Proposta", "CaiModel");
		$modelExport=$this->getModel("Export", "CaiModel");
		if($modelUtente->isSegretario()){
			JRequest::setVar("stato", 3);
			JRequest::setVar("view", "exportstatistiche");
			$view=$this->getView("exportstatistiche", "raw");
			JRequest::setVar("uniciTotali", "totali");
			$view->setModel($modelProposta, true);
			$view->setModel($modelIscrizione,true);
			$view->setModel($modelUtente,true);
			$view->setModel($modelExport,true);
		}
		parent::display();
	}
}