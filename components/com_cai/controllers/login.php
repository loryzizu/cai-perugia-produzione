<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerLogin extends JControllerLegacy{
	public function login(){
		$modelUtente=$this->getModel('Utente');
		$username=JRequest::getVar('username',null);
		$password=JRequest::getVar('password',null);
		$loggedin=$modelUtente->login($username,$password);
		if($loggedin==true){
			$modName = 'caiLogin';
			$mod = JModuleHelper::getModule($modName);
			JRequest::setVar("result", "true");
			$content = JModuleHelper::renderModule($mod);
			JRequest::setVar("view", "profilo");
			JRequest::setVar ( "stato", 0 );
			JRequest::setVar("id", null);
			$view = $this->getView ( 'profilo', 'html' );
			$modelProposta = $this->getModel ( 'Proposta' );
			$modelIscrizione=$this->getModel('Iscrizione');
			$view->setModel ( $modelUtente, true );
			$view->setModel ( $modelIscrizione, true );
			$view->setModel ( $modelProposta, true );
			
		}
		else{
			JRequest::setVar("view", "errorInvalidLogin");
		}
		parent::display();
	}

	public function logout(){
		$model=$this->getModel('utente');
		$loggedin=$model->logout();
		parent::setRedirect("index.php");
	}

	
}