<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerRelazione extends JControllerLegacy{

	public function modifica() {
		$idProposta=JRequest::getVar("id", null);
		JRequest::setVar("view","errorGeneric");
		if($idProposta!=null){
			$mProposta=$this->getModel("Proposta", "CAIModel");
			if($mProposta->isPropostaModificabileByCurrentUser($idProposta)){
				$proposta=$mProposta->getPropostaById($idProposta);
				JRequest::setVar("relazionePrivata", $mProposta->getRelazionePrivata($idProposta));
				JRequest::setVar("relazionePubblica", $mProposta->getRelazionePubblica($idProposta));
				$view=$this->getView("relazioneModifica", "html");
				$view->setModel($mProposta, true);
				JRequest::setVar("view","relazioneModifica");
			}
		}
		parent::display();
	}

	public function salva(){
		$idProposta=JRequest::getVar("id", null);
		JRequest::setVar("view","errorGeneric");
		if($idProposta!=null){
			$mProposta=$this->getModel("Proposta", "CAIModel");
			if($mProposta->isPropostaModificabileByCurrentUser($idProposta)){
				$submit=JRequest::getVar("salvaPrivata", null);
				$jinput = JFactory::getApplication ()->input;
				if($submit!=null && $submit=="Salva"){
						$relazionePrivataA = $jinput->get ( 'relazionePrivata', array(0=>""), 'ARRAY' );
						$relazionePrivata=$relazionePrivataA[0];
						$result=$mProposta->salvaRelazionePrivata($idProposta, $relazionePrivata);
						JRequest::setVar("salvataPrivata", $result);
				}else{
					$submit=JRequest::getVar("salvaPubblica", null);
					if($submit!=null && $submit=="Salva"){
						$relazionePubblicaA = $jinput->get ( 'relazionePubblica', array(0=>""), 'ARRAY' );
						$relazionePubblica=$relazionePubblicaA[0];
						$result=$mProposta->salvaRelazionePubblica($idProposta, $relazionePubblica);
						JRequest::setVar("salvataPubblica", $result);
					}

				}
				$view=$this->getView("relazioneModifica", "html");
				$view->setModel($mProposta, true);
				JRequest::setVar("view","relazioneModifica");
			}
		}
		parent::display();
	}

	public function visualizza(){
		$idProposta=JRequest::getVar("id", null);
		JRequest::setVar("view","errorGeneric");
		if($idProposta!=null){
			$mProposta=$this->getModel("Proposta", "CAIModel");
			$view=$this->getView("relazioneVisualizza", "html");
			$view->setModel($mProposta, true);
			JRequest::setVar("view","relazioneVisualizza");
		}
		parent::display();
	}


}