<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerIscrizione extends JControllerLegacy{

	public function display($cachable = false, $urlparams = false) {
		$propostaModel = $this->getModel ( 'proposta' );
		$utenteModel = $this->getModel ( 'utente' );
		$iscrizioneModel = $this->getModel ( 'iscrizione' );
		if($utenteModel->isLogged()){
			$id = JRequest::getVar ( 'id', null );
			if ($id != null) {
				JRequest::setVar ( 'isOrganizzatoreOAdmin', false );
				$isModificabile = $propostaModel->isPropostaModificabileByCurrentUser ( $id );
				if ($isModificabile) {
					JRequest::setVar ( 'isOrganizzatoreOAdmin', true );
				}else{
					$stato=$propostaModel->getPropostaById($id)->stato;
					if($stato!=1 && $stato !=4){
						JRequest::setVar ( 'view', 'errorIscrizioneNonPermessa' );
						parent::display ();
						return;
					}
				}
				$idUtente=$utenteModel->getCurrentUser()->id;
				JRequest::setVar ( 'idProposta', $id );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view = $this->getView ( 'partecipanti', 'html' );
				$view->setModel ( $propostaModel, true );
				$view->setModel ( $utenteModel, true );
				$view->setModel ( $iscrizioneModel, true );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function iscrizioneSocio(){
		$s=JRequest::getVar("submit", null);
		if($s!=null){
			if($s=="Iscrivi"){
				return $this->iscriviSocio();
			}
			elseif($s=="Cancella"){
				return $this->cancellaIscrizioneSocio();
			}
		}
		JRequest::setVar ( 'view', 'errorGeneric' );
		parent::display ();
	}

	public function iscriviSocio(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idUtenteSelezionato=JRequest::getVar("idUtenteSelezionato", null);
			$idProposta=JRequest::getVar("idProposta", null);
			if(isset($idUtente) && isset($idProposta) && isset($idUtenteSelezionato) && $idUtenteSelezionato!='null'){
				$iscritto = $modelIscrizione->iscrivi($idUtenteSelezionato, $idProposta);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($iscritto!==true){
					$view->assignRef ( 'errore', $iscritto );
				}
				else{
					$view->assignRef ( 'iscritto', $iscritto );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'idUtenteSelezionato', $idUtenteSelezionato );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function iscriviSocioEsterno(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idProposta=JRequest::getVar("idProposta", null);
			if(isset($idUtente) && isset($idProposta)){
				$iscritto = $modelIscrizione->iscriviSocioEsterno($idProposta);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($iscritto!==true){
					$view->assignRef ( 'erroreEsterno', $iscritto );
				}
				else{
					$view->assignRef ( 'iscrittoEsterno', $iscritto );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function iscriviNonSocio(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idProposta=JRequest::getVar("idProposta", null);
			if(isset($idUtente) && isset($idProposta)){
				$iscritto = $modelIscrizione->iscriviNonSocio($idProposta);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($iscritto!==true){
					$view->assignRef ( 'erroreNon', $iscritto );
				}
				else{
					$view->assignRef ( 'iscrittoNon', $iscritto );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function cancellaIscrizioneSocio(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idUtenteSelezionato=JRequest::getVar("idUtenteSelezionato", null);
			$idProposta=JRequest::getVar("idProposta", null);
			if(isset($idUtente) && isset($idProposta) && isset($idUtenteSelezionato) && $idUtenteSelezionato!='null'){
				$cancellato = $modelIscrizione->cancella($idUtenteSelezionato, $idProposta);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($cancellato!==true){
					$view->assignRef ( 'errore', $cancellato );
				}
				else{
					$view->assignRef ( 'cancellato', $cancellato );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'idUtenteSelezionato', $idUtenteSelezionato );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function cancellaIscrizioneSocioEsterno(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idProposta=JRequest::getVar("idProposta", null);
			$id=JRequest::getVar("id", null);
			if(isset($idUtente) && isset($idProposta) && isset($id)){
				$iscritto = $modelIscrizione->cancellaSocioEsterno($id);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($iscritto!==true){
					$view->assignRef ( 'errore', $iscritto );
				}
				else{
					$view->assignRef ( 'iscritto', $iscritto );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function cancellaIscrizioneNonSocio(){
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione = $this->getModel ( 'Iscrizione' );
		$iscritto = false;
		if ($modelUtente->isLogged ()) {
			$idUtente=JRequest::getVar("idUtente", null);
			$idProposta=JRequest::getVar("idProposta", null);
			$id=JRequest::getVar("id", null);
			if(isset($idUtente) && isset($idProposta) && isset($id)){
				$iscritto = $modelIscrizione->cancellaNonSocio($id);
				$view = $this->getView ( 'partecipanti', 'html' );
				if($iscritto!==true){
					$view->assignRef ( 'errore', $iscritto );
				}
				else{
					$view->assignRef ( 'iscritto', $iscritto );
				}
				JRequest::setVar ( 'idProposta', $idProposta );
				JRequest::setVar ( 'idUtente', $idUtente );
				JRequest::setVar ( 'view', 'partecipanti' );
				$view->setModel ( $modelProposta, true );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}


}