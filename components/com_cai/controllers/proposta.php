<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );

class CAIControllerProposta extends JControllerLegacy{

	public function display($cachable = false, $urlparams = false){
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelUtente = $this->getModel ( 'Utente' );
		if ($modelUtente->isLogged ()) {
			$vName = JRequest::getCmd ( 'view', 'proposta' ); // default proposta
			$view = $this->getView ( $vName, 'html' );
			JRequest::setVar ( 'view', $vName );
			$view->setModel ( $modelProposta, true );
			$view->setModel ( $modelUtente, true );
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}

	public function crea() {
		JSession::checkToken() or die( 'Invalid Token' );
		JSession::getFormToken(true);
		$modelUtente = $this->getModel ( 'Utente' );
		$modelProposta = $this->getModel ( 'Proposta' );
		$submit = JRequest::getVar ( 'crea', null );
		$salvata = false;
		if ($modelUtente->isLogged ()) {
			if ($submit !=null) {
				$salvata = $modelProposta->crea ();
			}
			if ($salvata == true) {
				JRequest::setVar ( 'view', 'profilo' );
				JRequest::setVar ( "stato", JRequest::getVar('stato', 4) );
				JRequest::setVar("msg", "Proposta inviata con successo!");
				$modelIscrizione= $this->getModel ( 'Iscrizione' );
				$view = $this->getView ( 'profilo', 'html' );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
				$view->setModel ( $modelProposta, true );
				JRequest::setVar("data_inizio", null);
				JRequest::setVar("data_fine", null);
			} else {
				JRequest::setVar ( 'view', 'proposta' );
				$view = $this->getView ( 'proposta', 'html' ); // get the view
				$propostaErrors = $modelProposta->getPropostaErrors ();
				$view->assignRef ( 'propostaErrors', $propostaErrors ); // set the data to the view
				$view->setModel ( $modelUtente, true );
			}
		}
		else{
			JRequest::setVar ( 'view', 'errorIsNotLogged' );
		}
		parent::display ();
	}
	public function modifica() {
		$model = $this->getModel ( 'proposta' );
		$id = JRequest::getVar ( 'id', null );
		$valida = false;
		if ($id != null) {
			$isModificabile = $model->isPropostaModificabileByCurrentUser ( $id );
			if ($isModificabile) {
				$proposta = $model->getPropostaByIdForView ( $id );
				if ($proposta != null) {
					JRequest::setVar ( 'view', 'proposta' );
					$modelUtente = $this->getModel ( 'Utente' );
					if($modelUtente->isSegretario()){
						if($model->isSvolta($id)){
							JRequest::setVar ( 'mode', 'modificaSvoltaAdmin' );
						}
						else{
							JRequest::setVar ( 'mode', 'modificaAdmin' );
						}
					}
					else{
						JRequest::setVar ( 'mode', 'modifica' );
					}
					$view = $this->getView ( 'proposta', 'html' ); // get the view
					$view->assignRef ( 'proposta', $proposta );
					$valida = true;
					$view->setModel ( $modelUtente, true );
				}
				else{
					JRequest::setVar ( 'view', 'errorGeneric' );
				}
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
			parent::display ();
		}
	}
	public function cestina() {
		$model = $this->getModel ( 'proposta' );
		$id = JRequest::getVar ( 'id', null );
		if ($id != null) {
			$isModificabile = $model->isPropostaModificabileByCurrentUser ( $id );
			if ($isModificabile) {
				$model->cestinaProposta($id);
			}
			else{
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
			JRequest::setVar ( 'view', 'attivitaDaApprovareAdmin' );
			$modelProposta = $this->getModel ( 'Proposta' );
			$view = $this->getView ( 'attivitaDaApprovareAdmin', 'html' );
			$view->setModel ( $modelProposta, true );
			parent::display ();
		}
	}

	public function salvaModifica() {
		JSession::checkToken() or die( 'Invalid Token' );
		JSession::getFormToken(true);
		$propostaModel = $this->getModel ( 'proposta' );
		$utenteModel = $this->getModel ( 'Utente' );
		$id=JRequest::getVar ( 'idProposta', null );
		$typeOfButtonsToDisplay=null;

		$type=null;

		if($id!=null){
			if($utenteModel->isSegretario()){
				$s1 = JRequest::getVar ( 'salvaModifiche', null );
				$s2 = JRequest::getVar ( 'approva', null );
				$s3 = JRequest::getVar ( 'approvaESalvaModifiche', null );
				$s4 = JRequest::getVar ( 'nonApprovare', null );
				$s5 = JRequest::getVar ( 'nonApprovareMaSalvaModifiche', null );
				if($s2!=null || $s3!=null || $s4!=null || $s5!=null || $s1!=null){
					$typeOfButtonsToDisplay="modificaAdmin";
					if($s2!=null){
						$type='approva';
					}
					elseif($s3!=null){
						$type='approvaESalvaModifiche';
					}
					elseif($s4!=null){
						$type='nonApprovare';
					}
					elseif($s5!=null){
						$type='nonApprovareMaSalvaModifiche';
					}
					else if($s1!=null){
						$type='salvaModifiche';
						$typeOfButtonsToDisplay="modificaSvoltaAdmin";

					}
				}
			}
			elseif($propostaModel->isPropostaModificabileByCurrentUser($id)){
				$s1 = JRequest::getVar ( 'salvaModifiche', null );
				if ($s1!=null){
					$type='salvaModifiche';
					$typeOfButtonsToDisplay="modifica";
				}
			}

			//entra solo se segretario o ddg
			if ($type!=null){//true se si è cliccato su un modifica
				$isSalvato=$propostaModel->salva($type);
				if($isSalvato){
					$view=null;
					if($utenteModel->isSegretario()){
						$stato=$propostaModel->getPropostaById($id)->stato;

						if(($stato==1 || $stato==4)){
							JRequest::setVar ( 'view', 'attivitaApprovateAdmin' );
							$view = $this->getView ( 'attivitaApprovateAdmin', 'html' );
							JRequest::setVar("msg", "Proposta approvata!");
						}
						elseif(($stato==3 || $stato==5)){
							JRequest::setVar ( 'view', 'attivitaDaApprovareAdmin' );
							$view = $this->getView ( 'attivitaDaApprovareAdmin', 'html' );
							JRequest::setVar("msg", "Proposta non approvata!");
						}
						elseif($stato==2){
							JRequest::setVar ( 'view', 'attivitaSvolteAdmin' );
							$view = $this->getView ( 'attivitaSvolteAdmin', 'html' );
							JRequest::setVar("msg", "Attività svolta modificata!");
						}
						else{
							JRequest::setVar ( 'view', 'errorGeneric' );
						}
					}
					else{
						JRequest::setVar ( 'view', 'profilo' );
						JRequest::setVar ( "stato", JRequest::getVar('stato', 4) );
						JRequest::setVar("msg", "Proposta modificata con successo!");
						$view = $this->getView ( 'profilo', 'html' );
					}
					if($view!=null){
						$modelIscrizione= $this->getModel ( 'Iscrizione' );
						JRequest::setVar("data_inizio", null);
						JRequest::setVar("data_fine", null);
						$view->setModel ( $utenteModel, true );
						$view->setModel ( $modelIscrizione, true );
						$view->setModel ( $propostaModel, true );
					}
					parent::display();
					return;
				}
				else{
					JRequest::setVar ( 'view', 'proposta' );
					$view = $this->getView ( 'proposta', 'html' ); // get the view
					$propostaErrors = $propostaModel->getPropostaErrors ();
					$view->assignRef ( 'propostaErrors', $propostaErrors ); // set the data to the view
					$view->setModel ( $utenteModel, true );
				}
			}
			else{//se non si vuole modificare si vuole ripristinare?
				$ripristino=false;
				if($utenteModel->isSegretario()){
					$s7 = JRequest::getVar ( 'ripristinaAdmin', null );
					$s8 = JRequest::getVar ( 'ripristinaSvoltaAdmin', null );
					if($s7!=null){
						$typeOfButtonsToDisplay="modificaAdmin";
						$ripristino=true;
					}
					if($s8!=null){
						$typeOfButtonsToDisplay="modificaSvoltaAdmin";
						$ripristino=true;
					}
				}
				elseif($propostaModel->isPropostaModificabileByCurrentUser($id)){
					$s6 = JRequest::getVar ( 'ripristina', null );
					if($s6!=null){
						$typeOfButtonsToDisplay="modifica";
						$ripristino=true;
					}
				}
				if($ripristino){//true se si vuole ripristinare
					JRequest::setVar ( 'view', 'proposta' );
					$view = $this->getView ( 'proposta', 'html' ); // get the view
					$proposta= $propostaModel->getPropostaByIdForView($id);
					$propostaBkp= $propostaModel->getPropostaBkpByIdForView($id);
					$propostaBkp->stato=$proposta->stato;
					$view->assignRef ( 'proposta', $propostaBkp ); // set the data to the view
					$view->setModel ( $utenteModel, true );
				}
			}
		}
		if($typeOfButtonsToDisplay==null){//non è stata inserita nessuna operazione valida da un utente valido
			JRequest::setVar ( 'view', 'errorGeneric' );
		}
		else{
			JRequest::setVar("mode", $typeOfButtonsToDisplay);
		}
		parent::display ();
	}

	public function cancellaImmagine(){
		$valid=false;
		$id=JRequest::getVar('id_proposta',null);
		$nome=JRequest::getVar('nome_immagine',null);
		if($id!=null && $nome!= null){
			$propostaModel=$this->getModel('proposta');
			if($propostaModel->isPropostaModificabileByCurrentUser($id)){
				if($propostaModel->cancellaImmagine($id,$nome)){
					JRequest::setVar("view","fileCancellato");
					$valid=true;
				}
			}
		}
		if(!$valid){
			JRequest::setVar("view","errorGeneric");
		}
		parent::display();
	}

	public function cancellaAllegato(){
		$valid=false;
		$id=JRequest::getVar('id_proposta',null);
		$nome=JRequest::getVar('nome_allegato',null);
		if($id!=null && $nome!= null){
			$propostaModel=$this->getModel('proposta');
			if($propostaModel->isPropostaModificabileByCurrentUser($id)){
				if($propostaModel->cancellaAllegato($id,$nome)){
					JRequest::setVar("view","fileCancellato");
					$valid=true;
				}
			}
		}
		if(!$valid){
			JRequest::setVar("view","errorGeneric");
		}
		parent::display();
	}



}