<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerProfilo extends JControllerLegacy{
	public function profilo() {
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione= $this->getModel ( 'Iscrizione' );
		$modelUtente = $this->getModel ( 'Utente' );
		if ($modelUtente->isLogged ()) {
			$idUtente = JRequest::getVar ( "id", null );
			JRequest::setVar ( "view", "profilo" );
			JRequest::setVar ( "stato", JRequest::getVar('stato', 0) );
			$view = $this->getView ( 'profilo', 'html' );
			$view->setModel ( $modelUtente, true );
			$view->setModel ( $modelIscrizione, true );
			$view->setModel ( $modelProposta, true );
			if ($modelUtente->isSegretario ()) {
				if ($idUtente != null) {
					JRequest::setVar ( "isAdmin", true );
					JRequest::setVar ( "id", $idUtente );
				}
			}
		}
		else{
			JRequest::setVar ( "view", "errorIsNotLogged" );
		}
		parent::display ();
	}
}