<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerUtente extends JControllerLegacy{
	
	public function cambiaPassword(){
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelIscrizione= $this->getModel ( 'Iscrizione' );
		$modelUtente = $this->getModel ( 'Utente' );
		$id=JRequest::getVar("id",null);
		JRequest::setVar("view", "errorGeneric");
		if($id!=null){
			$currentUserId=$modelUtente->getCurrentUser()->id;
			if ($modelUtente->isSegretario () || $currentUserId==$id) {
				$vecchia=JRequest::getVar("old", null);
				$nuova=JRequest::getVar("new", null);
				$nuovaC=JRequest::getVar("newC", null);
				$return=$modelUtente->cambiaPassword($id, $vecchia, $nuova, $nuovaC);
				if($return!==true){
					if($return===false){
						JRequest::setVar("view", "errorGeneric");
						parent::display();
						return;
					}
					JRequest::setVar("error",$return);
				}
				else{
					JRequest::setVar("cambiata", true);
				}
				if ($modelUtente->isSegretario ()) {
						JRequest::setVar ( "isAdmin", true );
						JRequest::setVar ( "id", $id );
				}
				JRequest::setVar ( "view", "profilo" );
				JRequest::setVar ( "stato", JRequest::getVar('stato', 1) );
				$view = $this->getView ( 'profilo', 'html' );
				$view->setModel ( $modelUtente, true );
				$view->setModel ( $modelIscrizione, true );
				$view->setModel ( $modelProposta, true );
			}
		}
		parent::display();
	}
	
	public function recuperoPassword(){
		JRequest::setVar("recupero", false);
		$model=$this->getModel('utente');
		$cf=JRequest::getVar("cf", null);
		$un=JRequest::getVar("username",null);
		$utente=null;
		if($un != null){
			$utente=$model->getUserByUsername($un);
		}
		if($utente == null){
			if($cf!= null){
				$utente=$model->getUserByCF($cf);
			}
		}
		if($utente!=null){
			if($model->invioEmailRecupero($utente->email, $utente)){
				JRequest::setVar("recupero", true);
			}
		}
		else{
			JRequest::setVar("error", true);
		}
		JRequest::setVar("view", "recuperoPassword");
		parent::display();
	}
	
	public function displayRecuperoPassword(){
		JRequest::setVar("view", "recuperoPassword");
		parent::display();
	}
		
}