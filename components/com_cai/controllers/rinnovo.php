<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerRinnovo extends JControllerLegacy{

	public function aggiorna() {
		//$modelProposta = $this->getModel ( 'Proposta' );
		//$modelIscrizione= $this->getModel ( 'Iscrizione' );
		$modelUtente = $this->getModel ( 'Utente' );
		if ($modelUtente->isSegretario ()) {
			$modelRinnovo=$this->getModel("Rinnovo");
			$result=$modelRinnovo->aggiorna();
			if($result!=false){
				JRequest::setVar("righe", $result["cntRows"]);
				JRequest::setVar("nuovi", $result["cntInserted"]);
				JRequest::setVar("aggiornati", $result["cntUpdated"]);
				JRequest::setVar("aggiornatoStato", $result["cntChangedStatus"]);
				JRequest::setVar("rinnovato", true);
			}else{
				JRequest::setVar("rinnovato", false);
			}
			JRequest::setVar("view", "rinnovoIscrizioniAdmin");
			$view=$this->getView("rinnovoIscrizioniAdmin", 'html');
			$view->setModel($modelUtente, true);
		}
		parent::display();
	}

	public function cambiaAnno() {
		$modelUtente = $this->getModel ( 'Utente' );
		if ($modelUtente->isSegretario ()) {
			$modelRinnovo=$this->getModel("Rinnovo");
			$result=$modelRinnovo->cambiaAnno();
			if($result!=false){
				JRequest::setVar("righe", $result["cntRows"]);
				JRequest::setVar("nuovi", $result["cntInserted"]);
				JRequest::setVar("aggiornati", $result["cntUpdated"]);
				JRequest::setVar("aggiornatoStato", $result["cntChangedStatus"]);
				JRequest::setVar("rinnovato", true);
			}else{
				JRequest::setVar("rinnovato", false);
			}
			JRequest::setVar("view", "rinnovoIscrizioniAdmin");
			$view=$this->getView("rinnovoIscrizioniAdmin", 'html');
			$view->setModel($modelUtente, true);
		}
		parent::display();
	}
}