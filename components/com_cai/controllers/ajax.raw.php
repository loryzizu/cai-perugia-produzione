<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerAjax extends JControllerLegacy{


	public function listaUtenti() {
		JRequest::setVar("view","ajaxListaUtenti");
		$view = $this->getView ( "ajaxListaUtenti", 'raw' );
		$modelUtente = $this->getModel( 'Utente' );
		$view->setModel($modelUtente, true );
		parent::display();

	}
}