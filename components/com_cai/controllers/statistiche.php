<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerStatistiche extends JControllerLegacy{

	public function visualizza(){
		$modelUtente=$this->getModel("Utente", "CaiModel");
		$modelIscrizione=$this->getModel("Iscrizione ", "CaiModel");
		$modelProposta=$this->getModel("Proposta", "CaiModel");
		JRequest::setVar("view", "errorGeneric");
		if($modelUtente->isSegretario()){
			JRequest::setVar("view", "statisticheAdmin");
			$view=$this->getView("statisticheAdmin", "html");
			$view->setModel($modelProposta, true);
			$view->setModel($modelIscrizione,true);
			$view->setModel($modelUtente,true);
		}
		parent::display();
	}

	public function singoloUtente(){
		$id=JRequest::getVar("utente", null);
		if(isset($id)){
			JRequest::setVar("stato", 1);
			JRequest::setVar("utente", $id);
			$this->visualizza();
		}
	}

	public function singolaAttivita(){
		$id=JRequest::getVar("idProposta", null);
		if(isset($id)){
			JRequest::setVar("stato", 2);
			JRequest::setVar("idProposta", $id);
			$this->visualizza();
		}
	}

	public function categoriaEData(){
		JRequest::setVar("stato", 3);
		//JRequest::setVar("filtraCategoriaData", true);
		$this->visualizza();
	}


}
