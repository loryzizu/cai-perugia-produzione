<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CAIControllerEmail extends JControllerLegacy{

	public function display($cachable = false, $urlparams = false) {
		$idProposta=JRequest::getVar("id", null);
		JRequest::setVar("view","errorGeneric");
		if($idProposta!=null){
			$mUtente=$this->getModel("Utente", "CAIModel");
			$mProposta=$this->getModel("Proposta", "CAIModel");
			if($mProposta->isPropostaModificabileByCurrentUser($idProposta)){
				$view=$this->getView("email", "html");
				$modelIscrizione=$this->getModel("Iscrizione", "CAIModel");
				$view->setModel($modelIscrizione, true);
				JRequest::setVar("view","email");
			}
		}
		parent::display();
	}

	public function invia(){
		$idProposta=JRequest::getVar("id", null);
		if($idProposta!=null){
			$mUtente=$this->getModel("Utente", "CAIModel");
			$mProposta=$this->getModel("Proposta", "CAIModel");
			if($mProposta->isPropostaModificabileByCurrentUser($idProposta)){
				$messaggio=null;
				$jinput = JFactory::getApplication ()->input;
				$messaggioA = $jinput->get ( 'messaggio', '', 'ARRAY' );
				if (isset ( $messaggioA [0] ) && count ( $messaggioA  ) > 0) {
					$messaggio = $messaggioA [0];
				}
				$destinatari=JRequest::getVar("destinatari", null);
				$oggetto=JRequest::getVar("oggetto", null);
				$mEmail=$this->getModel("Email", "CAIModel");
				$result=$mEmail->invia($destinatari, $oggetto, $messaggio);
				$view=$this->getView("email",'html');
				if($result===true){
					$view->assignRef('inviata', $result);
				}
				else{
					$view->assignRef('error', $result);
				}
				$modelIscrizione=$this->getModel("Iscrizione", "CAIModel");
				$view->setModel($modelIscrizione, true);
				JRequest::setVar("view","email");
			}
		}
		parent::display();
	}


}