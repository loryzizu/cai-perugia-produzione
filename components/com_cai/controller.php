<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
class CaiController extends JControllerLegacy{
	public function display($cachable = false, $urlparams = false) {
		$modelProposta = $this->getModel ( 'Proposta' );
		$modelUtente = $this->getModel ( 'Utente' );
		$modelIscrizione=$this->getModel('Iscrizione');
		if ($modelUtente->isLogged ()) {
			$vName = JRequest::getCmd ( 'view', 'proposta' ); // default proposta
			$view = $this->getView ( $vName, 'html' );
			if (trim ( $vName ) == "attivitaDaApprovareAdmin" || trim ( $vName ) == "attivitaApprovateAdmin" || trim ( $vName ) == "attivitaSvolteAdmin" || trim ( $vName ) == "elencoSociAdmin" || trim ( $vName ) == "rinnovoIscrizioniAdmin") {
				if ($modelUtente->isSegretario ()) {
					JRequest::setVar('view', $vName);
					$modelProposta = $this->getModel('Proposta');
					$view->setModel($modelProposta, true);
					$view->setModel($modelUtente, true);
					$view->setModel($modelIscrizione, true);
				}else {
					JRequest::setVar ( 'view', 'errorIsNotAdmin' );
				}
			}
			elseif(trim($vName)=="calendario"){
				JRequest::setVar ( 'view', 'calendario' );
			}
            elseif(trim($vName)=="tracciatigps"){
                $view = $this->getView ( $vName, 'html' );
                $view->setModel($modelProposta, true);
                JRequest::setVar ( 'view', 'tracciatigps' );
            }
			else {
				JRequest::setVar ( 'view', 'errorGeneric' );
			}
		} else {
			$vName = JRequest::getCmd ( 'view', 'errorIsNotLogged' );
			if(trim($vName)=="calendario"){
				JRequest::setVar ( 'view', 'calendario' );
			}
            elseif(trim($vName)=="tracciatigps"){
                $view = $this->getView ( $vName, 'html' );
                $view->setModel($modelProposta, true);
                JRequest::setVar ( 'view', 'tracciatigps' );
            }
			else{
				JRequest::setVar ( 'view', 'errorIsNotLogged' );
			}

		}
		parent::display ();
	}


}