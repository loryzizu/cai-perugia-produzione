<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import joomla controller library
jimport('joomla.application.component.controller');

JHtml::_('stylesheet', 'http://fonts.googleapis.com/css?family=Arvo');
JHtml::_('stylesheet', 'http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext');
JHtml::_('stylesheet', 'https://fonts.googleapis.com/icon?family=Material+Icons');
JHtml::_('stylesheet', JUri::root() . 'media/com_cai/css/style.css');



$controller = JControllerLegacy::getInstance('CAI');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
