<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelUtils extends JModelItem {
	public function convertDateFormat($date) {
		if ($date != null) {
			if (strpos($date, '/') !== FALSE){
				$date=str_replace ( "/", "-", $date );
			}
			return date ( 'Y-m-d', strtotime ( $date ) );
		}
	}
	public function isDatesInOrder($dataInizio, $dataFine) {
	    if($dataInizio ==null || $dataFine == null)
	        return false;
		if (trim ( ($dataInizio) ) != '' && trim ( ($dataFine) ) != '') {
			$data_inizio = $this->convertDateFormat ( $dataInizio );
			$data_fine = $this->convertDateFormat ( $dataFine );
			if ($data_inizio != false && $data_fine != false) {
				if ($data_fine > $data_inizio) {
					return true;
				}
			}
		}
		return false;
	}
	private function getTableRowsCount($tableName){
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$tableName=JFactory::getDbo()->escape($tableName);
		$results=$query->setQuery("SELECT COUNT(*) FROM $tableName;");
	}

	public function paginate($resultList,$page=1, $size=20){
		if(isset($resultList) && ($count=count($resultList))>0
			&& isset($page) && ((int) $page)>0
			&& isset($size) && ((int) $size)>0 ){

			$pages=(int)ceil($count/$size);
			if($page>0 && $page<=$pages){
				$page=$page;
			}
			else{
				$page=1;
			}
			$start=$size*($page-1);
			$end=$start+$size;

			$result=array();
			$j=0;
			for($i=$start; $i<$end;$i++){
				if($i>count($resultList)-1)
					break;
				$result[$j]=$resultList[$i];
				$j++;
			}
			return $result;
		}
		return $result=array();
	}



}