<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelExport extends JModelItem {

	public function inviaFileSingoloUtente($proposte, $partecipazioni, $iscrizioni, $nomeFile){
		$nomeFile=filter_var($nomeFile, FILTER_SANITIZE_STRING);
		$nomeFile=str_replace(" ", "", $nomeFile);
		$path=JPATH_ROOT."/media/com_cai/proposte_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output = fopen($path, 'w');

		fputcsv($output, array(
				"titolo"=>"Titolo",
				"data_inizio"=>"Data inizio",
				"data_fine"=>"Data fine",
				"tipologia"=>"Tipologia",
				"gruppo"=>"Gruppo",
				"stato"=>"Stato"
		)
		);
		foreach ($proposte as $proposta){
			$propostaModel = $this->getInstance ( "Proposta" , "CAIModel" );
			$titolo=$proposta->titolo;
			$data_inizio=$propostaModel->getDateEuropeanFormat($proposta->data_inizio);
			$data_fine=$propostaModel->getDateEuropeanFormat($proposta->data_fine);
			$tipologia=$propostaModel->getTipologiaLabel($proposta->tipologia);
			$gruppo=$propostaModel->getGruppoLabel($proposta->gruppo);
			$stato=$propostaModel->getStatoLabel($proposta->stato);
			$row= array(
					"titolo"=>$titolo,
					"data_inizio"=>$data_inizio,
					"data_fine"=>$data_fine,
					"tipologia"=>$tipologia,
					"gruppo"=>$gruppo,
					"stato"=>$stato

			);
			fputcsv($output, $row);
		}

		$path2=JPATH_ROOT."/media/com_cai/partecipazioni_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output2 = fopen($path2, 'w');

		fputcsv($output2, array(
				"titolo"=>"Titolo",
				"data_inizio"=>"Data inizio",
				"data_fine"=>"Data fine",
				"tipologia"=>"Tipologia",
				"gruppo"=>"Gruppo",
		)
		);
		foreach ($partecipazioni as $proposta){
			$propostaModel = $this->getInstance ( "Proposta" , "CAIModel"  );
			$titolo=$proposta->titolo;
			$data_inizio=$propostaModel->getDateEuropeanFormat($proposta->data_inizio);
			$data_fine=$propostaModel->getDateEuropeanFormat($proposta->data_fine);
			$tipologia=$propostaModel->getTipologiaLabel($proposta->tipologia);
			$gruppo=$propostaModel->getGruppoLabel($proposta->gruppo);
			$row= array(
					"titolo"=>$titolo,
					"data_inizio"=>$data_inizio,
					"data_fine"=>$data_fine,
					"tipologia"=>$tipologia,
					"gruppo"=>$gruppo,

			);
			fputcsv($output2, $row);
		}

		$path3=JPATH_ROOT."/media/com_cai/iscrizioni_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output3 = fopen($path3, 'w');

		fputcsv($output3, array(
				"titolo"=>"Titolo",
				"data_inizio"=>"Data inizio",
				"data_fine"=>"Data fine",
				"tipologia"=>"Tipologia",
				"gruppo"=>"Gruppo",
		)
		);
		foreach ($iscrizioni as $proposta){
			$propostaModel = $this->getInstance ( "Proposta" , "CAIModel" );
			$titolo=$proposta->titolo;
			$data_inizio=$propostaModel->getDateEuropeanFormat($proposta->data_inizio);
			$data_fine=$propostaModel->getDateEuropeanFormat($proposta->data_fine);
			$tipologia=$propostaModel->getTipologiaLabel($proposta->tipologia);
			$gruppo=$propostaModel->getGruppoLabel($proposta->gruppo);
			$row= array(
					"titolo"=>$titolo,
					"data_inizio"=>$data_inizio,
					"data_fine"=>$data_fine,
					"tipologia"=>$tipologia,
					"gruppo"=>$gruppo,

			);
			fputcsv($output3, $row);
		}

		$files = array($path, $path2, $path3);
		$zipname =JPATH_ROOT."/media/com_cai/$nomeFile.zip";
		$zip = new ZipArchive;
		$createZipResult=$zip->open($zipname, ZipArchive::CREATE);
		foreach ($files as $file) {
			$ok=$zip->addFromString(basename($file),file_get_contents($file));
		}
		$zip->close();

		fclose($output);
		fclose($output2);
		fclose($output3);
		@unlink($path);
		@unlink($path2);
		@unlink($path3);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-disposition: attachment; filename='.$nomeFile.".zip");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . filesize($zipname));
		ob_end_flush();
		@readfile($zipname);
		@unlink($zipname);


	}


	public function inviaFileSingolaAttivita($soci, $sociEsterni, $nonSoci, $nomeFile){
		$nomeFile=filter_var($nomeFile, FILTER_SANITIZE_STRING);
		$nomeFile=str_replace(" ", "-", $nomeFile);
		$nomeFile=str_replace("/", "-", $nomeFile);
		$path=JPATH_ROOT."/media/com_cai/soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output = fopen($path, 'w');

		fputcsv($output, array(
				"id",
				"Codice Fiscale",
				"username",
				"password",
				"email",
				"nome",
				"cognome",
				"titoli",
				"data di nascita",
				"telefono",
				"cellulare",
				"professione",
				"note",
				"foto",
				"stato"
		)
		);

		foreach ($soci as $socio){
			$propostaModel = $this->getInstance ( "Proposta" , "CAIModel" );
			$socio->data_nascita=$propostaModel->getDateEuropeanFormat($socio->data_nascita);
			$socio->stato=$this->getInstance ( "utente" , "CAIModel"  )->getStatoLabel($socio->stato);
			$socio->password="***********";
			fputcsv($output, (array) $socio);
		}

		$path2=JPATH_ROOT."/media/com_cai/soci_esterni_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output2 = fopen($path2, 'w');

		fputcsv($output2, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"sezione",
				"note",
		)
		);
		foreach ($sociEsterni as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output2, (array)$socio);
		}

		$path3=JPATH_ROOT."/media/com_cai/non_soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output3 = fopen($path3, 'w');

		fputcsv($output3, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"note",
		)
		);

		foreach ($nonSoci as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output3, (array)$socio);
		}

		$files = array($path, $path2, $path3);
		$zipname =JPATH_ROOT."/media/com_cai/tmp/$nomeFile.zip";
		$zip = new ZipArchive;
		$openZipResult=$zip->open($zipname, ZipArchive::CREATE);
		foreach ($files as $file) {
			$ok=$zip->addFromString(basename($file),file_get_contents($file));
		}
		$zip->close();

		fclose($output);
		fclose($output2);
		fclose($output3);
		@unlink($path);
		@unlink($path2);
		@unlink($path3);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-disposition: attachment; filename='.$nomeFile.".zip");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . filesize($zipname));
		ob_end_flush();
		@readfile($zipname);
		@unlink($zipname);


	}

	public function inviaFileCategoriaEDataUnici($soci, $sociEsterni, $nonSoci, $nomeFile){
		$nomeFile=filter_var($nomeFile, FILTER_SANITIZE_STRING);
		$nomeFile=str_replace(" ", "-", $nomeFile);
		$nomeFile=str_replace("/", "-", $nomeFile);
		$path=JPATH_ROOT."/media/com_cai/soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output = fopen($path, 'w');

		fputcsv($output, array(
				"id",
				"Codice Fiscale",
				"username",
				"password",
				"email",
				"nome",
				"cognome",
				"titoli",
				"data di nascita",
				"telefono",
				"cellulare",
				"professione",
				"note",
				"foto",
				"stato"
		)
		);

		foreach ($soci as $socio){
			$propostaModel = $this->getInstance ( "Proposta" , "CAIModel" );
			$socio->data_nascita=$propostaModel->getDateEuropeanFormat($socio->data_nascita);
			$socio->stato=$this->getInstance ( "utente" , "CAIModel" )->getStatoLabel($socio->stato);
			$socio->password="***********";
			unset($socio->salt);
			fputcsv($output, (array) $socio);
		}

		$path2=JPATH_ROOT."/media/com_cai/soci_esterni_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output2 = fopen($path2, 'w');

		fputcsv($output2, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"sezione",
				"note",
		)
		);
		foreach ($sociEsterni as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output2, (array)$socio);
		}

		$path3=JPATH_ROOT."/media/com_cai/non_soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output3 = fopen($path3, 'w');

		fputcsv($output3, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"note",
		)
		);

		foreach ($nonSoci as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output3, (array)$socio);
		}

		$files = array($path, $path2, $path3);
		$zipname =JPATH_ROOT."/media/com_cai/$nomeFile.zip";
		$zip = new ZipArchive;
		$zip->open($zipname, ZipArchive::CREATE);
		foreach ($files as $file) {
			$ok=$zip->addFromString(basename($file),file_get_contents($file));
		}
		$zip->close();

		fclose($output);
		fclose($output2);
		fclose($output3);
		@unlink($path);
		@unlink($path2);
		@unlink($path3);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-disposition: attachment; filename='.$nomeFile.".zip");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . filesize($zipname));
		ob_end_flush();
		@readfile($zipname);
		@unlink($zipname);


	}

	public function inviaFileCategoriaEDataTotali($soci, $sociEsterni, $nonSoci, $nomeFile){
		$nomeFile=filter_var($nomeFile, FILTER_SANITIZE_STRING);
		$nomeFile=str_replace(" ", "-", $nomeFile);
		$nomeFile=str_replace("/", "-", $nomeFile);
		$path=JPATH_ROOT."/media/com_cai/soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output = fopen($path, 'w');

		fputcsv($output, array(
				"id",
				"Codice Fiscale",
				"username",
				"email",
				"nome",
				"cognome",
				"titoli",
				"data di nascita",
				"telefono",
				"cellulare",
				"professione",
				"note",
				"stato",
				"titolo attività",
				"data inizio",
				"data fine",
				"tipologia",
				"gruppo"

		)
		);

		foreach ($soci as $socio){
			$propostaModel = $this->getInstance( "Proposta", "CAIModel" );
			$data_nascita=$propostaModel->getDateEuropeanFormat($socio->data_nascita);
			$stato=$this->getInstance ( "utente" , "CAIModel"  )->getStatoLabel($socio->stato);
			$data_inizio=$propostaModel->getDateEuropeanFormat($socio->data_inizio);
			$data_fine=$propostaModel->getDateEuropeanFormat($socio->data_fine);
			$tipologia=$propostaModel->getTipologiaLabel($socio->tipologia);
			$gruppo=$propostaModel->getGruppoLabel($socio->gruppo);
			$a=array($socio->id, $socio->CF, $socio->username, $socio->email, $socio->nome, $socio->cognome, $socio->titoli, $data_nascita,
					$socio->telefono, $socio->cellulare, $socio->professione, $socio->note, $stato, $socio->titolo, $data_inizio, $data_fine,
					$tipologia, $gruppo
			);
			fputcsv($output, $a);
		}

		$path2=JPATH_ROOT."/media/com_cai/soci_esterni_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output2 = fopen($path2, 'w');

		fputcsv($output2, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"sezione",
				"note",
		)
		);
		foreach ($sociEsterni as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output2, (array)$socio);
		}

		$path3=JPATH_ROOT."/media/com_cai/non_soci_$nomeFile.csv";
		// create a file pointer connected to the output stream
		$output3 = fopen($path3, 'w');

		fputcsv($output3, array(
				"nome",
				"cognome",
				"data di nascita",
				"telefono",
				"email",
				"codice fiscale",
				"note",
		)
		);

		foreach ($nonSoci as $socio){
			unset($socio->id);
			unset($socio->id_proposta);
			fputcsv($output3, (array)$socio);
		}

		$files = array($path, $path2, $path3);
		$zipname =JPATH_ROOT."/media/com_cai/$nomeFile.zip";
		$zip = new ZipArchive;
		$zip->open($zipname, ZipArchive::CREATE);
		foreach ($files as $file) {
			$ok=$zip->addFromString(basename($file),file_get_contents($file));
		}
		$zip->close();

		fclose($output);
		fclose($output2);
		fclose($output3);
		@unlink($path);
		@unlink($path2);
		@unlink($path3);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/octet-stream");
		header('Content-disposition: attachment; filename='.$nomeFile.".zip");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . filesize($zipname));
		ob_end_flush();
		@readfile($zipname);
		@unlink($zipname);


	}


}