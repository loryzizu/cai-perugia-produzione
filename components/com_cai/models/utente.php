<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelUtente extends JModelItem {
	public $proposta;
	public $propostaErrors;
	public $visualizza = false;
	public function getTable($type = 'Utente', $prefix = 'CAITable', $config = array()) {
		return JTable::getInstance ( $type, $prefix, $config );
	}
	public function getUserById($id) {
		if($id!=null && $id!='0' && trim($id)!=''){
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ("*");
			$query->from ( '#__cai_utente');
			$id=intval($id);
			$query->where("id = '$id' ");
			$db->setQuery ( $query );
			$utente = $db->loadObject ();
			return $utente;
		}
		return null;
	}
	public function getCognomeNomeById($id){
		if($id!=null && $id!='0' && trim($id)!=''){
			$u=$this->getUserById($id);
			return $u->cognome." ".$u->nome;
		}
		return '';
	}

	public function getCurrentUserCognomeNome(){
		$u=$this->getCurrentUser();
		if($u!=null){
			return $this->getCognomeNomeById($u->id);
		}
		return '';
	}
	public function getUtenteList() {//tutti gli iscritti
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( (array (
				't.*'
		)) )->from ( $db->quoteName ( '#__cai_utente', 't' ) );
		$query->where(" stato=1 ");
		$query->order("cognome");
		$db->setQuery ( $query );
		$listaUtenti = $db->loadObjectList ();
		return $listaUtenti;
	}
	
	public function getUtenteListOld() {//tutti gli iscritti vecchi
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( (array (
				't.*'
		)) )->from ( $db->quoteName ( '#__cai_utente', 't' ) );
		$query->where(" stato=0 ");
		$query->order("cognome");
		$db->setQuery ( $query );
		$listaUtenti = $db->loadObjectList ();
		return $listaUtenti;
	}

	public function getUtenteListAll() {//tutti gli iscritti, anche vecchi
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( (array (
				't.*'
		)) )->from ( $db->quoteName ( '#__cai_utente', 't' ) );
		$query->order("cognome");
		$db->setQuery ( $query );
		$listaUtenti = $db->loadObjectList ();
		return $listaUtenti;
	}
	public function login($username, $password) {
		if (trim ( $username ) != '' && trim ( $password ) != '') {
			$username=strtolower($username);
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			
			$query->select ( $db->quoteName ( array (
					'id',
					'username',
					'nome',
					'cognome', 
					'password'
			) ) )->from ( $db->quoteName ( '#__cai_utente' ) )
			->where ( $db->quoteName ( 'username' ) . '=' . $db->quote ( $username )  . " AND " . $db->quoteName ( 'stato' ) . '=' . $db->quote ( '1' ) );
			$db->setQuery ( $query );
			$utente = $db->loadObject ();
			if (isset ( $utente )) {
				if(JUserHelper::verifyPassword($password, $utente->password)){
					$session =JFactory::getSession ();
					$session->set ( 'user_id', $utente->id );
					$session->set ( 'user_name', $utente->nome );
					$session->set ( 'user_surname', $utente->cognome );
					return true;
				}
			}
		}
		return false;
	}

	public function getCurrentUser(){
		$session =JFactory::getSession();
		$user_id = $session->get ( 'user_id', null );
		return $this->getUserById($user_id);
	}

	public function logout() {
		$session =JFactory::getSession ();
		$session->clear('user_id');
		$session->clear('user_name');
		$session->clear('user_surname');
	}

	public function isOrganizzatore($idProposta) {
		$currentUser=$this->getCurrentUser();
		$idUtente=null;
		if($currentUser!=null){
			$idUtente=$currentUser->id;
		}
		if (isset ( $idUtente ) ) {
			$model = $this->getInstance ( 'proposta', 'caiModel' );
			$proposta = $model->getPropostaById ($idProposta);
			if (isset ( $proposta )) {
				if (trim ( $idUtente ) == trim ( $proposta->ddg1 ) || trim ( $idUtente ) == trim ( $proposta->ddg2 ) || trim ( $idUtente ) == trim ( $proposta->ddg3 ) || trim ( $idUtente ) == trim ( $proposta->ddg4 ) || trim ( $idUtente ) == trim ( $proposta->ddg5 )) {
					return true;
				}
			}
		}
		return false;
	}
	public function isSegretario() {
		$idUtente=null;
		$currentUser=$this->getCurrentUser();
		if($currentUser!=null){
			$idUtente=$currentUser->id;
		}
		if (isset ( $idUtente )) {
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ('*');
			$query->from ( $db->quoteName ( '#__cai_segretario' ) );
			$idUtente=intval($idUtente);
			$query->where ( "id='$idUtente'" );
			$db->setQuery ( $query );
			$utente = $db->loadObject ();
			if (isset ( $utente )) {
				return true;
			}
		}
		return false;
	}

	public function isLogged() {
		$u=$this->getCurrentUser();
		if($u!=null){
			return true;
		}
		return false;
	}

	public function getStatoLabel($stato) {
		if ($stato == 0) {
			return "Non Iscritto";
		}
		if ($stato == 1) {
			return "Iscritto";
		}
		return null;
	}

	public function getNucleoFamiliare($id){
		if($id!=null){
			//prendo il cf dell'utente
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query->select(" CF ");
			$query->from(" #__cai_utente ");
			$id=intval($id);
			$query->where(" id = $id ");
			$db->setQuery($query);
			$cf=$db->loadObject();
			if(isset($cf) && trim($cf->CF)!=''){
				//prendo i cf dei famigliari
				$cf->CF=$db->escape($cf->CF);
				$db->setQuery("SELECT id1 as CF FROM #__cai_familiare WHERE id2 ='$cf->CF'
						UNION 
						SELECT id2 as CF FROM #__cai_familiare WHERE id1='$cf->CF'");
				$cfFamigliari= $db->loadObjectList();
				//prendo l'utente relativo a ogni cf
				$query=$db->getQuery(true);
				$query->select("*");
				$query->from("#__cai_utente");
				$where= "( CF='$cf->CF' ";
				for($i=0; $i< count($cfFamigliari); $i++){
					$cf=$cfFamigliari[$i]->CF;
					$cf=$db->escape($cf);
					$where= $where." OR CF='$cf' ";
				}
				$where.=" ) AND stato=1 ";
				$query->where($where);
				$db->setQuery($query);
				return $db->loadObjectList();
			}
		}
	}

	public function isFamiliari($id1, $id2){
		if($id1!=null && $id2!=null){
			$db=JFactory::getDbo();
			$id1=$db->escape($id1);
			$id2=$db->escape($id2);
			$db->setQuery("SELECT COUNT(*) FROM #__cai_familiare WHERE (id1='$id1' AND id2='$id2') OR (id2='$id1' AND id1='$id2')");
			$i=$db->loadResult();
			if($i !=null && $i>0){
				return true;
			}
		}
		return false;
	}
	public function getUtentiByText($text){
		
		if($text==null || strlen($text)<3){
			return null;
		}
		else{
			$db=JFactory::getDbo();
			$parole=explode(" ", $text);
			$where="WHERE ";
			foreach ($parole as $key=> $parola){
				$parola=$db->escape($parola);
				if($key==0){
					$where.=" ( nome LIKE '%$parola%' OR cognome LIKE '%$parola%') ";
				}
				else{
					$where.= " AND ( nome LIKE '%$parola%' OR cognome LIKE '%$parola%') ";
				}
			}
			$db->setQuery("SELECT * FROM #__cai_utente $where ");
			return $db->loadObjectList();
		}
	}

	public function getUserByCF($cf){
		if($cf!=null){
			$db=JFactory::getDbo();
			$cf=$db->escape(trim(strtoupper($cf)));
			$db->setQuery("SELECT * FROM #__cai_utente WHERE CF='$cf'");
			return $utente=$db->loadObject();
		}
	}

	public function getUserByUsername($un){
		if($un!=null ){
			$db=JFactory::getDbo();
			$un=trim(strtolower($un));
			$un=$db->escape($un);
			$db->setQuery("SELECT * FROM #__cai_utente WHERE username='$un'");
			return $utente=$db->loadObject();
		}
	}

	
	public function invioEmailRecupero($mail, $utente){
		if($utente->id != null){
			$u=$this->getUserById($utente->id);
			if($u!=null){
				$new=JUserHelper::genRandomPassword();
				$u->password=JUserHelper::hashPassword($new);
				$updated=JFactory::getDbo()->updateObject("#__cai_utente", $u, "id");
				if($updated===true){
					$messaggio="<h2>Recupero password account socio CAI-Perugia</h2>
					<br>
					<div>I nostri sistemi hanno ricevuto una richiesta di recupero password per quanto riguarda l'account di $utente->nome $utente->cognome</div>
					<div>Username: <b> $utente->username </b> <br>
					Password: <b> $new </b> <br>
					</div>
					";
					return $this->getInstance("Email", "CAIModel")->invia($mail, "Recupero Password CAI Sezione di Perugia", $messaggio, "loryzizu@gmail.com");
				}
			}
		}
		return false;
	}
	
	public function cambiaPassword($id, $vecchia, $nuova, $nuovaC){
		if($id!=null){
			if ($this->isSegretario () || $this->getCurrentUser()->id==$id) {
				if($nuova==null || strlen($nuova)<5 || strlen($nuova)>20){
					return "La password non può essere più corta di 5 caratteri o più lunga di 10.";
				}
				if(trim($nuova)!= trim($nuovaC)){
					return "I valori inseriti per la password non coincidono.";
				}
				$user=$this->getUserById($id);
				if($vecchia==null || !JUserHelper::verifyPassword($vecchia, $user->password)){
					return "Il valore inserito per la vecchia password non è valido.";
				}
				$newPassword=JUserHelper::hashPassword($nuova);
				$user->password=$newPassword;
				$db=JFactory::getDbo();
				$result=$db->updateObject("#__cai_utente", $user, "id");
				return $result;
			}
		}
	}
}
class Utente {
	public $id;
	public $cf;
	public $username;
	public $password;
	public $email;
	public $nome;
	public $cognome;
	public $titoli;
	public $data_nascita;
	public $telefono;
	public $cellulare;
	public $professione;
	public $note;
	public $foto;
	public $stato;
	public $salt;
}
