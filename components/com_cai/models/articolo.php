<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelArticolo extends JModelItem {

	public function cancellaArticolo($id) {
		if ($id != null) {
			$db = JFactory::getDbo ();
			$id=$db->escape($id);
			$id= intval($id);
			if($id==0 || $id ==1){
				return false;
			}
			$query = $db->getQuery ( true );
			$query->delete ( '#__content' );
			$query->where ( "id = '$id'" );
			$db->setQuery ( $query );
			return $db->execute ();
		}
		return false;
	}

    public function nascondiArticolo($id) {
        if ($id != null) {
            $db = JFactory::getDbo ();
            $id=$db->escape($id);
            $id= intval($id);
            if($id==0 || $id ==1){
                return false;
            }
            $query = $db->getQuery ( true );
            $query->update( '#__content' );
            $query->set($db->quoteName('state') . ' = \'-2\'' );
            $query->where ( "id = '$id'" );
            $db->setQuery ( $query );
            return $db->execute ();
        }
        return false;
    }

    public function nascondiArticoloDaHompageSeSvolto($id) {
        if ($id != null) {
            $db = JFactory::getDbo();
            $id = $db->escape($id);
            $id = intval($id);
            if ($id == 0 || $id == 1) {
                return false;
            }
            JModelLegacy::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_content/models' );
            /**@var ContentModelArticle $art_model*/
            $art_model = $this->getInstance( 'Article', 'ContentModel' );
            $art_model->featured(array($id),0);
        }

    }


    /**
     * @param $proposta
     *
     * @return bool|mixed
     *
     * @since version
     */
    public function creaOModificaArticolo($proposta) {
        $db = JFactory::getDBO ();
        $model = $this->getInstance ( 'utils', 'caiModel' );
        $propostaModel=$this->getInstance ( 'Proposta', 'CaiModel' );
        JTable::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_content/tables' );
        JModelLegacy::addIncludePath( JPATH_ADMINISTRATOR . '/components/com_content/models' );
	    /**@var ContentModelArticle $art_modelF*/
        $art_model = $this->getInstance( 'Article', 'ContentModel' );

		$df=$db->getDateFormat();
		$nullDate=$db->getNullDate();

        $newArticle = new stdClass();
        $oldArticle=null;
        /**@var CAIModelProposta $propostaModel*/
        if($proposta->id != null && $proposta->id !=0){
            $articleIdTemp=$propostaModel->getPropostaById($proposta->id)->id_articolo;
            if($articleIdTemp!=null){
                $oldArticle=$art_model->getItem($articleIdTemp);
                if($oldArticle!=null){
                    $newArticle->id=(int)$articleIdTemp;
                    $newArticle->alias =$oldArticle->alias;
                }
            }
            else{
                $timezone = date_default_timezone_get ();
                $tz=date_default_timezone_set($timezone);
                $dateC = date ( 'dmYhis', time () );
                $alias=str_replace ( " ", "", $proposta->titolo . $dateC );
                $alias = preg_replace("/[^a-zA-Z0-9]+/", "", $alias);
                $alias=strtolower($alias);
                $newArticle->alias = $alias;
            }
        }


		$mask = new ArticoloMask ();
		$mask = $this->setMask ( $mask, $proposta );
		if (! $mask) {
			return false;
		}
		$newArticle->title = $proposta->titolo;
		$newArticle->catid = $mask->categoriaArticoloJoomla;
		$newArticle->access = 1;

		$newArticle->state=1;

        /*data creazione articolo*/
        $data_creazione_for_db=$model->convertDateFormat($proposta->data_inizio);
		if(isset($proposta->pubblicazione_homepage) && trim($proposta->pubblicazione_homepage)!=''){
            $data_creazione_for_db=$model->convertDateFormat($proposta->pubblicazione_homepage);

		}
        $newArticle->created= $data_creazione_for_db;


		$newArticle->introtext = '

				<link rel="stylesheet" type="text/css" href="media/com_cai/css/style.css">

				<table class="tabIntro">
                            <tbody>
    							 <tr>
	                                <td class="introLogo">
									</td>
	                                <td>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td class="introLogo"><img src="'.$mask->immagine.'" class="imgIntro"  /></td>
	                                <td>
	                                	<p class="introData"><span class="etichetta">Data: </span>' . $this->getDateEuropeanFormat ( $proposta->data_inizio ) . '<span style="display:' . $mask->data_fine . '"> - ' . $this->getDateEuropeanFormat ( $proposta->data_fine ) . '</span></p>
	                                    <p class="introTitolo">' . $proposta->titolo . '</p>
			                            <span class="etichetta"  >Descrizione: </span> <span>' . substr(html_entity_decode($proposta->descrizione_stripped),0,100).' Continua...</span>
			                            <br/>
			                             <form  method="POST" style="display:inline-block">
			                                <input type="submit" value="Iscriviti" name="iscriviti">
			                                <input type="hidden" value="' . $proposta->id . '" name="id">
	                                		<input name="task" type="hidden" value="iscrizione.display" />
											<input name="option" type="hidden" value="com_cai" />
			                            </form>
	                                </td>
	                            </tr>



                            </tbody>
                        </table>';
		$newArticle->fulltext=$this->getFullTextArticle($proposta, $mask);
		/*$dbo = JFactory::getDbo ();
		$result = $dbo->insertObject ( '#__content', $newArticle );
		if (! $result) {
			return false;
		}
		return $dbo->insertid ();*/
		

		$data= (array) $newArticle;
		if( !$art_model->save( $data ) )
		{
			return false;
		}
		$string=$db->escape($newArticle->alias);
		
		$db->setQuery("SELECT id FROM #__content WHERE alias='$string' ");
		$id=$db->loadResult();

        if($proposta->stato==2){
            $art_model->featured(array($id),0);
        }
        else{
            $art_model->featured(array($id),1);
        }

		return $id;
	}

	private function getFullTextArticle($proposta, $mask){
		$userModel=$this->getInstance('utente','caiModel');
		return "

<script>
 function printLocandina(){
    var body=jQuery(\"body\");
    var content=jQuery(\"#content\");
    var bodyChildren = body.find(\"*\");
    var contentAncestors=content.parents();
    var contentChildren=content.find(\"*\");
    for (var i = bodyChildren.length; i--;) {
        var currentNode= bodyChildren[i];
        var isContentAncestorOrChild=false;
        
        if(currentNode.id==\"content\"){
            isContentAncestorOrChild=true;
        }
        
        if(!isContentAncestorOrChild){
            for (var j = contentAncestors.length; j--;) {
                var currentContentAncestor=contentAncestors[j];
                if(currentContentAncestor==currentNode){
                    isContentAncestorOrChild=true;
                    break;
                }
            } 
        }
        
        if(!isContentAncestorOrChild){
            for (var k = contentChildren.length; k--;) {
                var currentContentChild=contentChildren[k];
                if(currentContentChild==currentNode){
                    isContentAncestorOrChild=true;
                    break;
                }
            } 
        }
        
        if(!isContentAncestorOrChild){
            currentNode.addClass(\"hideOnPrint\");
        }
    }
    print();
 }

</script>
<style>
@media print {
  .hideOnPrint{
    display: none !important;
  }
  .slideshowCell{
    /*visibility:collapse !important;*/
  }
}
</style>
<div id='section-to-print'>

				<table  class=\"tabellaArticolo\" >
		<tr  style=\"border:0px solid white\">
		<td id=\"logoCaiArticolo\" style=\"border:0px solid white\" colspan=\"3\"><img src=\"media/com_cai//loghi/logo_cai_articolo.jpg\"/></td>
		</tr>
		<tr>
		<td id=\"titoloArticolo\" class=\"bordoEPadding\" colspan=\"3\"
		style=\"padding-top:50px;padding-bottom:50px;
		padding-left:10px;padding-right:10px;\">
		$proposta->titolo
		</td>
		</tr>
		<!-- SPACER -->
		<tr style=\"border:0px solid white\">
		<td  style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
		</td>
		</tr>
		<tr  >
		<td id=\"dataArticolo\" class=\"bordoEPadding\"  colspan=\"1\">
		<span class=\"etichetta\" >Data: </span>".$this->getDateEuropeanFormat ( $proposta->data_inizio )."<span style=\"display:$mask->data_fine\">-".$this->getDateEuropeanFormat ( $proposta->data_fine )."</span>
		</td>
		<td  class=\"bordoEPadding\"    style=\"max-width:50%;\" colspan=\"2\">
		<span class=\"etichetta\" >
    										Categoria:
    									</span> $mask->tipologiaNome<span style=\"display:$mask->gruppo\">, $mask->gruppoNome </span>
		</td>

		</tr>

		<!-- SPACER -->
		<tr style=\"border:0px solid white\">
		<td  style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
		</td>
		</tr>
		<tr  >
		<td   class=\"bordoEPadding\"  rowspan=\"$mask->rowSpanOrganizzatore\">
		<span class=\"etichetta\">Organizzatori</span>
		</td>
		<td   class=\"bordoEPadding\"  colspan=\"2\">
		<span>".$userModel->getCognomeNomeById($proposta->ddg1)."</span>
		</td>
		</tr>
		<tr style=\"display:$mask->ddg2\" >
		<td   class=\"bordoEPadding\"  colspan=\"2\">
		<span >".$userModel->getCognomeNomeById($proposta->ddg2)."</span>
		</td>
		</tr>
		<tr style=\"display:$mask->ddg3\"  >
		<td   class=\"bordoEPadding\"  colspan=\"2\">
		<span>".$userModel->getCognomeNomeById($proposta->ddg3)."</span>
		</td>
		</tr>
		<tr style=\"display:$mask->ddg4\"  >
		<td   class=\"bordoEPadding\"  colspan=\"2\">
		<span>".$userModel->getCognomeNomeById($proposta->ddg4)."</span>
		</td>
		</tr>
		<tr style=\"display:$mask->ddg5\"  >
		<td  class=\"bordoEPadding\"   colspan=\"2\">
		<span>".$userModel->getCognomeNomeById($proposta->ddg5)."</span>
		</td>
		</tr>
		<!-- SPACER -->
		<tr style=\"border:0px solid white\">
		<td  style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
		</td>
		</tr>















	<tr class=\"slideshowCell\" style=\"display:$mask->immagini\">
	<td  class=\"bordoEPadding slideshowCell\"  colspan=\"3\">
	<section class=\"demo\">
	<button class=\"nextSS\">Next</button>
	<button class=\"prevSS\">Previous</button>
	</section>
	<div class=\"containerSS\">
	$mask->immaginiStringAppend
		</div>
		</td>
		</tr>




























	<!-- SPACER -->
	<tr style=\"border:0px solid white\">
	<td style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
	</td>
	</tr>

	<tr style=\"display:$mask->descrizione\"  >
	<td   class=\"bordoEPadding\"   colspan=\"3\">
	<span class=\"etichetta\"  >Descrizione: </span> <span>$proposta->descrizione</span>
	</td>
	</tr>
	<!-- SPACER -->
	<tr style=\"border:0px solid white\">
	<td style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
	</td>
	</tr>
	<tr  style=\"display:$mask->disableRow1;border:0px;padding:0;margin: 0px;\">
	<td   colspan=\"3\" class=\"tdTableContainer\">
	<table class=\"tabellaArticolo nestedTable\">
	<tr style=\"width:100%\" >
	<td  class=\"bordoEPadding\" style=\"width:100%;display:$mask->lunghezza; \" >
	<span class=\"etichetta\" >Lunghezza Percorso: </span> $proposta->lunghezza

	</td>
	<td  class=\"bordoEPadding\" style=\"width:100%;display:$mask->dislivello\" >
	<span class=\"etichetta\" >Dislivello: </span> $proposta->dislivello
	</td>
	<td  class=\"bordoEPadding\" style=\"width:100%;display:$mask->durata\"  >
	<span class=\"etichetta\" >Durata stimata: </span> $proposta->durata
	</td>
	</tr>
	</table>
	</td>

	</tr>

	<tr  style=\"display:$mask->disableRow2;border:0px;padding:0;margin: 0px;\">
	<td colspan=\"3\" class=\"tdTableContainer\">
	<table class=\"tabellaArticolo nestedTable\">
	<tr style=\"width:100%\" >
	<td  class=\"bordoEPadding\" style=\"width:100%;display:$mask->difficolta\" >
	<span class=\"etichetta\"  >Difficoltà: </span> $proposta->difficolta
	</td>

	<td  class=\"bordoEPadding\" style=\"width:100%;display:$mask->indicazioni\" >
	<span class=\"etichetta\"  >Indicazioni:</span> $proposta->indicazioni
	</td>
	</tr>
	</table>
	</td>
	</tr>


	<!-- SPACER -->
	<tr style=\"border:0px solid white\">
	<td style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
	</td>
	</tr>


	<tr style=\"display:$mask->modalita\">
	<td class=\"bordoEPadding\" >
	<span class=\"etichetta\" >Modalità  e mezzi: </span>
	</td>
	<td    class=\"bordoEPadding\"  colspan=\"2\">
	<span>$proposta->modalita </span>
	</td>
	</tr>
	<tr style=\"display:$mask->appuntamento\">
	<td class=\"bordoEPadding\" >
	<span class=\"etichetta\" >Appuntamento </span>
	</td>
	<td    class=\"bordoEPadding\"  colspan=\"2\"><span>$proposta->appuntamento </span></td>
	</tr>


	<!-- SPACER -->
	<tr style=\"border:0px solid white\">
	<td style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
	</td>
	</tr>



	<tr style=\"display:$mask->prenotazione\" >
	<td   class=\"bordoEPadding\"  colspan=\"3\">
	<span class=\"etichetta\"  >Iscrizione/Prenotazione:</span> $proposta->prenotazione
	</td>
	</tr>
	<tr style=\"display:$mask->approfondimenti\" >
	<td  class=\"bordoEPadding\"   colspan=\"3\">
	<span class=\"etichetta\"  >Approfondimenti: </span> $proposta->approfondimenti
	</td>
	</tr>
	<tr style=\"display:$mask->allegati\" >
	<td  class=\"bordoEPadding\"   colspan=\"3\">
	<span class=\"etichetta\"  >Allegati:</span> $mask->allegatiStringAppend
	</td>
	</tr>
	<!-- SPACER -->
	<tr style=\"border:0px solid white\">
	<td style=\"border:0px solid white;padding:3px;\" colspan=\"3\">
	</td>
	</tr>
	<tr  style=\"display:$mask->disableRow3;border:0px;\">
	<td colspan=\"3\" class=\"tdTableContainer\">
	<table class=\"tabellaArticolo nestedTable\">
	<tr style=\"width:100%\" >
	<td class=\"bordoEPadding\"  style=\"width:100%;display:$mask->costo\" >
	<span class=\"etichetta\"  >Quota di partecipazione: </span> $proposta->costo
	</td>

	<td class=\"bordoEPadding\"  style=\"width:100%;display:$mask->contatti\" >
	<span class=\"etichetta\"  >Contatta gli organizzatori:</span> $proposta->contatti
	</td>
	</tr>
	</table>
	</td>

	</tr>

	</table>




	</div>

	<br><br>
	<form method=\"POST\" style=\"display:inline-block\">
		<input type=\"submit\" value=\"Iscriviti\" name=\"iscriviti\">
		<input type=\"hidden\" value=\"$proposta->id\" name=\"id\">
        <input name=\"task\" type=\"hidden\" value=\"iscrizione.display\" />
		<input name=\"option\" type=\"hidden\" value=\"com_cai\" />
	</form>
	<form method=\"POST\" style=\"display:inline-block\">
		<input type=\"submit\" value=\"Relazione\" name=\"relazione\">
		<input type=\"hidden\" value=\"$proposta->id\" name=\"id\">
        <input name=\"task\" type=\"hidden\" value=\"relazione.visualizza\" />
		<input name=\"option\" type=\"hidden\" value=\"com_cai\" />
	</form>
	<div style=\"width: 100%; text-align: center;\">
	    <a onclick=\"printLocandina()\" style=\"font-size: large;\">Stampa locandina</a>
    </div>
	<div></div>
	<br><br>
	<div
	class=\"fb-like\"
	data-share=\"true\"
	data-width=\"450px\"
	data-show-faces=\"true\"
	style=\"display:none\"
	>

	</div>




	<div class=\"fb-like\" id=\"fbLike\" data-href=\"\"
	data-layout=\"standard\" data-action=\"like\" data-show-faces=\"true\" data-share=\"true\"></div>

	<div class=\"fb-comments\" id=\"fbComment\" data-width=\"100%\" data-order-by=\"social\"

	data-numposts=\"5\" data-colorscheme=\"light\" data-href=\"\" ></div>

	<div class=\"fb-follow\" data-href=\"http://caipg.it/CAI-Perugia\"
	data-width=\"100%\" data-colorscheme=\"light\" data-layout=\"standard\" data-show-faces=\"true\"></div>




<script>
		var currentIndex = 0,
		items = jQuery('.containerSS div'),
		itemAmt = items.length;

		function cycleItems() {
		var item = jQuery('.containerSS div').eq(currentIndex);
		items.hide();
		item.css('display','inline-block');
	}

	var autoSlide = setInterval(function() {
	currentIndex += 1;
	if (currentIndex > itemAmt - 1) {
	currentIndex = 0;
	}
	cycleItems();
	}, 10000);

	jQuery('.nextSS').click(function() {
	clearInterval(autoSlide);
	currentIndex += 1;
	if (currentIndex > itemAmt - 1) {
	currentIndex = 0;
	}
	cycleItems();
	});

	jQuery('.prevSS').click(function() {
	clearInterval(autoSlide);
	currentIndex -= 1;
	if (currentIndex < 0) {
	currentIndex = itemAmt - 1;
	}
	cycleItems();
	});
	</script>


	<script>
	var i=0;
	function changeImage() {
	document.getElementsByTagName('input')[0].focus();
	}
	function commenti() {
	var urlPagina=window.location.href;
	document.getElementById(\"fbComment\").setAttribute(\"data-href\", urlPagina );

	document.getElementById(\"fbLike\").setAttribute(\"data-href\", urlPagina );

	var meta = document.createElement('meta');
	meta.setAttribute(\"property\" , \"og:image\");
	meta.content = \"http://caipg.it/CAI-Perugia/images/loghi/logo_cai.png\";
	document.getElementsByTagName('head')[0].appendChild(meta);
	document.getElementsByTagName('head')[0].setAttribute(\"prefix\", \"og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# cai_perugia: http://ogp.me/ns/fb/cai_perugia#\" );


	}

	window.onload = commenti;
	</script>


	";
	}



	private function setMask($mask, $proposta) {
		$model = $this->getInstance ( 'proposta', 'caiModel' );
		$arrayMask = ( array ) $mask;
		$arrayMask ['colspan1']=0;
		$arrayMask ['colspan2']=0;
		$arrayMask ['colspan3']=0;
		$arrayProposta = ( array ) $proposta;
		if (count ( $arrayMask ) >= count ( $arrayProposta )) {
			foreach ( $arrayProposta as $key => $value ) {
				if ($value != null && trim($value) != '') {
					$arrayMask [$key] = "";
					if ($key == "ddg1" || $key == "ddg2" || $key == "ddg3" || $key == "ddg4" || $key == "ddg5") {
						if($value != null && $value != 'null'){
							$arrayMask [$key] = "table-row";
						}
						else{
							$arrayMask [$key] = "none";
						}
					}
					if ($key == "durata" || $key == "dislivello" || $key == "lunghezza") {
						$arrayMask ['colspan1'] ++;
					}
					if ($key == "difficolta" || $key == "indicazioni" ) {
						$arrayMask ['colspan2'] ++;
					}
					if ($key == "costo" || $key == "contatti" ) {
						$arrayMask ['colspan3'] ++;
					}
					if ($key == "gruppo" && $value == 0 ) {
						$arrayMask [$key] = "none";
					}
				} else {
					$arrayMask [$key] = "none";
				}
			}

			if($arrayMask ['colspan1']==0){
				$arrayMask ['disableRow1']="none";
			}
			if($arrayMask ['colspan2']==0){
				$arrayMask ['disableRow2']="none";
			}
			if($arrayMask ['colspan3']==0){
				$arrayMask ['disableRow3']="none";
			}

			foreach ( $arrayMask as $key => $value ) {
				$mask->$key = $value;
			}

			$gruppo=$model->getGruppo($proposta->gruppo);
			$mask->immagine = $gruppo->logo;
			$mask->gruppoNome = $gruppo->nome;
			$mask->categoriaArticoloJoomla = $gruppo->catid;




			switch($proposta->tipologia){
				case 0:
					$mask->tipologiaNome="Nulla";
					break;
				case 1:
					$mask->tipologiaNome="Alpinismo";
					break;
				case 2:
					$mask->tipologiaNome="Alpinismo Giovanile";
					break;
				case 3:
					$mask->tipologiaNome="Cultura";
					break;
				case 4:
					$mask->tipologiaNome="Eventi";
					break;
				case 5:
					$mask->tipologiaNome="Escursionismo";
					break;
				case 6:
					$mask->tipologiaNome="Speleologia";
					break;
				case 7:
					$mask->tipologiaNome="Arrampicata";
					break;
				case 8:
					$mask->tipologiaNome="Cicloescursionismo";
					break;
				case 9:
					$mask->tipologiaNome="Sciescursionismo";
					break;
				case 10:
					$mask->tipologiaNome="Scialpinismo";
					break;
				case 11:
					$mask->tipologiaNome="CAI-Perugia";
					break;
				case 12:
					$mask->tipologiaNome="Torrentismo";
					break;
				default:
					$mask->tipologiaNome="Nulla";
					break;
			}

			$mask->rowSpanOrganizzatore=1;


			if(isset($proposta->ddg2) && $proposta->ddg2!='null' && trim($proposta->ddg2)!=''){
				$mask->rowSpanOrganizzatore++;
			}
			if(isset($proposta->ddg3) && $proposta->ddg3!='null' && trim($proposta->ddg3)!=''){
				$mask->rowSpanOrganizzatore++;
			}if(isset($proposta->ddg4) && $proposta->ddg4!='null' && trim($proposta->ddg4)!=''){
				$mask->rowSpanOrganizzatore++;
			}if(isset($proposta->ddg5) && $proposta->ddg5!='null' && trim($proposta->ddg5)!=''){
				$mask->rowSpanOrganizzatore++;
			}

			$mask->immagini="none";
			$immaginiList=$model->getImmaginiById($proposta->id);
			$cntImmagini=count($immaginiList);
			for($i=0; $i < $cntImmagini; $i++){
				$nome=$immaginiList[$i]->nome_immagine;
				if($i==0){
					$mask->immagini="";
					$mask->immaginiStringAppend=$mask->immaginiStringAppend.'
	           	<div style="display: inline-block;">
					<img class="slideshow" src="media/com_cai/immagini_proposte/'.$proposta->id.'/'.$nome.'"/>
	            </div>';

				}
				else{
					$mask->immaginiStringAppend=$mask->immaginiStringAppend.'
	           	<div >
					<img class="slideshow" src="media/com_cai/immagini_proposte/'.$proposta->id.'/'.$nome.'"/>
	            </div>';
				}
			}

			$mask->allegati="none";
			$allegatiList=$model->getAllegatiById($proposta->id);
			$cntAllegati=count($allegatiList);
			for($i=0; $i < $cntAllegati; $i++){
				$nome=$allegatiList[$i]->nome_allegato;
					$mask->allegati="";
					$mask->allegatiStringAppend=$mask->allegatiStringAppend.'
							<a target="_blank" href="media/com_cai/allegati_proposte/'.$proposta->id.'/'.$nome.'">'.$nome.'</a><br>';
			}

			return $mask;
		}
		return false;
	}
	public function getDateEuropeanFormat($date) {
		if ($date != null) {
			return date ( 'd/m/Y', strtotime ( $date ) );
		}
	}
}


class Articolo {
	public function __construct() {
		$this->id = null;
		$this->state = 1;
	}
	public $id;
	public $asset_id;
	public $title;
	public $alias;
	public $title_alias;
	public $introtext;
	public $fulltext;
	public $state;
	public $sectionid;
	public $mask;
	public $catid;
	public $created;
	public $created_by;
	public $created_by_alias;
	public $modified;
	public $modified_by;
	public $check_out;
	public $check_out_time;
	public $publish_up;
	public $publish_down;
	public $images;
	public $urls;
	public $attribs;
	public $version;
	public $parentid;
	public $ordering;
	public $metakey;
	public $metadesc;
	public $access;
	public $hits;
	public $metadata;
	public $featured;
	public $language;
	public $xreference;
}
class ArticoloMask {
	public $id;
	public $ddg1;
	public $ddg2;
	public $ddg3;
	public $ddg4;
	public $ddg5;
	public $tipologia;
	public $gruppo;
	public $titolo;
	public $data_inizio;
	public $abilita_data_fine;
	public $data_fine;
	public $appuntamento;
	public $descrizione;
	public $difficolta;
	public $dislivello;
	public $lunghezza;
	public $indicazioni;
	public $durata;
	public $modalita;
	public $costo;
	public $contatti;
	public $approfondimenti;
	public $prenotazione;
	public $id_articolo;
	public $data_proposta;
	public $stato;
	public $pubblicazione;

	public $immagine;
	public $categoriaArticoloJoomla;
	public $gruppoNome;
	public $tipologiaNome;
	public $rowSpanOrganizzatore;
	public $immagini;
	public $allegati;
	public $immaginiStringAppend;
	public $allegatiStringAppend;
	public $colspan1;
	public $colspan2;
	public $colspan3;
	public $disableRow1;
	public $disableRow2;
	public $disableRow3;
}
