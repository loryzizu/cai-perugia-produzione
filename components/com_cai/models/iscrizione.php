<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelIscrizione extends JModelItem {
	private function getPropostaModel() {
		return $this->getInstance ( "proposta", "caiModel" );
	}
	private function getUtenteModel() {
		return $this->getInstance ( "utente", "caiModel" );
	}
	public function iscrivi($idUtente, $idProposta) {
		$u = $this->getUtenteModel ();
		$p = $this->getPropostaModel ();
		$CFDaIscrivere=$u->getUserById($idUtente)->CF;
		if (($idUtente != null && $idProposta != null) && ($u->getCurrentUser()->id == $idUtente || $p->isPropostaModificabileByCurrentUser ( $idProposta ) || $u->isFamiliari ( $CFDaIscrivere, $u->getCurrentUser()->CF ))) {
			$r = $this->controllaSeUtenteIscrittoODDG ( $idUtente, $idProposta );
			if (! $r) {
				$iscrizione = new Iscrizione ();
				$iscrizione->id_proposta = $idProposta;
				$iscrizione->id_utente = $idUtente;
				$db = JFactory::getDbo ();
				$r = $db->insertObject ( "#__cai_iscrizioni", $iscrizione );
				if ($r !== true) {
					$r = "Errore nell'iscrizione dell'utente";
				}
			}
			return $r;
		}
		return "Non hai il permesso di modificare l'iscrizione per questo utente";
	}
	public function iscriviSocioEsterno($idProposta) {
		$p = $this->getPropostaModel ();
		if ($p->isPropostaModificabileByCurrentUser ( $idProposta )) {
			$isc = new IscrizioneSocioEsterno ();
			$isc->id_proposta = $idProposta;
			$isc->nome = JRequest::getVar ( "nome", "" );
			$isc->cognome = JRequest::getVar ( "cognome", "" );
			$isc->data_nascita = JRequest::getVar ( "data_nascita", "" );
			$isc->cellulare = JRequest::getVar ( "cellulare", "" );
			$isc->email = JRequest::getVar ( "email", "" );
			$isc->cf = JRequest::getVar ( "cf", "" );
			$isc->sezione = JRequest::getVar ( "sezione", "" );
			$isc->note = JRequest::getVar ( "note", "" );
			$db = JFactory::getDbo ();
			if (trim ( $isc->nome ) != "" && trim ( $isc->cognome ) != "") {
				return $db->insertObject ( "#__cai_iscrizioni_socio_esterno", $isc );
			}
		}
		return false;
	}
	public function iscriviNonSocio($idProposta) {
		$p = $this->getPropostaModel ();
		if ($p->isPropostaModificabileByCurrentUser ( $idProposta )) {
			$isc = new IscrizioneSocioEsterno ();
			$isc->id_proposta = $idProposta;
			$isc->nome = JRequest::getVar ( "nome", "" );
			$isc->cognome = JRequest::getVar ( "cognome", "" );
			$isc->data_nascita = JRequest::getVar ( "data_nascita", "" );
			$isc->cellulare = JRequest::getVar ( "cellulare", "" );
			$isc->email = JRequest::getVar ( "email", "" );
			$isc->cf = JRequest::getVar ( "cf", "" );
			$isc->note = JRequest::getVar ( "note", "" );
			$db = JFactory::getDbo ();
			if (trim ( $isc->nome ) != "" && trim ( $isc->cognome ) != "") {
				return $db->insertObject ( "#__cai_iscrizioni_non_socio", $isc );
			}
		}
		return false;
	}
	public function cancella($idUtente, $idProposta) {
		$u = $this->getUtenteModel ();
		$p = $this->getPropostaModel ();$CFDaIscrivere=$u->getUserById($idUtente)->CF;
		if (($idUtente != null && $idProposta != null) && ($u->getCurrentUser()->id == $idUtente || $p->isPropostaModificabileByCurrentUser ( $idProposta ) || $u->isFamiliari ( $CFDaIscrivere, $u->getCurrentUser()->CF ))) {
			$r = $this->controllaSeUtenteDDG ( $idUtente, $idProposta );
			if ($r) {
				return "L'utente selezionato è un organizzatore e non può essere cancellata la sua iscrizione";
			}
			$r = $this->controllaSeUtenteIscritto ( $idUtente, $idProposta );
			if (! $r) {
				return "L'utente selezionato non è ancora iscritto";
			}
			$iscrizione = new Iscrizione ();
			$iscrizione->id_proposta = $idProposta;
			$iscrizione->id_utente = $idUtente;
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->delete ( '#__cai_iscrizioni' );
			$idProposta=intval($idProposta);
			$idUtente=intval($idUtente);
			$query->where ( "id_proposta = $idProposta && id_utente=$idUtente" );
			$db->setQuery ( $query );
			return $db->execute ();
		}
		return "Non hai il permesso di modificare l'iscrizione per questo utente";
	}

	public function cancellaSocioEsterno($id){
		if($id!=null){
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->delete ( '#__cai_iscrizioni_socio_esterno' );
			$id=intval($id);
			$query->where ( "id= $id" );
			$db->setQuery ( $query );
			return $db->execute ();
		}
		return false;
	}

	public function cancellaNonSocio($id){
		if($id!=null){
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->delete ( '#__cai_iscrizioni_non_socio' );
			$id=intval($id);
			$query->where ( "id= $id" );
			$db->setQuery ( $query );
			return $db->execute ();
		}
		return false;
	}

	public function controllaSeUtenteIscrittoODDG($idUtente, $idProposta) {
		if ($this->controllaSeUtenteDDG ( $idUtente, $idProposta )) {
			return "L'utente selezionato è un organizzatore";
		} elseif ($this->controllaSeUtenteIscritto ( $idUtente, $idProposta )) {
			return "L'utente selezionato è già iscritto";
		}
		return false;
	}
	public function controllaSeUtenteDDG($idUtente, $idProposta) {
		$db = JFactory::getDbo ();
		$idProposta=intval($idProposta);
		$idUtente=intval($idUtente);
		$db->setQuery ( "SELECT COUNT(*) FROM #__cai_proposta WHERE id=$idProposta AND ( ddg1=$idUtente OR ddg2=$idUtente OR ddg3=$idUtente OR ddg4=$idUtente OR ddg5=$idUtente )" );
		$i = $db->loadResult ();
		if ($i == 1) {
			return true;
		}
		return false;
	}
	public function controllaSeUtenteIscritto($idUtente, $idProposta) {
		$db = JFactory::getDbo ();
		$idProposta=intval($idProposta);
		$idUtente=intval($idUtente);
		$db->setQuery ( "SELECT COUNT(*) FROM #__cai_iscrizioni WHERE id_proposta=$idProposta AND id_utente=$idUtente" );
		$i = $db->loadResult ();
		if ($i == 1) {
			return true;
		}
		return false;
	}
	public function getIscrizioniNonSvolteListByCurrentUser($name, $data_inizio, $data_fine,$page,$size) {
		$u = $this->getUtenteModel ()->getCurrentUser ();
		if (isset ( $u )) {
			return $this->getIscrizioniNonSvolteListByUserId ( $name, $data_inizio, $data_fine, $u->id ,$page,$size);
		}
		return array ();
	}

	public function getIscrizioniNonSvolteListByCurrentUserPages($name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getIscrizioniNonSvolteListByCurrentUser($name, $data_inizio, $data_fine))/$size);
	}

	public function getIscrizioniNonSvolteListByUserId($name, $data_inizio, $data_fine, $idUtente,$page,$size) {
		if ($idUtente != null && trim ( $idUtente ) != '') {
			$utente = $this->getInstance ( "utente", "caiModel" )->getUserById ( $idUtente );
			if ($utente != null) {
				$stato = "Approvata";
				$where = $this->getPropostaModel ()->getFilteredWhereForProposteList ( $stato, $name, $data_inizio, $data_fine );
				$db = JFactory::getDbo ();
				// query iscrizioni
				$query = $db->getQuery ( true );
				$query->select ( "proposta.*" );
				$query->from ( $db->quoteName ( '#__cai_proposta', 'proposta' ) );
				$query->join ( 'INNER', $db->quoteName ( '#__cai_iscrizioni', 'iscritti' ) . " ON proposta.id = iscritti.id_proposta " );
				$idUtente=intval($idUtente);
				$whereIscritti = $where . " AND ( iscritti.id_utente = $idUtente ) ";
				$query->where ( $whereIscritti );
				$query->order ( "proposta.data_inizio DESC" );

				$db->setQuery ( $query );
				$proposte = $db->loadObjectList ();
				if (isset ( $proposte )) {
					if($page!=null && $size!=0){
						$proposte=$this->getInstance ( 'utils', 'caiModel' )->paginate($proposte,$page, $size);
					}
					return $proposte;
				}
			}
		}
		return array ();
	}
	public function getIscrizioniNonSvolteListByUserIdPages($name, $data_inizio, $data_fine, $idUtente,$size) {
		return (int) ceil(count($this->getIscrizioniNonSvolteListByUserId($name, $data_inizio, $data_fine, $idUtente))/$size);
	}



	public function getSociIscrittiEDDGAttivita($id) {
		$proposta = $this->getPropostaModel ();
		if ($id != null) {
			$p = $proposta->getPropostaById ( $id );
			if ($p != null) {
				$ids = array ();
				$cnt = 0;
				if ($p->ddg1 != 0 || $p->ddg1 != null) {
					$ids [$cnt] = $p->ddg1;
					$cnt ++;
				}
				if ($p->ddg2 != 0 || $p->ddg2 != null) {
					$ids [$cnt] = $p->ddg2;
					$cnt ++;
				}
				if ($p->ddg3 != 0 || $p->ddg3 != null) {
					$ids [$cnt] = $p->ddg3;
					$cnt ++;
				}
				if ($p->ddg4 != 0 || $p->ddg4 != null) {
					$ids [$cnt] = $p->ddg4;
					$cnt ++;
				}
				if ($p->ddg5 != 0 || $p->ddg5 != null) {
					$ids [$cnt] = $p->ddg5;
					$cnt ++;
				}
				$cntDDG=$cnt;
				$db = JFactory::getDbo ();
				$query = $db->getQuery ( true );
				$query->select ( "id_utente , date" );
				$query->from ( "#__cai_iscrizioni" );
				$id=intval($id);
				$query->where ( " id_proposta = '$id' " );
                $query->order("date");
				$db->setQuery ( $query );
				$partecipantiIds = $db->loadObjectList ();
                for($i = 0; $i < count ( $partecipantiIds ); $i ++) {
                    $ids [$cnt] = $partecipantiIds [$i]->id_utente;
                    $cnt ++;
                }
                $result = array ();
                $cntRes=0;
                $cntPart=0;
                for($i = 0; $i < count ( $ids ); $i ++) {
                    $p = $this->getUtenteModel ()->getUserById ( $ids [$i]);
                    if ($p != null) {
                        $result [$cntRes] = $p;
                        if($i>=$cntDDG) {
                            $result[$cntRes]->date = $partecipantiIds [$cntPart]->date;
                        }
                        else{
                            $result[$cntRes]->date = "DDG";
                        }
                        $cntRes ++;
                    }
                    if($i>=$cntDDG){
                        $cntPart++;
                    }
				}
				return $result;
			}
		}
	}
	public function getSociDiAltreSezioniIscrittiAttivita($id) {
		if ($id != null) {
			$db = JFactory::getDbo ();
			$id=intval($id);
			$db->setQuery ( "SELECT * FROM #__cai_iscrizioni_socio_esterno WHERE id_proposta=$id ORDER BY date" );
			$r = $db->loadObjectList ();
			return $r;
		}
	}
	public function getNonSociIscrittiAttivita($id) {
		if ($id != null) {
			$db = JFactory::getDbo ();
			$id=intval($id);
			$db->setQuery ( "SELECT * FROM #__cai_iscrizioni_non_socio WHERE id_proposta=$id ORDER BY date" );
			$r = $db->loadObjectList ();
			return $r;
		}
	}
	public function getSociIscrittiEDDGByTipologiaEDataUnici($tipologia, $data_inizio, $data_fine){
		$db=JFactory::getDbo();
		$where="";
		if($tipologia!=null){
			$tipologia=intval($tipologia);
			$where.=" (p.tipologia='$tipologia') ";
		}

		if ($data_inizio != null && $data_fine != null) {
			$valid = $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $data_inizio, $data_fine );
			if ($valid) {
				$data_inizio = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_inizio ));
				$data_fine = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_fine ));
				
				if($where !=""){
					$where=$where." AND ";
				}
				$where = $where . "  (p.data_inizio>= '$data_inizio' AND p.data_inizio <= '$data_fine') ";
			}
		}

		if($where!=""){
			$where= " AND $where";
		}
		$query="SELECT * FROM #__cai_utente WHERE id IN (
				SELECT DISTINCT u.id AS id_utente
				FROM #__cai_partecipanti AS v, #__cai_proposta AS p, #__cai_utente AS u
				WHERE
				v.id_proposta=p.id AND u.id=v.id_utente  $where
		)";

		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function getSociIscrittiEDDGByTipologiaEDataAll($tipologia, $data_inizio, $data_fine){
		$db=JFactory::getDbo();
		$where="";
		if($tipologia!=null){
			$tipologia=intval($tipologia);
			$where.=" (p.tipologia='$tipologia') ";
		}

		if ($data_inizio != null && $data_fine != null) {
			$valid = $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $data_inizio, $data_fine );
			if ($valid) {
				$data_inizio = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_inizio ));
				$data_fine = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_fine ));
				if($where !=""){
					$where=$where." AND ";
				}
				$where = $where . "  (p.data_inizio>= '$data_inizio' AND p.data_inizio <= '$data_fine') ";
			}
		}

		if($where!=""){
			$where= " AND $where";
		}

		$query="SELECT u.id AS id_utente, p.id AS id_proposta, u.stato AS stato_utente, p.stato AS stato_proposta, u.*, p.*
				FROM #__cai_partecipanti AS v, #__cai_proposta AS p, #__cai_utente AS u
				WHERE
				v.id_proposta=p.id AND u.id=v.id_utente  $where
				ORDER BY p.data_inizio DESC
		";

		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function getSociDiAltreSezioniIscrittiByTipologiaEData($tipologia, $data_inizio, $data_fine){
		$db=JFactory::getDbo();
		$where="";
		if($tipologia!=null){
			$tipologia=intval($tipologia);
			$where.=" (p.tipologia='$tipologia') ";
		}

		if ($data_inizio != null && $data_fine != null) {
			$valid = $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $data_inizio, $data_fine );
			if ($valid) {
				$data_inizio = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_inizio ));
				$data_fine = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_fine ));
				if($where !=""){
					$where=$where." AND ";
				}
				$where = $where . " (p.data_inizio>= '$data_inizio' AND p.data_inizio <= '$data_fine') ";
			}
		}

		if($where!=""){
			$where= " AND $where";
		}

		$query="SELECT i.*  FROM #__cai_iscrizioni_socio_esterno AS i
		INNER JOIN #__cai_proposta AS p ON i.id_proposta= p.id
		WHERE (p.stato=1 OR p.stato=2 OR p.stato=4)  $where";
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function getNonSociIscrittiByTipologiaEData($tipologia, $data_inizio, $data_fine){
		$db=JFactory::getDbo();
		$where="";
		if($tipologia!=null){
			$tipologia=intval($tipologia);
			$where.=" (p.tipologia='$tipologia') ";
		}

		if ($data_inizio != null && $data_fine != null) {
			$valid = $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $data_inizio, $data_fine );
			if ($valid) {
				$data_inizio = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_inizio ));
				$data_fine = $db->escape($this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_fine ));
				if($where !=""){
					$where=$where." AND ";
				}
				$where = $where . " (p.data_inizio>= '$data_inizio' AND p.data_inizio <= '$data_fine') ";
			}
		}
		if($where!=""){
			$where= " AND $where";
		}

		$query="SELECT i.*  FROM #__cai_iscrizioni_non_socio AS i
		INNER JOIN #__cai_proposta AS p ON i.id_proposta= p.id
		WHERE  (p.stato=1 OR p.stato=2 OR p.stato=4)  $where ";
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}


class Iscrizione {
	public $id_proposta;
	public $id_utente;
}
class IscrizioneSocioEsterno {
	public $id;
	public $id_proposta;
	public $nome;
	public $cognome;
	public $data_nascita;
	public $cellulare;
	public $email;
	public $cf;
	public $sezione;
	public $note;
}
class IscrizioneNonSocio {
	public $id;
	public $id_proposta;
	public $nome;
	public $cognome;
	public $data_nascita;
	public $cellulare;
	public $email;
	public $cf;
	public $note;
}