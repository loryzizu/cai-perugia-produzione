<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );
require_once JPATH_LIBRARIES . '/cai/PHPExcel/Classes/PHPExcel/IOFactory.php';

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelRinnovo extends JModelItem
{
    private $cntRows = 0;
    private $cntUpdated = 0;
    private $cntInserted = 0;


    public function aggiorna()
    {
        return $this->aggiornaAux();
    }

    public function cambiaAnno()
    {
        return $this->aggiornaAux(true);
    }

    private function aggiornaAux($nuovoAnno = false)
    {

        $arrayFileSoci = $this->fileSociIscrittiToArray();

        if (isset($arrayFileSoci) && count($arrayFileSoci) > 0) {
            $db = JFactory::getDbo();
            $arraySociDaNotificare = null;
            try {
                $db->transactionStart();

                if ($nuovoAnno) {
                    $this->disiscriviTemporaneamenteTuttiTranneSegretario($db);
                }
                $arraySociDaNotificare = $this->aggiungiNonIscritti($arrayFileSoci, $db);

                if ($nuovoAnno) {
                    $this->disiscriviTuttiQuelliCheNonHannoRinnovato($db);
                }

                $db->transactionCommit();

            } catch (Exception $e) {
                // catch any database errors.
                $db->transactionRollback();
                $arraySociDaNotificare = null;
                JErrorPage::render($e);
            }

            if (isset($arraySociDaNotificare) && count($arraySociDaNotificare) > 0) {
                $this->notificaSoci($arraySociDaNotificare);
            }

            return array(
                'cntRows' => $this->cntRows,
                'cntInserted' => $this->cntInserted,
                'cntUpdated' => $this->cntUpdated,
                'cntChangedStatus' => 0
            );

        }
        return false;
    }

    private function fileSociIscrittiToArray()
    {
        if (isset ($_FILES ["file"] ["name"]) && trim($_FILES ["file"] ["name"]) != "") {
            $file = $_FILES ["file"];
            copy($file["tmp_name"], $file["tmp_name"] . ".xls");
            $objPHPExcel = PHPExcel_IOFactory::load($file["tmp_name"] . ".xls");
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            return $sheetData;
        }
    }


    private function disiscriviTemporaneamenteTuttiTranneSegretario($db)
    {
        $db->setQuery("SELECT id FROM #__cai_segretario ");
        $segretari = $db->loadObjectList();

        $where = " WHERE stato=1 AND";
        foreach ($segretari as $key => $value) {
            $value->id = intval($value->id);
            if ($key == 0) {
                $where .= " id != $value->id ";
            } else {
                $where .= " AND id != $value->id ";
            }
        }

        if ($where == "WHERE ") {
            $where = "";
        }

        $db->setQuery("UPDATE #__cai_utente SET stato=3  $where");
        return $db->execute();
    }

    private function disiscriviTuttiQuelliCheNonHannoRinnovato($db)
    {
        $db->setQuery("UPDATE #__cai_utente SET stato=0  WHERE stato=3");
        return $db->execute();
    }


    private function aggiungiNonIscritti($arrayFileSoci, $db)
    {

        $utentiDaNotificare = array();
        $header=$arrayFileSoci[1];
        $columnMap=array();

        foreach ($header as $columnName=>$headerColumn){
            $headerColumn=trim($headerColumn);
            if(strcasecmp($headerColumn,"CODICE FISCALE")===0){
                $columnMap["cf"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"NOME")===0){
                $columnMap["nome"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"COGNOME")===0){
                $columnMap["cognome"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"TITOLO STUDIO")===0){
                $columnMap["titoli"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"DATA DI NASCITA")===0){
                $columnMap["data_nascita"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"EMAIL")===0){
                $columnMap["email"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"TELEFONO")===0){
                $columnMap["telefono"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"CELLULARE")===0){
                $columnMap["cellulare"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"PROFESSIONE")===0){
                $columnMap["professione"]=$columnName;
            }
            else if(strcasecmp($headerColumn,"FAMILIARI")===0){
                $columnMap["familiari"]=$columnName;
            }
        }

        if(count($columnMap)===10){
            foreach ($arrayFileSoci as $row) {
                $this->cntRows++;
                $newUtente = new UtenteRinnovo ();

                $cf = trim($row [$columnMap["cf"]]);
                // se un codice fiscale è != di 16 è errato
                if (strlen($cf) != 16) {
                    continue;
                }
                $newUtente->cf = strtoupper($cf);
                $newUtente->email = trim($row [$columnMap["email"]]);
                if (!filter_var($newUtente->email, FILTER_VALIDATE_EMAIL)) {
                    continue;
                }
                $newUtente->nome = ucfirst(trim($row [$columnMap["nome"]]));
                $newUtente->cognome = ucfirst(trim($row [$columnMap["cognome"]]));

                if ($newUtente->nome == "" || $newUtente->cognome == "") {
                    continue;
                }
                $newUtente->titoli = trim($row [$columnMap["titoli"]]);
                $newUtente->data_nascita = $this->getInstance("Utils", "CAIModel")->convertDateFormat(trim($row [$columnMap["data_nascita"]]));
                $newUtente->telefono = trim($row [$columnMap["telefono"]]);
                $newUtente->cellulare = trim($row [$columnMap["cellulare"]]);
                $newUtente->professione = trim($row [$columnMap["professione"]]);
                $newUtente->stato = 1;
                $familiari = trim($row [$columnMap["familiari"]]);
                $arrayFamiliari = explode(",", $familiari);

                $cf = $db->escape($cf);
                $db->setQuery("SELECT stato, username FROM #__cai_utente WHERE cf='$cf'");
                $result=$db->loadObject();
                if(!isset($result)){
                    $stato=null;
                }
                else {
                    $stato = $result->stato;
                    $username = $result->username;
                }

                if ($stato == null) {
                    //utente mai inserito prima nel sistema
                    $newUtente->username = $this->creaUsername($newUtente->nome, $newUtente->cognome);
                    $passwordArray = $this->creaPassword();
                    $newUtente->password = $passwordArray["crypted"];
                    $newUtente->salt = $passwordArray["salt"];
                    $db->insertObject("#__cai_utente", $newUtente);
                    $this->addFamiliari($arrayFamiliari, $newUtente->cf, $db);
                    $newUtente->passwordPlain=$passwordArray["plain"];
                    $utentiDaNotificare[] = $newUtente;
                    $this->cntInserted++;
                } elseif ($stato == 1 || $stato == 3) {
                    /*
                     * aggiorno utente già iscritto
                     * */
                    $db->updateObject("#__cai_utente", $newUtente, 'cf', false);
                    $this->addFamiliari($arrayFamiliari, $newUtente->cf, $db);
                    $this->cntUpdated++;
                } else {
                    //stato 0, iscritto in passato, cambio solo password
                    $passwordArray = $this->creaPassword();
                    $newUtente->username=$username;
                    $newUtente->password = $passwordArray["crypted"];
                    $newUtente->salt = $passwordArray["salt"];
                    $db->updateObject("#__cai_utente", $newUtente, 'cf', false);
                    $this->addFamiliari($arrayFamiliari, $newUtente->cf, $db);
                    $newUtente->passwordPlain=$passwordArray["plain"];
                    $utentiDaNotificare[] = $newUtente;
                    $this->cntUpdated++;
                }
            }
        }


        return $utentiDaNotificare;
    }

    public function notificaSoci($arraySociDaNotificare){
        /**
         * @var $utente UtenteRinnovo
         * @since version
         */
        foreach ($arraySociDaNotificare as $utente){
            $this->inviaUsernamePasswordEmail($utente->nome." ".$utente->cognome, $utente->username, $utente->passwordPlain, $utente->email);
        }
    }


    private function inviaUsernamePasswordEmail($nomeUtente, $username, $password, $email)
    {
        $messaggio = "<h1>Dati di accesso al sito web</h1>
				<div>Salve $nomeUtente, in seguito alla sua iscrizione alla Sezione di Perugia del CAI, è stato attivato il suo profilo per il sito web
				raggiungibile cliccando <a href=\"http://www.caiperugia.it/\">qui</a>.</div>
				<div>I suoi dati di accesso sono i seguenti:</div>
				<div>Username: <b>$username</b></div>
				<div>Password: <b>$password</b></div>
				<div>Si ricordi di non diffondere questi dati, e di salvarli in un luogo sicuro.
				Sarà possibile cambiare la propria password attraverso l'apposita sezione nell'area \"Profilo\".
				</div>
				
				<div>Cordiali saluti, CAI Perugia.</div>
				";
        $this->getInstance("Email", "CAIModel")->invia($email, "CAI Perugia- Dati accesso sito web", $messaggio);
    }

    private function creaUsername($nome, $cognome)
    {
        $nome = str_replace(" ", "", $nome);
        $cognome = str_replace(" ", "", $cognome);
        $username = $nome . "." . $cognome;
        $username = strtolower($username);
        if (strlen($username) > 25) {
            $username = substr($username, 0, 25);
        }

        $db = JFactory::getDbo();
        $username = $db->escape($username);
        $db->setQuery("SELECT COUNT(*) FROM #__cai_utente WHERE username='$username'");
        $exists = $db->loadResult();
        if ($exists != null && $exists > 0) {
            $valid = false;
            $cnt = 1;
            while (!$valid) {
                $cnt++;
                $usernameTemp = "$username" . "$cnt";
                $usernameTemp = $db->escape($usernameTemp);
                $db->setQuery("SELECT COUNT(*) FROM #__cai_utente WHERE username='$usernameTemp'");
                $exists = $db->loadResult();
                if ($exists != null && $exists == 0) {
                    $valid = true;
                    $username = $usernameTemp;
                }
            }
        }
        return $username;
    }

    private function creaPassword()
    {
        $plainPassword = JUserHelper::genRandomPassword(7);
        $salt = JUserHelper::getSalt();
        $password = JUserHelper::hashPassword($plainPassword);
        $result = array('plain' => $plainPassword, 'crypted' => $password, 'salt' => $salt);
        return $result;
    }

    private function addFamiliari($arrayFamiliari, $cf, $db)
    {
        $familiariObjArray = array();
        $cntF = 0;
        foreach ($arrayFamiliari as $key => $value) {
            if (strlen(trim($value)) != 16) {
                continue;
            }
            $familiare = new Familiari ();
            $familiare->id1 = $cf;
            $familiare->id2 = $value;
            // $familiariObjArray [$cntF] = $familiare;
            // $cntF ++;
            try {
                $db->insertObject("#__cai_familiare", $familiare);
            } catch (RuntimeException $e) {
                continue;
            }
        }
    }
}

class UtenteRinnovo {
	public $id;
	public $cf;
    public $username;
	public $password;
	public $email;
	public $nome;
	public $cognome;
	public $titoli;
	public $data_nascita;
	public $telefono;
	public $cellulare;
	public $professione;
	public $note;
	public $foto;
	public $stato;
}
class Familiari {
	public $id1;
	public $id2;
}

