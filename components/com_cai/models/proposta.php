<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelProposta extends JModelItem {
	public static $PAGE_SIZE=20;

	public $proposta;
	public $propostaErrors;
	public $mode;
	public $visualizza = false;
	public $id;



	public function getPropostaById($id) {
		if (isset ( $id )) {
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ( '*' );
			$query->from ( $db->quoteName ( '#__cai_proposta' ) );
			$id=intval($id);
			$query->where ( "id= '$id'" );
			$db->setQuery ( $query );
			$proposta = $db->loadObject ();
			if (isset ( $proposta )) {
				return $proposta;
			}
		}
		return null;
	}
	public function getPropostaByIdForView($id) {
		if (isset ( $id )) {
			$p = $this->getPropostaById ( $id );
			$p->data_inizio = $this->getDateEuropeanFormat ( $p->data_inizio );
			$p->data_fine = $this->getDateEuropeanFormat ( $p->data_fine );
			if($p->abilita_data_fine == 1){
				$p->abilita_data_fine='on';
			}
			else{
				$p->abilita_data_fine='';
				$p->data_fine = '';
			}
			return $p;
		}
		return null;
	}
	public function getPropostaBkpById($id) {
		if (isset ( $id )) {
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ( '*' );
			$query->from ( $db->quoteName ( '#__cai_proposta_bkp' ) );
			$id=intval($id);
			$query->where ( "id= '$id'" );
			$db->setQuery ( $query );
			$proposta = $db->loadObject ();
			if (isset ( $proposta )) {
				return $proposta;
			}
		}
		return null;
	}
	public function getPropostaBkpByIdForView($id) {
		if (isset ( $id )) {
			$p = $this->getPropostaBkpById ( $id );
			$p->data_inizio = $this->getDateEuropeanFormat ( $p->data_inizio );
			$p->data_fine = $this->getDateEuropeanFormat ( $p->data_fine );
			if($p->abilita_data_fine == 1){
				$p->abilita_data_fine='on';
			}
			else{
				$p->abilita_data_fine='';
				$p->data_fine = '';
			}
			return $p;
		}
		return null;
	}
	public function isPropostaModificabileByCurrentUser($idProposta) {
		$userModel = $this->getInstance ( 'utente', 'caiModel' );
		$modificabile = $userModel->isSegretario () || $userModel->isOrganizzatore ( $idProposta );
		return $modificabile;
	}
	public function getPropostaErrors() {
		if (isset ( $this->propostaErrors )) {
			return $this->propostaErrors;
		}
		return new PropostaErrors ();
	}
	public function getAllegatiById($id) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( "nome_allegato" );
		$query->from ( "#__cai_allegato" );
		$id=intval($id);
		$query->where ( " id_proposta= '$id'" );
		$db->setQuery ( $query );
		return $db->loadObjectList ();
	}
	public function getGruppi(){
		$db = JFactory::getDbo ();
		$db->setQuery("SELECT * FROM #__cai_gruppo WHERE id >-1 ORDER BY id");
		return $db->loadObjectList();
	}
	public function getImmaginiById($id) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( "nome_immagine" );
		$query->from ( "#__cai_immagine" );
		$id=intval($id);
		$query->where ( " id_proposta= '$id'" );
		$db->setQuery ( $query );
		return $db->loadObjectList ();
	}
	public function crea() {
// 		$table = $this->getTable ();
// 		$tableBkp = $this->getTable ( 'PropostaBkp' );
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$propostaValida = $this->controllaProposta ( $propostaRow );
		$propostaRow->data_inizio = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_inizio );
		$propostaRow->data_fine = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_fine );
		if($propostaRow->abilita_data_fine!=null && $propostaRow->abilita_data_fine=='on'){
			$propostaRow->abilita_data_fine=1;
		}
		else{
			$propostaRow->abilita_data_fine=0;
		}
		$id = null;
		if ($propostaValida == true) {
// 			if (! $table->bind ( $propostaRow ) || ! $tableBkp->bind ( $propostaRow )) {
// 				return false;
// 			}
// 			if ($table->check ( $propostaRow ) || $tableBkp->check ( $propostaRow )) {
// 				if (! $tableBkp->save ( $propostaRow )) {
// 					return false;
// 				} else {
// 					$db = JFactory::getDBO ();
// 					$id = $db->insertid ();
// 					$propostaRow->id = $id;
// 					$result = JFactory::getDbo ()->insertObject ( '#__cai_proposta', $propostaRow );
// 					if (! $result) {
// 						return false;
// 					}
// 				}
// 			} else {
// 				return false;
// 			}
			$db = JFactory::getDBO ();
			if($db->insertObject("#__cai_proposta_bkp", $propostaRow)){
				$id = $db->insertid ();
				$propostaRow->id = $id;
				if (! $db->insertObject ( '#__cai_proposta', $propostaRow )) {
					return false;
				}
			}
			else{
				return false;
			}
			$this->fileUpload ( $propostaRow->id );
			return true;
		}
		return false;
	}
	public function salvaModifiche() {
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$id = null;
		if ($propostaRow == null || $propostaRow->id == null) {
			return false;
		}
		$id = $propostaRow->id;
		$propostaOld = $this->getPropostaById ( $id );
		$statoOld = $propostaOld->stato;
		$statoFinale = $statoOld;
		$propostaValida = $this->controllaProposta ( $propostaRow );
		$propostaRow->data_inizio = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_inizio );
		$propostaRow->data_fine = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_fine );
		if($propostaRow->abilita_data_fine!=null && $propostaRow->abilita_data_fine=='on'){
			$propostaRow->abilita_data_fine=1;
		}
		else{
			$propostaRow->abilita_data_fine=0;
		}
        $propostaRow->id_articolo=$propostaOld->id_articolo;

		//Se è svolta e la data di inizio è precedente alla attuale la porto a stato approvata con modifiche
		$today = date ( 'd/m/Y', time () );
		if($statoOld==2 && $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder($today,$this->proposta->data_inizio) ){
			$statoFinale=4;//approvata con modifiche
		}
		if ($propostaValida == true) {
			$propostaRow->stato = $statoFinale;
			$db = JFactory::getDBO ();
			$result = JFactory::getDbo ()->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
			if (! $result) {
				return false;
			}
			$valido = true;
			if (! $this->fileUpload ( $propostaRow->id )) {
				$valido = false;
			}
			if ($statoFinale == 1 || $statoFinale == 2 || $statoFinale == 4) { // se rimane approvata o svolta devo cambiare articolo
				$articoloModel = $this->getInstance ( 'articolo', 'caiModel' );
				$ok = false;
				$idArticolo = $propostaOld->id_articolo;
//				if ($idArticolo != null) {
//					$articoloModel->cancellaArticolo ( $idArticolo );
//				}
				if (($idArticoloNew = $articoloModel->creaOModificaArticolo ( $propostaRow )) != false) {
					$propostaRow->id_articolo = $idArticoloNew;
					$result = JFactory::getDbo ()->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
				} else {
					$valido = false;
				}
			}

			if (! $valido) { // ripristina proposta se ci sono stati errori
				$result = JFactory::getDbo ()->updateObject ( '#__cai_proposta', $propostaOld, 'id', true );
				return false;
			}
			return true;
		}
		return false;
	}
	public function approva() {
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$id = null;
		if ($propostaRow == null || $propostaRow->id == null) {
			return false;
		}
		$id = $propostaRow->id;
		$propostaOld = $this->getPropostaById ( $id );
		$propostaRow = $propostaOld; // Salvo la stessa del database, non considero le modifiche della request
		$statoOld = $propostaOld->stato;
		$statoFinale=-1;
		if ($statoOld == 1 || $statoOld == 4) {
			return true;
		} else if ($statoOld == 0 || $statoOld == 5) {
			$statoFinale = 1;
		} else if ($statoOld == 3) {
			$statoFinale = 4;
		} else {
			$this->propostaErrors->erroreGenerico = "Impossibile approvare questa proposta";
			return false;
		}
		$propostaRow->stato = $statoFinale;
        $propostaRow->id_articolo=$propostaOld->id_articolo;
		$db = JFactory::getDBO ();
		$result = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
		if ($result) {
			// creo articolo, se arrivo in questo punto sicuramente non esisteva precedentemente un articolo per questa gita
			$articoloModel = $this->getInstance ( 'articolo', 'caiModel' );
			if (($idArticolo = $articoloModel->creaOModificaArticolo ( $propostaRow )) != false) {
				$propostaRow->id_articolo = $idArticolo;
				$result = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
				return true;
			}
		}
		return false;
	}
	public function nonApprovare() {
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$id = null;
		if ($propostaRow == null || $propostaRow->id == null) {
			return false;
		}
		$id = $propostaRow->id;
		$propostaOld = $this->getPropostaById ( $id );
		$propostaRow = $propostaOld; // Salvo la stessa del database, non considero le modifiche della request
		$statoOld = $propostaOld->stato;
		$statoFinale=-1;
		if ($statoOld == 3 || $statoOld == 5) {
			return true;
		} else if ($statoOld == 0 || $statoOld == 1 || $statoOld == 2) { // mai modificata o svolta
			$statoFinale = 5;
		} else if ($statoOld == 4) { // modificata
			$statoFinale = 3;
		} else {
			$this->propostaErrors->erroreGenerico = "Impossibile disapprovare questa proposta";
			return false;
		}
        $propostaRow->id_articolo=$propostaOld->id_articolo;
		$propostaRow->stato = $statoFinale;
		$db = JFactory::getDBO ();
		$result = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
		if ($result) {
			$idArticolo = $propostaOld->id_articolo;
			if ($idArticolo != null) {
				// cancello articolo che sicuramente esisteva perchè era approvata
				$articoloModel = $this->getInstance ( 'articolo', 'caiModel' );
				$propostaRow->id_articolo = null;
				$articoloModel->nascondiArticolo ( $idArticolo );
				$result = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
				return $result;
			}
		}
		return false;
	}
	public function approvaESalvaModifiche() {
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$id = null;
		if ($propostaRow == null || $propostaRow->id == null) {
			return false;
		}
		$id = $propostaRow->id;
		$propostaOld = $this->getPropostaById ( $id );
		$statoOld = $propostaOld->stato;
		if ($statoOld != 0 && $statoOld != 1 && $statoOld != 3 && $statoOld != 4 && $statoOld != 5) {
			return false;
		}
		$articoloModel = $this->getInstance ( 'articolo', 'caiModel' );
		$statoFinale = 4;
		$valido = true;
		$valido = $this->controllaProposta ( $propostaRow ); // controllo dati form
		$propostaRow->data_inizio = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_inizio );
		$propostaRow->data_fine = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_fine );
		if($propostaRow->abilita_data_fine!=null && $propostaRow->abilita_data_fine=='on'){
			$propostaRow->abilita_data_fine=1;
		}
		else{
			$propostaRow->abilita_data_fine=0;
		}
		$propostaRow->id_articolo=$propostaOld->id_articolo;
		$db = JFactory::getDBO ();
		if ($valido) { // salvo dati e cambio stato finale
			$propostaRow->stato = $statoFinale;
			$valido = JFactory::getDbo ()->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
		} else {
			return false;
		}
		if ($valido) { // carico i file
			$valido = $this->fileUpload ( $propostaRow->id );
		}
//		if ($valido) { // cancello articolo se necessario
//			if ($statoOld == 1 || $statoOld == 4) { // era approvato, cancello articolo
//				$idArticolo = $propostaOld->id_articolo;
//				if ($idArticolo != null) {
//					// cancello articolo che sicuramente esisteva perchè era approvata
//					$articoloModel->cancellaArticolo ( $idArticolo );
//				}
//			}
//		}
		if ($valido) { // creo  o modifico articolo
			if (($idArticolo = $articoloModel->creaOModificaArticolo ( $propostaRow )) != false) {
				$propostaRow->id_articolo = $idArticolo;
				$valido = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
			}
		}

		if (! $valido) { // riprisitino se ci sono stati errori
			$result = $db->updateObject ( '#__cai_proposta', $propostaOld, 'id', true );
			return false;
		}
		return true;
	}
	public function nonApprovareMaSalvaModifiche() {
		$this->getPropostaFromRequest ();
		$propostaRow = $this->proposta;
		$id = null;
		if ($propostaRow == null || $propostaRow->id == null) {
			return false;
		}
		$id = $propostaRow->id;
		$propostaOld = $this->getPropostaById ( $id );
		$statoOld = $propostaOld->stato;
		$db = JFactory::getDBO ();
		if ($statoOld != 0 && $statoOld != 1 && $statoOld != 3 && $statoOld != 4 && $statoOld != 5 && $statoOld != 2) { // includo anche le svolte
			return false;
		}
		$articoloModel = $this->getInstance ( 'articolo', 'caiModel' );
		$statoFinale = 3;
		$valido = true;
		$valido = $this->controllaProposta ( $propostaRow ); // controllo dati form
		$propostaRow->data_inizio = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_inizio );
		$propostaRow->data_fine = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $this->proposta->data_fine );
		if($propostaRow->abilita_data_fine!=null && $propostaRow->abilita_data_fine=='on'){
			$propostaRow->abilita_data_fine=1;
		}
		else{
			$propostaRow->abilita_data_fine=0;
		}
        $propostaRow->id_articolo=$propostaOld->id_articolo;
		if ($valido) { // salvo dati e cambio stato finale
			$propostaRow->stato = $statoFinale;

			$valido = $db->updateObject ( '#__cai_proposta', $propostaRow, 'id', true );
		} else {
			return false;
		}
		if ($valido) { // carico i file
			$valido = $this->fileUpload ( $propostaRow->id );
		}
		if ($valido) { // cancello articolo se necessario
			if ($statoOld == 1 || $statoOld == 4 || $statoOld == 2) { // era approvato o svolto, cancello articolo
				$idArticolo = $propostaOld->id_articolo;
				if ($idArticolo != null) {
					// cancello articolo che sicuramente esisteva perchè era approvata
					$articoloModel->nascondiArticolo ( $idArticolo );
				}
			}
		}

		if (! $valido) { // riprisitino se ci sono stati errori
			$result = $db->updateObject ( '#__cai_proposta', $propostaOld, 'id', true );
			return false;
		}
		return true;
	}
	public function salva($stato) {
		if ($stato == "salvaModifiche") {
			return $this->salvaModifiche ();
		} elseif ($stato == "approva") {
			return $this->approva ();
		} elseif ($stato == "approvaESalvaModifiche") {
			return $this->approvaESalvaModifiche ();
		} elseif ($stato == "nonApprovareMaSalvaModifiche") {
			return $this->nonApprovareMaSalvaModifiche ();
		} elseif ($stato == "nonApprovare") {
			return $this->nonApprovare ();
		}
		return false;
	}
	public function cestinaProposta($id) {
		$db = JFactory::getDbo ();
		$row = $this->getPropostaById ( $id );
		$row->stato = 6;
		$result = JFactory::getDbo ()->updateObject ( '#__cai_proposta', $row, 'id', true );
	}
	public function getStatoLabel($stato) {
		if ($stato == 0) {
			return "Nuova";
		}
		if ($stato == 1) {
			return "Approvata";
		}
		if ($stato == 2) {
			return "Svolta";
		}
		if ($stato == 3) {
			return "Non approvata con modifiche";
		}
		if ($stato == 4) {
			return "Approvata con modifiche";
		}
		if ($stato == 5) {
			return "Non Approvata";
		}
		if ($stato == 6) {
			return "Cestinata";
		}
	}
	public function getTipologiaLabel($tipologia) {
		switch($tipologia){
			case 0:
				return "Nulla";
				break;
			case 1:
				return "Alpinismo";
				break;
			case 2:
				return "Alpinismo Giovanile";
				break;
			case 3:
				return "Cultura";
				break;
			case 4:
				return "Eventi";
				break;
			case 5:
				return "Escursionismo";
				break;
			case 6:
				return "Speleologia";
				break;
			case 7:
				return "Arrampicata";
				break;
			case 8:
				return "Cicloescursionismo";
				break;
			case 9:
				return "Sciescursionismo";
				break;
			case 10:
				return "Scialpinismo";
				break;
			case 11:
				return "CAI-Perugia";
				break;
			case 12:
				return "Torrentismo";
				break;
			default:
				return "Nessuna";
				break;
		}
	}
	public function getGruppoLabel($gruppo) {
		$db = JFactory::getDbo ();
		$gruppo=intval($gruppo);
		$db->setQuery("SELECT * FROM #__cai_gruppo WHERE id=$gruppo");
		$gruppo=$db->loadObject();
		if($gruppo !=null){
			return $gruppo->nome;
		}
		return null;
	}

	public function getGruppo($id) {
		$db = JFactory::getDbo ();
		$id=intval($id);
		$db->setQuery("SELECT * FROM #__cai_gruppo WHERE id=$id");
		$gruppo=$db->loadObject();
		if($gruppo !=null){
			return $gruppo;
		}
		return null;
	}
	private function getProposteList($stato, $name, $data_inizio, $data_fine, $page,$size) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( "proposta.*" );
		$query->from ( $db->quoteName ( '#__cai_proposta', 'proposta' ) );
		$where = $this->getFilteredWhereForProposteList ( $stato, $name, $data_inizio, $data_fine );
		$query->where ( $where );
		$query->order('data_inizio DESC');
		$db->setQuery ( $query );
		$proposte = $db->loadObjectList ();
		if (isset ( $proposte )) {
			if($page!=null && $size!=0){
				$proposte=$this->getInstance ( 'utils', 'caiModel' )->paginate($proposte,$page, $size);
			}
			return $proposte;
		}
		return array();
	}
	
	public function getProposteListByUserId($stato, $name, $data_inizio, $data_fine, $idUtente,$page, $size) {
		if ($idUtente != null && trim ( $idUtente ) != '') {
			$utente = $this->getInstance ( "utente", "caiModel" )->getUserById ( $idUtente );
			if ($utente != null) {
				$db = JFactory::getDbo ();
				$where = $this->getFilteredWhereForProposteList ( $stato, $name, $data_inizio, $data_fine );
				$idUtente=intval($idUtente);
				$whereIscritti = $where . " AND ( iscritti.id_utente = $idUtente ) ";
				$whereDDG = $where . " AND ( ddg1=$idUtente OR ddg2=$idUtente OR ddg3=$idUtente OR ddg4=$idUtente OR ddg5=$idUtente )";
				$db->setQuery("(SELECT proposta.*
					FROM #__cai_proposta as proposta
					where $whereDDG )
					UNION
					(SELECT proposta.*
					FROM #__cai_proposta as proposta
					INNER JOIN #__cai_iscrizioni as iscritti ON proposta.id = iscritti.id_proposta
					WHERE $whereIscritti)
					ORDER BY 'data_inizio'");
				$proposte = $db->loadObjectList ();
				if (isset ( $proposte )) {
					if($page!=null && $size!=0){
						$proposte=$this->getInstance ( 'utils', 'caiModel' )->paginate($proposte,$page, $size);
					}
					return $proposte;
				}
			}
		}
		return array();
	}

	public function getProposteListByUserIdPages($stato, $name, $data_inizio, $data_fine, $idUtente,$size) {
		return (int) ceil(count($this->getProposteListByUserId($stato, $name, $data_inizio, $data_fine, $idUtente))/$size);
	}



	public function getProposteOrganizzateListAllByUserId($name, $data_inizio, $data_fine, $idUtente,$page,$size) {
		if ($idUtente != null && trim ( $idUtente ) != '') {
			$utente = $this->getInstance ( "utente", "caiModel" )->getUserById ( $idUtente );
			if ($utente != null) {
				$stato="Tutti";
				$where = $this->getFilteredWhereForProposteList ( $stato, $name, $data_inizio, $data_fine );
				$db = JFactory::getDbo ();
				// query iscrizioni
				$query2 = $db->getQuery ( true );
				$query2->select ( "proposta.*" );
				$query2->from ( $db->quoteName ( '#__cai_proposta', 'proposta' ) );
				$whereDDG = $where . " AND ( ddg1=$idUtente OR ddg2=$idUtente OR ddg3=$idUtente OR ddg4=$idUtente OR ddg5=$idUtente )";
				$query2->where ( $whereDDG );
				$query2->order("proposta.data_inizio DESC");

				$db->setQuery ( $query2 );
				$proposte = $db->loadObjectList ();
				if (isset ( $proposte )) {
					if($page!=null && $size!=0){
						$proposte=$this->getInstance ( 'utils', 'caiModel' )->paginate($proposte,$page, $size);
					}
					return $proposte;
				}
			}
		}
		return array();
	}

	public function getProposteOrganizzateListAllByUserIdPages($name, $data_inizio, $data_fine, $idUtente,$size) {
		return (int) ceil(count($this->getProposteOrganizzateListAllByUserId($name, $data_inizio, $data_fine, $idUtente) )/$size);
	}



	public function getFilteredWhereForProposteList($stato, $name, $data_inizio, $data_fine) {
		$stati = array ();
		if ($stato == "Da approvare") {
			$stati [0] = 0;
			$stati [1] = 3;
			$stati [2] = 5;
		} elseif ($stato == "Approvata") {
			$stati [0] = 1;
			$stati [1] = 4;
		} elseif ($stato == "Svolta") {
			$stati [0] = 2;
		} elseif ($stato == "Cestinate") {
			$stati [0] = 6;
		}
		elseif($stato == "Tutti"){
			$stati [0] = 0;
			$stati [1] = 1;
			$stati [2] = 2;
			$stati [3] = 3;
			$stati [4] = 4;
			$stati [5] = 5;
			$stati [6] = 6;
		}
        elseif($stato == "ApprovataOSvolta"){
            $stati [0] = 1;
            $stati [1] = 4;
            $stati [2] = 2;
        }
		else {
			"";
		}
		$statoTemp = $stati [0];
		$statoTemp=intval($statoTemp);
		$where = "( proposta.stato='$statoTemp'";
		for($i = 1; $i < count ( $stati ); $i ++) {
			$statoTemp = $stati [$i];
			$statoTemp=intval($statoTemp);
			$where = $where . " OR  proposta.stato='$statoTemp'";
		}
		$where = $where . ' )';

		if ($name != null) {
			$name = str_replace ( " ", "%", $name );
			$name = "%" . $name . "%";
			$name=JFactory::getDbo()->escape($name);
			$where = $where . " AND (proposta.titolo LIKE '$name' ) ";
		}

		if ($data_inizio != null && $data_fine != null) {
			$valid = $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $data_inizio, $data_fine );
			if ($valid) {
				$data_inizio = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_inizio );
				$data_fine = $this->getInstance ( 'utils', 'caiModel' )->convertDateFormat ( $data_fine );
				$data_inizio=JFactory::getDbo()->escape($data_inizio);
				$data_fine=JFactory::getDbo()->escape($data_fine);
				$where = $where . " AND (proposta.data_inizio>= '$data_inizio' AND proposta.data_inizio <= '$data_fine') ";
			}
		}
		return $where;
	}
	public function getDaApprovare($name, $data_inizio, $data_fine,$page,$size) {
		$stato = "Da approvare";
		return $this->getProposteList ( $stato, $name, $data_inizio, $data_fine , $page, $size);
	}
	public function getDaApprovarePages($name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getDaApprovare($name, $data_inizio, $data_fine,0, $size))/$size);
	}

	public function getProposteListByCurrentUser($stato,$name, $data_inizio, $data_fine,$page,$size) {
		$u = $this->getInstance ( "utente", "caiModel" )->getCurrentUser();
		$proposte=array();
		if (isset ( $u )) {
			$proposte=$this->getProposteListByUserId ( $stato, $name, $data_inizio, $data_fine, $u->id,$page,$size );
		}
		return $proposte;
	}
	public function getProposteListByCurrentUserPages($stato,$name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getProposteListByCurrentUser($stato,$name, $data_inizio, $data_fine))/$size);
	}


	public function getSvolte($name, $data_inizio, $data_fine,$page,$size) {
		$stato = "Svolta";
		return $this->getProposteList ( $stato, $name, $data_inizio, $data_fine,$page,$size);
	}
	public function getSvoltePages($name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getSvolte($name, $data_inizio, $data_fine, 0, $size))/$size);
	}

    public function getSvolteEApprovate($name, $data_inizio, $data_fine,$page,$size) {
        $stato = "ApprovataOSvolta";
		return $this->getProposteList ( $stato, $name, $data_inizio, $data_fine,$page,$size);
    }
    public function getSvolteEApprovateConGPS($name, $data_inizio, $data_fine,$page,$size) {
        $proposteList=$this->getSvolteEApprovate($name, $data_inizio, $data_fine,1,0);
        $cntProposte=count($proposteList);

        $result = array();
        $cntResults = 0;

        for($j=0; $j < $cntProposte; $j++) {

            $allegatiList = $this->getAllegatiById($proposteList[$j]->id);
            $cntAllegati = count($allegatiList);
            $linkArticolo=$this->getLinkArticoloProposta($proposteList[$j]->id);
            if( $linkArticolo!=null){
                for ($i = 0; $i < $cntAllegati; $i++) {
                    $nome = $allegatiList[$i]->nome_allegato;
                    $fileExt = explode(".", $nome)[1];
                    if (strtolower($fileExt) == 'gpx') {
                        $resultElement = $proposteList[$j];
                        $resultElement->linkArticolo=$linkArticolo;
                        $result[$cntResults] = $resultElement;
                        $cntResults++;
                        break;
                    }

                }
            }

        }

        if (isset ( $result )) {
            if($page!=null && $size!=0){
                $result=$this->getInstance ( 'utils', 'caiModel' )->paginate($result,$page, $size);
            }
        }
        return $result;
    }


    public function getSvolteEApprovateConGPSPages($name, $data_inizio, $data_fine,$size) {
        return (int) ceil(count($this->getSvolteEApprovateConGPS($name, $data_inizio, $data_fine, 0, $size))/$size);
    }

	public function getProposteOrganizzateListAllByCurrentUser($name, $data_inizio, $data_fine,$page,$size) {
		$u = $this->getInstance ( "utente", "caiModel" )->getCurrentUser();
		if (isset ( $u )) {
			return $this->getProposteOrganizzateListAllByUserId ($name, $data_inizio, $data_fine, $u->id,$page,$size );
		}
		return array();
	}

	public function getProposteOrganizzateListAllByCurrentUserPages($name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getProposteOrganizzateListAllByCurrentUser($name, $data_inizio, $data_fine,0, $size))/$size);
	}


	public function getApprovate($name, $data_inizio, $data_fine, $page=null, $size=null) {
		$stato = "Approvata";
		return $this->getProposteList ( $stato, $name, $data_inizio, $data_fine, $page, $size);
	}
	public function getApprovatePages($name, $data_inizio, $data_fine,$size) {
		return (int) ceil(count($this->getApprovate($name, $data_inizio, $data_fine))/$size);
	}


	private function fileUpload($id) {
		if ($id != null) {
			$valido = true;
			$allegati = null;
			$immagini = null;
			if (isset ( $_FILES ["allegati"] )) {
				$allegati = $_FILES ["allegati"];
			}
			if (isset ( $_FILES ["immagini"] )) {
				$immagini = $_FILES ["immagini"];
			}
			if ($immagini == null && $allegati == null) {
				return $valido;
			}

			$numAllegati = count ( $allegati ["name"] );
			$numImmagini = count ( $immagini ["name"] );

			$destAllegati = JPATH_ROOT . "/media/com_cai/allegati_proposte/$id";
			$destImmagini = JPATH_ROOT . "/media/com_cai/immagini_proposte/$id";

			if (! file_exists ( $destAllegati )) {
				if (! mkdir ( $destAllegati, 0777, true )) {
					return false;
				}
			}
			if (! file_exists ( $destImmagini )) {
				if (! mkdir ( $destImmagini, 0777, true )) {
					return false;
				}
			}
			$dbo = JFactory::getDbo ();
			if ($numAllegati >= 1) {
				if (isset ( $allegati ["name"] [0] ) && trim ( $allegati ["name"] [0] ) != "") {
					for($i = 0; $i < $numAllegati; $i ++) {
						$name = $allegati ["name"] [$i];
						if (file_exists ( $destAllegati . "/$name" )) {
							$name = "copia" . $name;
						}
						if (! move_uploaded_file ( $allegati ["tmp_name"] [$i], $destAllegati . "/$name" )) {
							return false;
						} else {
							$allegato = new Allegato ();
							$allegato->id_proposta = $id;
							$allegato->nome_allegato = $name;
							$dbo->insertObject ( "#__cai_allegato", $allegato );
						}
					}
				}
			}
			if ($numImmagini >= 1) {
				if (isset ( $immagini ["name"] [0] ) && trim ( $immagini ["name"] [0] ) != "") {
					for($i = 0; $i < $numImmagini; $i ++) {
						$name = $immagini ["name"] [$i];
						if (file_exists ( $destImmagini . "/$name" )) {
							$name = "copia" . $name;
						}
						if (! move_uploaded_file ( $immagini ["tmp_name"] [$i], $destImmagini . "/$name" )) {
							return false;
						} else {
							$immagine = new Immagine ();
							$immagine->id_proposta = $id;
							$immagine->nome_immagine = $name;
							$dbo->insertObject ( "#__cai_immagine", $immagine );
						}
					}
				}
			}

			return true;
		}
		return false;
	}
	public function getPropostaFromRequest() {
		$jinput = JFactory::getApplication ()->input;
		$this->proposta = new Proposta ();
		$id = $jinput->get ( 'idProposta', null );
		if ($id != null) {
			$this->proposta->id = $id;
		}
		$this->proposta->titolo = $jinput->get ( 'titolo', '', 'STRING' );

		$this->proposta->data_inizio = $jinput->get ( 'data_inizio', '', 'STRING' );
		$this->proposta->abilita_data_fine = $jinput->get ( 'abilita_data_fine', '', 'STRING' );
		$this->proposta->data_fine = $jinput->get ( 'data_fine', '', 'STRING' );
		$this->proposta->tipologia = $jinput->get ( 'tipologia', null, 'STRING' );
		$this->proposta->gruppo = $jinput->get ( 'gruppo', '', 'STRING' );

		$this->proposta->descrizione = '';
		$descrizione = $jinput->get ( 'descrizione', '', 'ARRAY' );
		if (isset ( $descrizione [0] ) && count ( $descrizione ) > 0) {
			$this->proposta->descrizione = $descrizione [0];
			$this->proposta->descrizione_stripped=strip_tags($this->proposta->descrizione);
		}

		$this->proposta->ddg1 = $jinput->get ( 'ddg1', null, 'STRING' );
		$this->proposta->ddg2 = $jinput->get ( 'ddg2', null, 'STRING' );
		$this->proposta->ddg3 = $jinput->get ( 'ddg3', null, 'STRING' );
		$this->proposta->ddg4 = $jinput->get ( 'ddg4', null, 'STRING' );
		$this->proposta->ddg5 = $jinput->get ( 'ddg5', null, 'STRING' );
		if($this->proposta->ddg1==null){
			$this->proposta->ddg1=null;
		}
		if($this->proposta->ddg2==null){
			$this->proposta->ddg2=null;
		}
		if($this->proposta->ddg3==null){
			$this->proposta->ddg3=null;
		}
		if($this->proposta->ddg4==null){
			$this->proposta->ddg4=null;
		}
		if($this->proposta->ddg5==null){
			$this->proposta->ddg5=null;
		}

		$this->proposta->contatti = '';
		$contatti = $jinput->get ( 'contatti', '', 'ARRAY' );
		if (isset ( $contatti [0] ) && count ( $contatti ) > 0) {
			$this->proposta->contatti = $contatti [0];
		}

		$this->proposta->difficolta = $jinput->get ( 'difficolta', '', 'STRING' );
		$this->proposta->dislivello = $jinput->get ( 'dislivello', '', 'STRING' );
		$this->proposta->lunghezza = $jinput->get ( 'lunghezza', '', 'STRING' );
		$this->proposta->durata = $jinput->get ( 'durata', '', 'STRING' );
		$this->proposta->indicazioni = $jinput->get ( 'indicazioni', '', 'STRING' );

		$this->proposta->appuntamento = '';
		$appuntamento = $jinput->get ( 'appuntamento', '', 'ARRAY' );
		if (isset ( $appuntamento [0] ) && count ( $appuntamento ) > 0) {
			$this->proposta->appuntamento = $appuntamento [0];
		}

		$this->proposta->modalita = $jinput->get ( 'modalita', '', 'STRING' );
		$this->proposta->costo = $jinput->get ( 'costo', '', 'STRING' );

		$this->proposta->approfondimenti = '';
		$approfondimenti = $jinput->get ( 'approfondimenti', '', 'ARRAY' );
		if (isset ( $approfondimenti [0] ) && count ( $approfondimenti ) > 0) {
			$this->proposta->approfondimenti = $approfondimenti [0];
		}

		$this->proposta->prenotazione = '';
		$prenotazione = $jinput->get ( 'prenotazione', '', 'ARRAY' );
		if (isset ( $prenotazione [0] ) && count ( $prenotazione ) > 0) {
			$this->proposta->prenotazione = $prenotazione [0];
		}

		$this->proposta->pubblicazione_homepage = $jinput->get ( 'pubblicazioneHomepage', '', 'STRING' );
		return $this->proposta;
	}
	private function controllaProposta($proposta) {
		$this->propostaErrors = new PropostaErrors ();
		if (isset ( $this->proposta )) {
			$valida = true;
			$dataIniziale = true;
			if (trim ( ($proposta->titolo) ) == '') {
				$valida = false;
				$this->propostaErrors->titolo = "Il titolo è obbligatorio";
			}
			if (trim ( ($proposta->data_inizio) ) == '') {
				$valida = false;
				$this->propostaErrors->data = "La data di svolgimento è obbligatoria";
				$dataIniziale = false;
			}
			if ($dataIniziale && (trim ( ($proposta->abilita_data_fine) ) == 'on' && ! $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder ( $proposta->data_inizio, $proposta->data_fine ))) {
				$valida = false;
				$this->propostaErrors->data = "La data di fine svolgimento è antecedente alla data di inizio";
			}
			if (!isset ($proposta->tipologia) || ($proposta->tipologia) == "NULL" || ($proposta->tipologia) > 12 || ($proposta->tipologia) < 0) {
				$valida = false;
				$this->propostaErrors->tipologia = "La tipologia è obbligatoria";
			}
			if (! $this->controllaDDG ()) {
				$valida = false;
				$this->propostaErrors->ddg = "Hai ripetuto più volte la stessa persona come organizzatore";
			}
			if (! $this->controllaFileCaricati ()) {
				$valida = false;
			}
			if ($valida) {
				return $valida;
			}
		}
		return false;
	}
	private function controllaFileCaricati() {
		$valido = true;
		$allegati = null;
		$immagini = null;
		if (isset ( $_FILES ["allegati"] )) {
			$allegati = $_FILES ["allegati"];
		}
		if (isset ( $_FILES ["immagini"] )) {
			$immagini = $_FILES ["immagini"];
		}
		if ($immagini == null && $allegati == null) {
			return $valido;
		}
		$id = JRequest::getVar ( 'idProposta', null );
		$cntOldAllegati = 0;
		$cntOldImmagini = 0;
		if ($id != null) {
			$this->proposta->id = $id;
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ( '*' );
			$query->from ( $db->quoteName ( '#__cai_allegato' ) );
			$id=intval($id);
			$query->where ( "id_proposta= '$id'" );
			$db->setQuery ( $query );
			$allegatiRow = $db->loadObjectList ();
			if ($allegatiRow != null) {
				$cntOldAllegati = count ( $allegatiRow );
			}

			$query = $db->getQuery ( true );
			$query->select ( '*' );
			$query->from ( $db->quoteName ( '#__cai_immagine' ) );
			$id=intval($id);
			$query->where ( "id_proposta= '$id'" );
			$db->setQuery ( $query );
			$immaginiRow = $db->loadObjectList ();
			if ($immaginiRow != null) {
				$cntOldImmagini = count ( $immaginiRow );
			}
		}
		if ($allegati != null) {
			$numAllegati = count ( $allegati ["name"] );
			if ($numAllegati >= 1) {
				if (isset ( $allegati ["name"] [0] ) && trim ( $allegati ["name"] [0] ) != "") {
					if ($numAllegati + $cntOldAllegati <= 5) {
						for($i = 0; $i < $numAllegati; $i ++) {
							$size = $allegati ["size"] [$i];
							$type = trim ( $allegati ["type"] [$i] );
							if ($size > 10000000) { // 10mB
								$valido = false;
								$this->propostaErrors->allegati = "Le dimensioni di un allegato sono troppo grandi";
							}
						}
					} else {
						$valido = false;
						$this->propostaErrors->allegati = $this->propostaErrors->allegati . "Hai inserito $numAllegati allegati, $cntOldAllegati sono stati inseriti in precedenza per questa
						proposta, il limite massimo di allegati per ogni proposta è 5. Puoi cancellare i vecchi allegati.";
					}
				}
			}
		}

		if ($immagini != null) {
			$numImmagini = count ( $immagini ["name"] );
			if ($numImmagini >= 1) {
				if (isset ( $immagini ["name"] [0] ) && trim ( $immagini ["name"] [0] ) != "") {
					if ($numImmagini + $cntOldImmagini <= 3) {
						for($i = 0; $i < $numImmagini; $i ++) {
							$size = $immagini ["size"] [$i];
							$type = trim ( $immagini ["type"] [$i] );
							if ($type != "image/jpeg" && $type != "image/gif" && $type != "image/bmp" && $type != "image/png") {
								$valido = false;
								$this->propostaErrors->immagini = $this->propostaErrors->immagini . "Formato di immagine non valido, supportati solo i formati jpeg, jpg, gif, png, bmp";
								return $valido;
							} else {
								if ($size > 10000000) {
									$valido = false;
									$this->propostaErrors->immagini = $this->propostaErrors->immagini . "Le dimensioni di un'immagine sono troppo grandi";
									return $valido;
								}
							}
						}
					} else {
						$valido = false;
						$this->propostaErrors->immagini = $this->propostaErrors->immagini . "Hai inserito $numImmagini immagini, $cntOldImmagini sono state inserite in precedenza per questa
						proposta, il limite massimo di immagini per ogni proposta è 3. Puoi cancellare le vecchie immagini.";
					}
				}
			}
		}
		return $valido;
	}
	public function getDateEuropeanFormat($date) {
		if ($date != null) {
			$str=strtotime ( $date );
			$data=date ( 'd/m/Y', $str );
			return $data;
		}
	}
	private function controllaDDG() {
		$ddg1 = $this->proposta->ddg1;
		$ddg2 = $this->proposta->ddg2;
		$ddg3 = $this->proposta->ddg3;
		$ddg4 = $this->proposta->ddg4;
		$ddg5 = $this->proposta->ddg5;

		$arrayInserted = array ();

		$ddg2Index = null;
		$ddg3Index = null;
		$ddg4Index = null;
		$ddg5Index = null;

		$cnt = - 1;

		$riordina = array ();
		if (isset ( $ddg1 ) ) {
			$cnt ++;
			$arrayInserted [$cnt] = $ddg1;
			$riordina [$cnt] = $ddg1;
		}
		if (isset ( $ddg2 ) ) {
			$cnt ++;
			$arrayInserted [$cnt] = $ddg2;
			$riordina [$cnt] = $ddg2;
		}
		if (isset ( $ddg3 )) {
			$cnt ++;
			$arrayInserted [$cnt] = $ddg3;
			$riordina [$cnt] = $ddg3;
		}
		if (isset ( $ddg4 ) ) {
			$cnt ++;
			$arrayInserted [$cnt] = $ddg4;
			$riordina [$cnt] = $ddg4;
		}
		if (isset ( $ddg5 ) ) {
			$cnt ++;
			$arrayInserted [$cnt] = $ddg5;
			$riordina [$cnt] = $ddg5;
		}

		$this->proposta->ddg2 = $this->proposta->ddg3 = $this->proposta->ddg4 = $this->proposta->ddg5 = null;
		if ($cnt >= 1) {
			$this->proposta->ddg2 = $riordina [1];
		}
		if ($cnt >= 2) {
			$this->proposta->ddg3 = $riordina [2];
		}
		if ($cnt >= 3) {
			$this->proposta->ddg4 = $riordina [3];
		}
		if ($cnt == 4) {
			$this->proposta->ddg5 = $riordina [4];
		}

		if ($cnt >= 0) {
			for($i = 0; $i <= $cnt - 1; $i ++) {
				for($j = $i + 1; $j <= $cnt; $j ++) {
					$temp1 = $arrayInserted [$i];
					$temp2 = $arrayInserted [$j];
					if ($temp1 == $temp2) {
						return false;
					}
				}
			}
		}
		return true;
	}
	public function cancellaImmagine($id, $nome) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$id=intval($id);
		$nome= $db->escape($nome);
		$query->delete ( "#__cai_immagine" )->where ( " id_proposta= '$id' AND nome_immagine = '$nome'" );
		$db->setQuery ( $query );
		if ($db->execute ()) {
			$filename = JPATH_ROOT . "/media/com_cai/immagini_proposte/" . $id . "/" . $nome;
			return unlink ( $filename );
		}
		return false;
	}
	public function cancellaAllegato($id, $nome) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$id=intval($id);
		$nome= $db->escape($nome);
		$query->delete ( "#__cai_allegato" )->where ( " id_proposta= '$id' AND nome_allegato = '$nome'" );
		$db->setQuery ( $query );
		if ($db->execute ()) {
			$filename = JPATH_ROOT . "/media/com_cai/allegati_proposte/" . $id . "/" . $nome;
			return unlink ( $filename );
		}
		return false;
	}
	public function isSvolta($id) {
		$p = $this->getPropostaById ( $id );
		return $p->stato == 2;
	}
	public function updateAttivitaSvolte() {
        $db = JFactory::getDbo ();

        $articleModel = $this->getInstance ( 'articolo', 'caiModel' );
        $query = $db->getQuery ( true );
        $today =$db->escape( date ( 'Y-m-d', time ()-(86400*1)));//today+1 days

        $where = " (stato = 1 OR stato=4) AND data_inizio<= '$today'";
        $query->select("id, id_articolo")
            ->from("#__cai_proposta")
            ->where($where);
        $db->setQuery ( $query );
        $newSvolte=$db->loadObjectList ();
        foreach ($newSvolte as $newSvolta){
            $articleModel->nascondiArticoloDaHompageSeSvolto($newSvolta->id_articolo);

            $query = $db->getQuery ( true );
            $query->update ( "#__cai_proposta" );
            $query->set ( 'stato = 2' );
            $query->where ("id = '$newSvolta->id'" );
            $db->setQuery ( $query );
            $db->execute ();
        }
	}
	public function getLinkArticoloProposta($idProposta) {
		$p = $this->getPropostaById ( $idProposta );
		if ($p != null && isset ( $p->id_articolo ) && trim ( $p->id_articolo ) != '') {
			$id = $p->id_articolo;
			$link=JRoute::_('index.php?option=com_content&view=article&id='.$id);
			return $link;
		}
	}

	public function salvaRelazionePrivata($id, $testo){
		$r=new Relazione();
		$r->id_proposta=$id;
		$r->relazione=$testo;
		$db=JFactory::getDbo();
		$id=intval($id);
		$db->setQuery("SELECT COUNT(*) FROM #__cai_relazione_privata WHERE id_proposta=$id");
		if($db->loadResult()>0){
			return $db->updateObject("#__cai_relazione_privata", $r, "id_proposta", true);
		}
		return $db->insertObject("#__cai_relazione_privata", $r, "id_proposta");
	}

	public function salvaRelazionePubblica($id, $testo){
		$r=new Relazione();
		$r->id_proposta=$id;
		$r->relazione=$testo;
		$db=JFactory::getDbo();
		$id=intval($id);
		$db->setQuery("SELECT COUNT(*) FROM #__cai_relazione_pubblica WHERE id_proposta=$id");
		if($db->loadResult()>0){
			return $db->updateObject("#__cai_relazione_pubblica", $r, "id_proposta", true);
		}
		return $db->insertObject("#__cai_relazione_pubblica", $r, "id_proposta");
	}

	public function getRelazionePubblica($id){
		$db=JFactory::getDbo();
		$id=intval($id);
		$db->setQuery("SELECT relazione FROM #__cai_relazione_pubblica WHERE id_proposta=$id");
		return $db->loadResult();
	}

	public function getRelazionePrivata($id){
		$db=JFactory::getDbo();
		$id=intval($id);
		$db->setQuery("SELECT relazione FROM #__cai_relazione_privata WHERE id_proposta=$id");
		return $db->loadResult();
	}

	public function getTable($type = 'Proposta', $prefix = 'CAITable', $config = array()) {
		return JTable::getInstance ( $type, $prefix, $config );
	}
}
class Proposta {
	public $id;
	public $ddg1;
	public $ddg2;
	public $ddg3;
	public $ddg4;
	public $ddg5;
	public $tipologia;
	public $gruppo;
	public $titolo;
	public $data_inizio;
	public $abilita_data_fine;
	public $data_fine;
	public $appuntamento;
	public $descrizione;
	public $difficolta;
	public $dislivello;
	public $lunghezza;
	public $indicazioni;
	public $durata;
	public $modalita;
	public $costo;
	public $contatti;
	public $approfondimenti;
	public $prenotazione;
	public $id_articolo;
	public $data_proposta;
	public $stato;
	public $pubblicazione_homepage;
	public $descrizione_stripped;
}
class PropostaErrors {
	public $ddg = "";
	public $tipologia = "";
	public $titolo = "";
	public $data = "";
	public $allegati = "";
	public $immagini = "";
	public $erroreGenerico;
}
class Allegato {
	public $id_proposta;
	public $nome_allegato;
}
class Immagine {
	public $id_proposta;
	public $nome_immagine;
}

class Relazione{
	public $id_proposta;
	public $relazione;
}
