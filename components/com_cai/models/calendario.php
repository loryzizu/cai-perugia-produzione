<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelCalendario extends JModelItem {

	public function getEventi(){
        $today = date('Y-m-d', time());
		$utenteModel=$this->getInstance("Utente", "CAIModel");
		$propostaModel=$this->getInstance("Proposta", "CAIModel");
		$db=JFactory::getDbo();

        $query="SELECT * FROM #__cai_proposta WHERE (stato=1 OR stato=2 OR stato=4) ";
		if($utenteModel->isSegretario()){
			$query="SELECT * FROM #__cai_proposta WHERE stato != 6 ";
		}
		$db->setQuery($query);
		$proposte=$db->loadObjectList();
		$result= array();
		$cnt=0;
		foreach($proposte AS $proposta){

			$proposta->titolo=str_replace("'", "&#39;", $proposta->titolo);
			$proposta->titolo=str_replace("\r\n", " ", $proposta->titolo);
			$proposta->titolo=str_replace("\r", " ", $proposta->titolo);
			$proposta->titolo=str_replace("\n", " ", $proposta->titolo);
			$titolo="title: '$proposta->titolo' , ";
			$start="start: '$proposta->data_inizio' ,";
			$finish=trim($proposta->data_fine);
			if($finish==null || $finish=="0000-00-00" || $finish==""){
				$finish="";
			}
			else{
				$finish2= date('Y-m-d',strtotime($finish. ' + 1 day'));
				$finish="end: '$finish2',";
			}
			$id_articolo=$proposta->id_articolo;
			if($id_articolo==null || trim($id_articolo)==""){
				$indirizzo="";
			}
			else{
				$id_articolo=intval($id_articolo);
				$db->setQuery("SELECT alias FROM #__content where id=$id_articolo ");
				$alias=$db->loadObject();
				if($alias !=null
                    && trim($alias->alias)!=''
                    && $this->getInstance ( 'utils', 'caiModel' )->isDatesInOrder($proposta->pubblicazione_homepage, $today)){

				    $modelProposta = $this->getInstance ( 'proposta', 'caiModel' );
					$linkProposta=$modelProposta->getLinkArticoloProposta ( $proposta->id );
					$indirizzo='"url": "'.$linkProposta.'" , ';

				}
				else{
					$indirizzo="";
				}
			}

			$tipologia=$proposta->tipologia;
			switch($tipologia){
				case 0:
					$color="grey";//nulla
					break;
				case 11:
				case 3:
				case 4:
					$color="#3b5998";//Cai Perugia, Cultura, Eventi (blu Cai)
					break;
				case 2:
					$color="#FFD600";//cAlpinismo giovanile(giallo)
					break;
				case 5 :
				case 8:
					$color="#558B2F";//Escursionismo, Cicloescursionismo (verde)
					break;
				case 6 :
				case 12 :
					$color="#F44336";//speleologia, torrentismo ROSSO
					break;
				case 9:
                case 10:
					$color="#42A5F5";//Sci (fondo, sciescursionismo, scialpinismo) (azzurro)
					break;
                case 1:
                case 7:
                    $color="orange";//Alpinismo, Arrampicata (arancio)
                    break;
				default:
					$color="grey";

			};

			$stato=$proposta->stato;
			if($stato==0 || $stato==3 || $stato==5){//se non approvato colore nero
				$color="black";
			}
			$borderColor="borderColor: '$color', ";
			$textColor="textColor: '$color'";
			$result[$cnt]= "{
			$titolo
			$start
			$finish
			$indirizzo
			$borderColor
			$textColor
			}";
			$cnt++;
		}
		return $result;

	}

    public function getUrlArticolo($proposta){
        $id_articolo=$proposta->id_articolo;
        if($id_articolo==null || trim($id_articolo)==""){
            $indirizzo="";
        }
        else{
            $db=JFactory::getDbo();
            $id_articolo=intval($id_articolo);
            $db->setQuery("SELECT alias FROM #__content where id=$id_articolo ");
            $alias=$db->loadObject();
            if($alias !=null && trim($alias->alias)!=''){
                $modelProposta = $this->getInstance ( 'proposta', 'caiModel' );
                $linkProposta=$modelProposta->getLinkArticoloProposta ( $proposta->id );
                $indirizzo='"url": "'.$linkProposta.'" , ';
            }
            else{
                $indirizzo="";
            }
        }
        return $indirizzo;
    }
}