<?php
// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla modelitem library
jimport ( 'joomla.application.component.modelitem' );
class CAIModelEmail extends JModelItem {
	public function invia($destinatari, $oggetto, $messaggio, $mittente=null) {
		if ($destinatari != null && trim ( $destinatari ) != "") {
			$array = explode ( ",", $destinatari );
			$newArray = array ();
			$cnt = 0;
			foreach ( $array as $indirizzo ) {
				$indirizzo = trim ( $indirizzo );
				if (filter_var ( $indirizzo, FILTER_VALIDATE_EMAIL )) {
					$newArray [$cnt] = $indirizzo;
					$cnt ++;
				}
			}
			if (count ( $newArray ) > 0) {
				//$destinatari = implode ( " , ", $newArray );
				$oggetto = trim ( $oggetto );
				if (strlen ( $oggetto ) > 0) {
					$messaggio = trim ( $messaggio );
					if (strlen ( $messaggio ) > 0) {
						if($mittente==null){
							$mittente = $this->getInstance ( "Utente", "CAIModel" )->getCurrentUser ();
							$from = trim ( $mittente->email );
							$fromName = $mittente->nome . " " . $mittente->cognome;
						}
						else{
							$from= $mittente;
							$fromName="CAI Perugia";
						}
						/*require_once JPATH_BASE .'/media/com_cai/lib/swiftmailer/lib/swift_required.php';
						$transport = Swift_SmtpTransport::newInstance ( 'ssl://smtps.aruba.it', 465 )->setUsername ( 'postamaster@caiperugia.it' )-> // Your Gmail Username
						setPassword ( '' ); // Your Gmail Password
						$mailer = Swift_Mailer::newInstance ( $transport );
						$message = Swift_Message::newInstance ( $oggetto )->setFrom ( array (
								$from => $fromName
						) )->setTo ( $newArray )->setBody ( $messaggio, 'text/html' );

						if ($mailer->send ( $message )) {
							return true;
						}*/
						
						
						$to = "";
						foreach ($newArray as $destinatario){
							$to=$to." $destinatario , ";
						}
						$subject = $oggetto;
						$sender = "postmaster@caiperugia.it";
						
						
						$headers   = array();
						$headers[] = "MIME-Version: 1.0";
						$headers[] = "Content-type: text/html; charset=iso-8859-1";
						$headers[] = "From: $fromName <$sender>";
						$headers[] = "Reply-To: Recipient Name <$from>";
						$headers[] = "Subject: {$oggetto}";
						$headers[] = "X-Mailer: PHP/".phpversion();
						
						
						if (mail($to, $oggetto, $messaggio, implode("\r\n", $headers))){
							return true;
						}
						return false;
					}
				}
			}
		}
	}
}