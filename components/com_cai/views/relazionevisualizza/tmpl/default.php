<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>


	<h1>Relazione</h1>
	<h2>Attività: <?php echo $this->titolo; ?></h2>
	
    <br>
    <h3>Relazione: </h3>
    <table class="tabIntro">
    	<tr>
    		<td>
				<?php echo $this->relazionePubblica;?>
    		</td>
    	</tr>
    </table>

    <?php if($this->isAdmin){?>
    <br>
    <h3>Relazione riservata: </h3>
    <table class="tabIntro">
    	<tr>
    		<td>
				<?php echo $this->relazionePrivata;?>
    		</td>
    	</tr>
    </table>
    <?php }?>