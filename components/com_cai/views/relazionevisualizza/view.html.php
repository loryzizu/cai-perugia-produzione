<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewRelazioneVisualizza extends JViewLegacy{

	public $id;
	public $titolo;
	public $relazionePrivata="";
	public $relazionePubblica="";
	public $isAdmin=false;

	function display($tpl = null) {
		$this->id = JRequest::getVar ( "id", null );
		if ($this->id != null) {
			$proposta = $this->getModel ( "Proposta" )->getPropostaById( $this->id );
			$this->titolo=$proposta->titolo;
			$this->relazionePubblica=$this->getModel ( "Proposta" )->getRelazionePubblica($this->id);
			if($this->getModel ( "Proposta" )->isPropostaModificabileByCurrentUser( $this->id )){
				$this->isAdmin=true;
				$this->relazionePrivata=$this->getModel ( "Proposta" )->getRelazionePrivata($this->id);
			}
		}
		parent::display ( $tpl );
	}
}
