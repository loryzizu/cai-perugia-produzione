<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({
	selector: "textarea.editor",
	language_url : "<?php echo JUri::base();?>/media/com_cai/lib/tinyMCE_lang/it.js",
	language : "it",
	plugins: [
	          "advlist autolink link  lists charmap print preview hr anchor  spellchecker",
	          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime  nonbreaking",
	          "save table contextmenu directionality emoticons  paste textcolor"
	    ],
	    toolbar: " insertfile undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect",

	});
</script>
<?php if($this->inviata===true){?>
<span class="iscrizione">Email inviata con successo!</span>
<?php }?>
<br>
<form method="POST">
	<h2>Messaggio:</h2>
	<br> <span class="error"><?php $this->error->destinatari?></span> <label>Destinatari:</label><br>
	<textarea name="destinatari" style="max-width: 95%; width: 95%;"
		 rows="5"><?php
		echo $this->destinatari;

		?></textarea>
	<br> <span class="error"><?php $this->error->oggetto?></span> Oggetto:<br>
	<input type="text" name="oggetto" style="max-width: 95%; width: 95%;"
		placeholder="Oggetto..."
		<?php

if (isset ( $this->oggetto )) {

			echo "value=\"$this->oggetto\"";
		}

		?>><br>
	<br> <span class="error"><?php $this->error->messaggio?></span>
	<textarea class="editor" name="messaggio"
		placeholder="Scrivi email da inviare ai partecipanti..." cols="60"
		rows="50">
	        <?php

if (isset ( $this->messaggio )) {
										echo $this->messaggio;
									}

									?></textarea>
	<br>
	        <?php

if (isset ( $this->idProposta )) {
										echo "<input type=\"hidden\" value=\"$this->idProposta\" name=\"id\">";
									}

									?>
	        <input type="submit" name="submit" value="Invia"> <br> <input
		type="hidden" name="option" value="com_cai" /> <input type="hidden"
		name="task" value="email.invia" />
</form>