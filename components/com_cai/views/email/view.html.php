<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewEmail extends JViewLegacy{
	public $destinatari;
	public $oggetto;
	public $messaggio;
	public $idProposta;
	public $error;
	public $inviata = false;

	function display($tpl = null) {
		$this->idProposta = JRequest::getVar ( "id", null );
		if ($this->error == null) {
			$this->error = new EmailErrors ();
		}
		if ($this->idProposta != null) {
			$iscritti = $this->getModel ( "Iscrizione" )->getSociIscrittiEDDGAttivita ( $this->idProposta );
			if ($iscritti == null || count ( $iscritti ) < 1) {
				$this->destinatari = "Non risultano iscritti a questa attività";
			} else {
				$this->destinatari = "";
				foreach ( $iscritti as $iscritto ) {
					$this->destinatari .= "$iscritto->email ,";
				}
			}
			$this->oggetto = JRequest::getVar ( "oggetto" );
			$jinput = JFactory::getApplication ()->input;
			$messaggioA = $jinput->get ( 'messaggio', '', 'ARRAY' );
			if (isset ( $messaggioA [0] ) && count ( $messaggioA ) > 0) {
				$this->messaggio = $messaggioA [0];
			}
		}
		parent::display ( $tpl );
	}
}
class EmailErrors {
	public $destinatari = "";
	public $oggetto = "";
	public $messaggio = "";
}
