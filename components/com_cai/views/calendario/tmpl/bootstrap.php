


<link rel="stylesheet"
	href="media/com_cai/js/bower_components/bootstrap/docs/assets/css/bootstrap.css">

<link rel="stylesheet"
	href="media/com_cai/js/bower_components/bootstrap-calendar/css/calendar.css">


<div class="page-header">

	<div class="pull-right form-inline">
		<div class="btn-group">
			<button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
			<button class="btn" data-calendar-nav="today">Today</button>
			<button class="btn btn-primary" data-calendar-nav="next">Next >></button>
		</div>
		<div class="btn-group">
			<button class="btn btn-warning" data-calendar-view="year">Year</button>
			<button class="btn btn-warning active" data-calendar-view="month">Month</button>
			<button class="btn btn-warning" data-calendar-view="week">Week</button>
			<!--  <button class="btn btn-warning" data-calendar-view="day">Day</button>-->
		</div>
	</div>
	<h3></h3>
</div>

<div id="calendar"></div>




<script type="text/javascript"
	src="media/com_cai/js/bower_components/jquery/jquery.js"></script>

<script type="text/javascript"
	src="media/com_cai/js/bower_components/underscore/underscore-min.js"></script>

<script type="text/javascript"
	src="media/com_cai/js/bower_components/bootstrap/docs/assets/js/bootstrap.js"></script>

	<script type="text/javascript"
	src="media/com_cai/js/bower_components/moment/moment.js"></script>
	<script type="text/javascript"
	src="media/com_cai/js/bower_components/bootstrap-calendar/js/calendar.js"></script>
	<script type="text/javascript">
	(function($) {

		"use strict";

		var options = {
				events_source: function(){
            	    return  [<?php
        			foreach($this->proposte as $proposta) {
        				echo $proposta.", ";
        			}
  ?>
            	   ];
            	},
			view: 'month',
			tmpl_path: 'media/com_cai/js/bower_components/bootstrap-calendar/tmpls/',
			tmpl_cache: false,
			day: '2013-03-12',
			onAfterEventsLoad: function(events) {
				if(!events) {
					return;
				}
				var list = $('#eventlist');
				list.html('');

				$.each(events, function(key, val) {
					$(document.createElement('li'))
						.html('<a href="' + val.url + '">' + val.title + '</a>')
						.appendTo(list);
				});
			},
			onAfterViewLoad: function(view) {
				$('.page-header h3').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		};

		var calendar = $('#calendar').calendar(options);

		$('.btn-group button[data-calendar-nav]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.navigate($this.data('calendar-nav'));
			});
		});

		$('.btn-group button[data-calendar-view]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.view($this.data('calendar-view'));
			});
		});

		$('#first_day').change(function(){
			var value = $(this).val();
			value = value.length ? parseInt(value) : null;
			calendar.setOptions({first_day: value});
			calendar.view();
		});

		$('#language').change(function(){
			calendar.setLanguage($(this).val());
			calendar.view();
		});

		$('#events-in-modal').change(function(){
			var val = $(this).is(':checked') ? $(this).val() : null;
			calendar.setOptions({modal: val});
		});
		$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
			//e.preventDefault();
			//e.stopPropagation();
		});
	}(jQuery));
    </script>
    <!-- <script type="text/javascript"
	src="media/com_cai/js/bower_components/bootstrap-calendar/js/app.js"></script>
 -->
