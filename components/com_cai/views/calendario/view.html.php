<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewCalendario extends JViewLegacy{
	public $eventi;
	public $proposte;

	function display($tpl = null) {
		$this->eventi=$this->getModel("Calendario")->getEventi();
		parent::display ( $tpl );
	}

	public function getUrlArticolo($proposta){
		return $this->getModel("Calendario")->getUrlArticolo($proposta);
	}
}
