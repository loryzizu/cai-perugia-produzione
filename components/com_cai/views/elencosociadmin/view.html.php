<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewElencoSociAdmin extends JViewLegacy{

	public $nomeUtente="Lorenzo";
	public $fotoPath="";
	public $stato="0";

	function display($tpl = null) {
		$this->nomeUtente="Lorenzo";
		$this->fotoPath="";
		$this->stato=JRequest::getVar("stato", "0");
		parent::display ( $tpl );
	}
}
