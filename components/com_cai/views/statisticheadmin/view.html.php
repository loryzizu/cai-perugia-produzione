<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewStatisticheAdmin extends JViewLegacy{

	public $stato;

	public $listaUtenti;
	public $mostraStatistiche=false;
	public $mostraStatisticheUniche=false;
	public $mostraStatisticheTotali=false;
	public $utente;
	public $idUtente;


	public $name;
	public $data_inizio;
	public $data_fine;
	public $attivitaSvolte;
	public $attivitaApprovate;
	public $proposta;

	public $tipologia;
	public $uniciTotali;
	

	function display($tpl = null) {
		$this->stato=JRequest::getVar("stato", 0);
		if($this->stato==1){
			$this->listaUtenti=$this->getModel("Utente")->getUtenteListAll();
			$id=JRequest::getVar("utente" , null);
			$this->idUtente=$id;
			if($id != null){
				$propostaModel = $this->getModel ( "Proposta" );
				$this->mostraStatistiche=true;
				$this->utente=$this->getModel("utente")->getUserById($id);
				$this->utente->proposte=$propostaModel->getProposteOrganizzateListAllByUserId (  null, null, null, $id );
				$this->utente->partecipazioni=$propostaModel->getProposteListByUserId ( "Svolta", null, null, null, $id );
				$this->utente->iscrizioni=$propostaModel->getProposteListByUserId ( "Approvata", null, null, null, $id );
			}
		}
		elseif($this->stato==2){
			$propostaModel = $this->getModel ( "Proposta" );
			$reset=JRequest::getVar("reset", null);
			if($reset==null){
				$this->name = JRequest::getVar ( "name", null );
				$this->data_inizio = JRequest::getVar ( "data_inizio", null );
				$this->data_fine = JRequest::getVar ( "data_fine", null );
			}
			$this->attivitaSvolte=$propostaModel->getSvolte($this->name, $this->data_inizio, $this->data_fine );
			$this->attivitaApprovate=$propostaModel->getApprovate($this->name, $this->data_inizio, $this->data_fine );
			$id=JRequest::getVar("idProposta" , null);
			if($id != null){
				$modelIscrizione=$this->getModel("Iscrizione");
				$this->proposta=$propostaModel->getPropostaById($id);
				$this->mostraStatistiche=true;
				$this->proposta->soci=$modelIscrizione->getSociIscrittiEDDGAttivita($id);
				$this->proposta->sociEsterni=$modelIscrizione->getSociDiAltreSezioniIscrittiAttivita($id);
				$this->proposta->nonSoci=$modelIscrizione->getNonSociIscrittiAttivita($id);
			}
		}
		elseif($this->stato==3){
			$reset=JRequest::getVar("reset", null);
			if($reset==null){
				$this->tipologia = JRequest::getVar ( "tipologia", null );
				$this->data_inizio = JRequest::getVar ( "data_inizio", null );
				$this->data_fine = JRequest::getVar ( "data_fine", null );
			}
			$filtra=JRequest::getVar("filtraCategoriaData", null);
			if($filtra!=null){
				$modelIscrizione=$this->getModel("Iscrizione");

				$this->proposta= new stdClass();
				$this->uniciTotali=JRequest::getVar("uniciTotali", "unici");
				if($this->uniciTotali=="unici"){
					$this->proposta->soci=$modelIscrizione->getSociIscrittiEDDGByTipologiaEDataUnici($this->tipologia, $this->data_inizio, $this->data_fine);
					$this->mostraStatisticheUniche=true;
				}
				else{
					$this->proposta->soci=$modelIscrizione->getSociIscrittiEDDGByTipologiaEDataAll($this->tipologia, $this->data_inizio, $this->data_fine);
					$this->mostraStatisticheTotali=true;
				}
				$this->proposta->sociEsterni=$modelIscrizione->getSociDiAltreSezioniIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
				$this->proposta->nonSoci=$modelIscrizione->getNonSociIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
			}
		}
		parent::display ( $tpl );
	}

	public function getDateEuropeanFormat($date) {
		$propostaModel = $this->getModel ( "Proposta" );
		$newDate = $propostaModel->getDateEuropeanFormat ( $date );
		return $newDate;
	}
	public function getLinkArticoloProposta($idProposta) {
		return $this->getModel ( "Proposta" )->getLinkArticoloProposta ( $idProposta );
	}

	public function getStatoLabel($stato) {
		$propostaModel = $this->getModel ( "Proposta" );
		$label = $propostaModel->getStatoLabel ( $stato );
		if ($label == null) {
			$label = "Stato non trovato";
		}
		return $label;
	}


}


