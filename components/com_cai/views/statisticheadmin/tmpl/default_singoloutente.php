<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
JHtml::_('script', JUri::base() .'media/com_cai/js/cai.js', false, true);
?>
<table class="tabIntro">
	<tr>
		<td>
<center>
	<h2 style="margin:10px;">Partecipazioni Socio</h2>
</center>

<form method="post">
	<span class="iscrizione" style="margin:10px;">Seleziona un socio per visualizzare tutte le sue partecipazioni ed
	iscrizioni:</span>

				<div style="width: 100%" id="ddg2">
					<input type="text" id="ddg2search" name="utenteSearch" onkeyup="setOptions(this.value, 'utente')">
					<select name="utente"  id="utente" >
					</select>
				</div>


	<br/><br/><input type="hidden" name="option" value="com_cai"> <input
		type="hidden" name="task" value="statistiche.singoloUtente"> <input
		type="submit" value="Mostra attività utente">
</form>

</td>
</tr>
</table>
<br/>
<br/>
<?php if($this->mostraStatistiche){?>
<br/><br/>
<a href="index.php?option=com_cai&format=raw&task=exportStatistiche.exportSingoloUtente&utente=<?php echo $this->idUtente;?>" target="blank">Esporta in formato csv</a>
<br/><br/>

<table class="tabIntro">
	<tr>
		<td>


<center>		<form method="post">
				<input type="submit" value="<?php echo $this->utente->nome." ".$this->utente->cognome; ?>" name="profilo"> <input
					type="hidden" name="id" value="<?php echo  $this->utente->id;?>" />
				<input name="task" type="hidden" value="profilo.profilo" /> <input
					name="option" type="hidden" value="com_cai" />
			</form>
</center>

			<h2>Proposte</h2>

			<table class="listaProposte">
				<tr style="width: 100%;">
					<th>Titolo</th>
					<th>Data</th>
					<th>Stato</th>
				</tr>
	    <?php
	foreach ( $this->utente->proposte as $proposta ) {
		$linkArticolo = $this->getLinkArticoloProposta ( $proposta->id );
		$statoLabel = $this->getStatoLabel ( $proposta->stato );
		?>
		<tr>
					<td>
				<?php if($linkArticolo!=false){?><a
						href="<?php echo $linkArticolo ;?>"><?php }echo $proposta->titolo ;?></a>
					</td>
					<td>
				<?php echo $this->getDateEuropeanFormat($proposta->data_inizio); ?>
			</td>
					<td>
				<?php echo $statoLabel; ?>
			</td>

				</tr>
		<?php
	}
	?>


    </table>

			<h2>Partecipazioni</h2>
			<table class="listaProposte">
				<tr style="width: 100%;">
					<th>Titolo</th>
					<th>Data</th>
				</tr>
	    <?php
	foreach ( $this->utente->partecipazioni as $proposta ) {
		$linkArticolo = $this->getLinkArticoloProposta ( $proposta->id );
		?>
    			<tr>
					<td>
    					<?php if($linkArticolo){?><a
						href="<?php echo $linkArticolo ;?>"><?php }echo $proposta->titolo ;?></a>
					</td>
					<td>
    					<?php echo $this->getDateEuropeanFormat($proposta->data_inizio); ?>
    				</td>
				</tr>
		<?php
	}
	?>
    </table>

			<h2>Iscrizioni</h2>
			<table class="listaProposte">
				<tr style="width: 100%;">
					<th>Titolo</th>
					<th>Data</th>
				</tr>
	    <?php
	foreach ( $this->utente->iscrizioni as $proposta ) {
		$linkArticolo = $this->getLinkArticoloProposta ( $proposta->id );
		?>
    			<tr>
					<td>
    					<?php if($linkArticolo){?><a
						href="<?php echo $linkArticolo ;?>"><?php }echo $proposta->titolo ;?></a>
					</td>
					<td>
    					<?php echo $this->getDateEuropeanFormat($proposta->data_inizio); ?>
    				</td>
				</tr>
		<?php
	}
	?>
    </table>

		</td>
	</tr>
</table>
<?php }?>