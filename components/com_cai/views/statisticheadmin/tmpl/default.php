<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<center>
	<h2>Statistiche</h2>
</center>
<form method="post">
	<input type="radio" name="stato" value="1" <?php if($this->stato ==1) echo "checked";?>><span style="font-weight: bold; margin-left:10px; margin-right:15px ">Partecipazioni singolo utente</span>
	<input type="radio" name="stato" value="2" <?php if($this->stato ==2) echo "checked";?>> <span style="font-weight: bold; margin-left:10px; margin-right:15px ">Partecipazioni singola attività</span>
	<input type="radio" name="stato" value="3" <?php if($this->stato ==3) echo "checked";?>><span style="font-weight: bold; margin-left:10px; margin-right:15px ">Partecipazioni filtrate per categoria e data</span>
	<br/><br/>
	<input type="submit" name="filtra" value="Scegli Filtro">
</form>
<br/>
<br/>

<?php

	if($this->stato==1){
		echo $this->loadTemplate('singoloutente');
	}
	elseif($this->stato==2){
		echo $this->loadTemplate('singolaattivita');
	}
	elseif($this->stato==3){
		echo $this->loadTemplate('categoriaedata');
	}
?>