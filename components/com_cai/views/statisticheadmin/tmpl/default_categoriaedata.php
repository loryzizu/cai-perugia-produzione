<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>

<table class="tabIntro">
	<tr>
		<td>
			<center>
				<h2 style="margin:10px;">Partecipazioni Attività</h2>
			</center>

			<script type="text/javascript" src="media/com_cai/js/calendar/calendar.js"></script>
			<form method="post">
			        <span>Filtra:</span>
			        <br/>
		        	<select name="tipologia" >
						<option value=""
							<?php if ($this->tipologia=="NULL" ) echo"selected"; ?>>Scegli
							Tipologia</option>
						<option value="0"
							<?php if ($this->tipologia=="0" ) echo"selected"; ?>>Nulla</option>
						<option value="1"
							<?php if ($this->tipologia=="1" ) echo"selected"; ?>>Alpinismo</option>
						<option value="2"
							<?php if ($this->tipologia=="2" ) echo"selected"; ?>>Alpinismo
							Giovanile</option>
						<option value="3"
							<?php if ($this->tipologia=="3" ) echo"selected"; ?>>Cultura</option>
						<option value="4"
							<?php if ($this->tipologia=="4" ) echo"selected"; ?>>Eventi</option>
						<option value="5"
							<?php if ($this->tipologia=="5" ) echo"selected"; ?>>Escursionismo</option>
						<option value="6"
							<?php if ($this->tipologia=="6" ) echo"selected"; ?>>Speleologia</option>
						<option value="7"
							<?php if ($this->tipologia=="7" ) echo"selected"; ?>>Arrampicata</option>
						<option value="8"
							<?php if($this->tipologia=="8") echo"selected"; ?>>Cicloescursionismo</option>
						<option value="9"
							<?php if($this->tipologia=="9") echo"selected"; ?>>Sciescursionismo</option>
						<option value="10"
							<?php if($this->tipologia=="10") echo"selected"; ?>>Scialpinismo</option>
						<option value="11"
							<?php if($this->tipologia=="11") echo"selected"; ?>>CAI
							Perugia</option>
						<option value="12"
							<?php if($this->tipologia=="12") echo"selected"; ?>>Torrentismo</option>
					</select>
					<br/><br/>

			        <input type="text" placeholder="Da..."  name="data_inizio" value="<?php echo $this->data_inizio;?>" onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
			        <input type="text" placeholder="A..."  name="data_fine"  value="<?php echo $this->data_fine;?>"onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
			        <br/><br/>

			        <span>Unici: </span> <input type="radio" name="uniciTotali" value="unici"  <?php if(!isset($this->uniciTotali) || $this->uniciTotali=="unici") echo "checked";?>/>
			        <span>Totali: </span> <input type="radio" name="uniciTotali" value="totali" <?php if($this->uniciTotali=="totali") echo "checked";?>/>
			        <br/>
					<input type="submit" name="filtraCategoriaData" value="Filtra">
			        <input type="submit" name="reset" value="Annulla Filtro" >
			        <input type="hidden" name="option" value="com_cai">
			        <input type="hidden" name="task" value="statistiche.categoriaEData">
			        <input type="hidden" name="stato" value="3">
			</form>
			<br/><br/>
		</td>
	</tr>
</table>


<?php if($this->mostraStatisticheUniche){?>
<a href="index.php?option=com_cai&format=raw&task=exportStatistiche.exportCategoriaEDataUnici&tipologia=<?php echo $this->tipologia;?>&data_inizio=<?php echo $this->data_inizio;?>&data_fine=<?php echo $this->data_fine;?>" target="blank">Esporta in formato csv</a>

<table class="tabIntro">
	<tr>
		<td>

			<?php
			$sociIscritti = $this->proposta->soci;
			$sociEsterni = $this->proposta->sociEsterni;
			$nonSoci = $this->proposta->nonSoci;
			?>

			<span class="iscrizione">Numero di <b>soci</b> partecipanti:
			</span>
			<span><?php echo count($sociIscritti);?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di <b>soci di altre sezioni</b>
				partecipanti:
			</span>
			<span><?php echo count($sociEsterni);?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di <b>non soci</b> partecipanti:
			</span>
			<span><?php echo count($nonSoci)?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di partecipanti <b>totali</b>:
			</span>
			<span><?php echo count($sociIscritti)+ count($sociEsterni) +count($nonSoci);?></span>
			<br />
			<br />


			<span class="iscrizione">Soci partecipanti:</span>
			<table class=tabSoci>
				<tr class=tabSociTitle>
					<td class=tabSociTitle>Utente</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Telefono</td>
					<td class=tabSociTitle>Cellulare</td>
				</tr>
				<?php

				foreach ( $sociIscritti as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci>
						<form method="post">
							<input type="submit" value="<?php echo $socio->cognome. " ".$socio->nome; ?>" name="profilo"> <input
								type="hidden" name="id" value="<?php echo  $socio->id;?>" /> <input
								name="task" type="hidden" value="profilo.profilo" /> <input
								name="option" type="hidden" value="com_cai" />
						</form>
					</td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->telefono ;?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
				</tr>
				<?php }?>
			</table>
			<br />
			<br />
			<span class="iscrizione">Soci di altre sezioni partecipanti:</span>
			<table class=tabSoci>
				<tr class=tabSociTitle>

					<td class=tabSociTitle>Nome</td>
					<td class=tabSociTitle>Data di Nascita</td>
					<td class=tabSociTitle>Cellulare</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Codice Fiscale</td>
					<td class=tabSociTitle>Sezione</td>
					<td class=tabSociTitle>Note</td>
					<td class=tabSociTitle>Cancella</td>
				</tr>
				<?php
				foreach ( $sociEsterni as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
					<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->cf ;?></td>
					<td class=tabSoci><?php echo $socio->sezione ;?></td>
					<td class=tabSoci><?php echo $socio->note ;?></td>
					<td class=tabSoci>
						<form method="post">
						<input type="submit" value="Cancella"> <input type="hidden"
					name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
					type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
					<input
					type="hidden" name="id" value="<?php echo  $socio->id?>" />
				<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneSocioEsterno" /> <input
					name="option" type="hidden" value="com_cai" />

						</form>
					</td>

				</tr>
				<?php }?>
			</table>
			<br />
			<br />

			<br />
			<br />
			<span class="iscrizione">Non soci partecipanti:</span>

			<table class=tabSoci>
				<tr class=tabSociTitle>

					<td class=tabSociTitle>Nome</td>
					<td class=tabSociTitle>Data di Nascita</td>
					<td class=tabSociTitle>Cellulare</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Codice Fiscale</td>
					<td class=tabSociTitle>Note</td>
					<td class=tabSociTitle>Cancella</td>
				</tr>
				<?php
				foreach ( $nonSoci as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
					<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->cf ;?></td>
					<td class=tabSoci><?php echo $socio->note ;?></td>
					<td class=tabSoci>
						<form method="post">
						<input type="submit" value="Cancella"> <input type="hidden"
					name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
					type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
					<input
					type="hidden" name="id" value="<?php echo  $socio->id?>" />
				<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneNonSocio" /> <input
					name="option" type="hidden" value="com_cai" />

						</form>
					</td>

				</tr>
				<?php }?>
			</table>
			<br />
			<br />
		</td>
	</tr>
</table>

<br />
<br />

























<?php }elseif($this->mostraStatisticheTotali){?>
<a href="index.php?option=com_cai&format=raw&task=exportStatistiche.exportCategoriaEDataTotali&tipologia=<?php echo $this->tipologia;?>&data_inizio=<?php echo $this->data_inizio;?>&data_fine=<?php echo $this->data_fine;?>" target="blank">Esporta in formato csv</a>

<table class="tabIntro">
	<tr>
		<td>

			<?php
			$sociIscritti = $this->proposta->soci;
			$sociEsterni = $this->proposta->sociEsterni;
			$nonSoci = $this->proposta->nonSoci;
			?>

			<span class="iscrizione">Numero di <b>soci</b> partecipanti:
			</span>
			<span><?php echo count($sociIscritti);?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di <b>soci di altre sezioni</b>
				partecipanti:
			</span>
			<span><?php echo count($sociEsterni);?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di <b>non soci</b> partecipanti:
			</span>
			<span><?php echo count($nonSoci)?></span>
			<br />
			<br />
			<span class="iscrizione">Numero di partecipanti <b>totali</b>:
			</span>
			<span><?php echo count($sociIscritti)+ count($sociEsterni) +count($nonSoci);?></span>
			<br />
			<br />


			<span class="iscrizione">Soci partecipanti:</span>
			<table class=tabSoci>
				<tr class=tabSociTitle>
					<td class=tabSociTitle>Utente</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Telefono</td>
					<td class=tabSociTitle>Cellulare</td>
					<td class=tabSociTitle>Titolo Attività</td>
					<td class=tabSociTitle>Data</td>
				</tr>
				<?php

				foreach ( $sociIscritti as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci>
						<form method="post">
							<input type="submit" value="<?php echo $socio->cognome. " ".$socio->nome; ?>" name="profilo"> <input
								type="hidden" name="id" value="<?php echo  $socio->id;?>" /> <input
								name="task" type="hidden" value="profilo.profilo" /> <input
								name="option" type="hidden" value="com_cai" />
						</form>
					</td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->telefono ;?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
					<td class=tabSoci><?php echo $socio->titolo;?></td>
					<td class=tabSoci><?php echo $this->getDateEuropeanFormat($socio->data_inizio); ?></td>
				</tr>
				<?php }?>
			</table>
			<br />
			<br />
			<span class="iscrizione">Soci di altre sezioni partecipanti:</span>
			<table class=tabSoci>
				<tr class=tabSociTitle>

					<td class=tabSociTitle>Nome</td>
					<td class=tabSociTitle>Data di Nascita</td>
					<td class=tabSociTitle>Cellulare</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Codice Fiscale</td>
					<td class=tabSociTitle>Sezione</td>
					<td class=tabSociTitle>Note</td>
					<td class=tabSociTitle>Cancella</td>
				</tr>
				<?php
				foreach ( $sociEsterni as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
					<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->cf ;?></td>
					<td class=tabSoci><?php echo $socio->sezione ;?></td>
					<td class=tabSoci><?php echo $socio->note ;?></td>
					<td class=tabSoci>
						<form method="post">
						<input type="submit" value="Cancella"> <input type="hidden"
					name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
					type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
					<input
					type="hidden" name="id" value="<?php echo  $socio->id?>" />
				<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneSocioEsterno" /> <input
					name="option" type="hidden" value="com_cai" />

						</form>
					</td>

				</tr>
				<?php }?>
			</table>
			<br />
			<br />

			<br />
			<br />
			<span class="iscrizione">Non soci partecipanti:</span>

			<table class=tabSoci>
				<tr class=tabSociTitle>

					<td class=tabSociTitle>Nome</td>
					<td class=tabSociTitle>Data di Nascita</td>
					<td class=tabSociTitle>Cellulare</td>
					<td class=tabSociTitle>Email</td>
					<td class=tabSociTitle>Codice Fiscale</td>
					<td class=tabSociTitle>Note</td>
					<td class=tabSociTitle>Cancella</td>
				</tr>
				<?php
				foreach ( $nonSoci as $socio ) {
					?>
				<tr class=tabSoci>

					<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
					<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
					<td class=tabSoci><?php echo $socio->cellulare ;?></td>
					<td class=tabSoci><?php echo $socio->email ;?></td>
					<td class=tabSoci><?php echo $socio->cf ;?></td>
					<td class=tabSoci><?php echo $socio->note ;?></td>
					<td class=tabSoci>
						<form method="post">
						<input type="submit" value="Cancella"> <input type="hidden"
					name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
					type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
					<input
					type="hidden" name="id" value="<?php echo  $socio->id?>" />
				<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneNonSocio" /> <input
					name="option" type="hidden" value="com_cai" />

						</form>
					</td>

				</tr>
				<?php }?>
			</table>
			<br />
			<br />
		</td>
	</tr>
</table>

<br />
<br />
<?php }?>