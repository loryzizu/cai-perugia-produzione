<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>

<?php if($this->mostraStatistiche){?>
	<a href="index.php?option=com_cai&format=raw&task=exportstatistiche.exportSingolaAttivita&idProposta=<?php echo $this->proposta->id;?>" target="blank">Esporta in formato csv</a>
	<table class="tabIntro">
		<tr>
			<td>
				<h2>Attività: <?php echo $this->proposta->titolo;?></h2>

				<?php
				$sociIscritti = $this->proposta->soci;
				$sociEsterni = $this->proposta->sociEsterni;
				$nonSoci = $this->proposta->nonSoci;
				?>

				<span class="iscrizione">Numero di <b>soci</b> partecipanti:
			</span>
				<span><?php echo count($sociIscritti);?></span>
				<br />
				<br />
			<span class="iscrizione">Numero di <b>soci di altre sezioni</b>
				partecipanti:
			</span>
				<span><?php echo count($sociEsterni);?></span>
				<br />
				<br />
			<span class="iscrizione">Numero di <b>non soci</b> partecipanti:
			</span>
				<span><?php echo count($nonSoci)?></span>
				<br />
				<br />
			<span class="iscrizione">Numero di partecipanti <b>totali</b>:
			</span>
				<span><?php echo count($sociIscritti)+ count($sociEsterni) +count($nonSoci);?></span>
				<br />
				<br />


				<span class="iscrizione">Elenco soci iscritti a questa attività:</span>
				<table class=tabSoci>
					<tr class=tabSociTitle>
						<td class=tabSociTitle>Nome</td>
						<td class=tabSociTitle>Profilo</td>
						<td class=tabSociTitle>Email</td>
						<td class=tabSociTitle>Telefono</td>
						<td class=tabSociTitle>Cellulare</td>
					</tr>
					<?php

					foreach ( $sociIscritti as $socio ) {
						?>
						<tr class=tabSoci>

							<td class=tabSoci><?php echo $socio->cognome. " ".$socio->nome; ?></td>
							<td class=tabSoci>
								<form method="post">
									<input type="submit" value="Profilo" name="profilo"> <input
										type="hidden" name="id" value="<?php echo  $socio->id;?>" /> <input
										name="task" type="hidden" value="profilo.profilo" /> <input
										name="option" type="hidden" value="com_cai" />
								</form>
							</td>
							<td class=tabSoci><?php echo $socio->email ;?></td>
							<td class=tabSoci><?php echo $socio->telefono ;?></td>
							<td class=tabSoci><?php echo $socio->cellulare ;?></td>
						</tr>
					<?php }?>
				</table>
				<br />
				<br />
			<span class="iscrizione">Elenco soci di altre sezioni iscritti a questa
				attività:</span>
				<table class=tabSoci>
					<tr class=tabSociTitle>

						<td class=tabSociTitle>Nome</td>
						<td class=tabSociTitle>Data di Nascita</td>
						<td class=tabSociTitle>Cellulare</td>
						<td class=tabSociTitle>Email</td>
						<td class=tabSociTitle>Codice Fiscale</td>
						<td class=tabSociTitle>Sezione</td>
						<td class=tabSociTitle>Note</td>
						<td class=tabSociTitle>Cancella</td>
					</tr>
					<?php
					foreach ( $sociEsterni as $socio ) {
						?>
						<tr class=tabSoci>

							<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
							<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
							<td class=tabSoci><?php echo $socio->cellulare ;?></td>
							<td class=tabSoci><?php echo $socio->email ;?></td>
							<td class=tabSoci><?php echo $socio->cf ;?></td>
							<td class=tabSoci><?php echo $socio->sezione ;?></td>
							<td class=tabSoci><?php echo $socio->note ;?></td>
							<td class=tabSoci>
								<form method="post">
									<input type="submit" value="Cancella"> <input type="hidden"
																				  name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
										type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
									<input
										type="hidden" name="id" value="<?php echo  $socio->id?>" />
									<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneSocioEsterno" /> <input
										name="option" type="hidden" value="com_cai" />

								</form>
							</td>

						</tr>
					<?php }?>
				</table>
				<br />
				<br />

				<br />
				<br />
				<span class="iscrizione">Elenco non soci iscritti a questa attività:</span>

				<table class=tabSoci>
					<tr class=tabSociTitle>

						<td class=tabSociTitle>Nome</td>
						<td class=tabSociTitle>Data di Nascita</td>
						<td class=tabSociTitle>Cellulare</td>
						<td class=tabSociTitle>Email</td>
						<td class=tabSociTitle>Codice Fiscale</td>
						<td class=tabSociTitle>Note</td>
						<td class=tabSociTitle>Cancella</td>
					</tr>
					<?php
					foreach ( $nonSoci as $socio ) {
						?>
						<tr class=tabSoci>

							<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
							<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
							<td class=tabSoci><?php echo $socio->cellulare ;?></td>
							<td class=tabSoci><?php echo $socio->email ;?></td>
							<td class=tabSoci><?php echo $socio->cf ;?></td>
							<td class=tabSoci><?php echo $socio->note ;?></td>
							<td class=tabSoci>
								<form method="post">
									<input type="submit" value="Cancella"> <input type="hidden"
																				  name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
										type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
									<input
										type="hidden" name="id" value="<?php echo  $socio->id?>" />
									<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneNonSocio" /> <input
										name="option" type="hidden" value="com_cai" />

								</form>
							</td>

						</tr>
					<?php }?>
				</table>
				<br />
				<br />
			</td>
		</tr>
	</table>

	<br />
	<br />
<?php }?>




















<table class="tabIntro">
	<tr>
		<td>
			<center>
				<h2 style="margin:10px;">Partecipazioni Attività</h2>
			</center>

			<script type="text/javascript" src="media/com_cai/js/calendar/calendar.js"></script>
			<form method="post">
				<span>Filtra:</span>
				<br/>
				<input type="text" placeholder="Nome" name="name" value="<?php echo $this->name;?>"/><br/><br/>

				<input type="text" placeholder="Da..."  name="data_inizio" value="<?php echo $this->data_inizio;?>" onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
				<input type="text" placeholder="A..."  name="data_fine"  value="<?php echo $this->data_fine;?>"onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
				<br/><br/>
				<input type="submit" name="filtra" value="Filtra">
				<input type="submit" name="reset" value="Annulla Filtro" >
				<input type="hidden" name="option" value="com_cai">
				<input type="hidden" name="task" value="statistiche.visualizza">
				<input type="hidden" name="stato" value="2">
			</form>
			<br/><br/>
			<h2>Attività Svolte</h2>
			<table class="listaProposte">
				<tr style="width:100%;">
					<th>Titolo</th>
					<th>Data</th>
					<th>Partecipanti</th>
				</tr>
				<?php
				$list=$this->attivitaSvolte;
				if(isset($list)){
					for ($i=0; $i<count($list); $i++){
						$id=$list[$i]->id;
						$titolo=$list[$i]->titolo;
						$linkArticolo=$this->getLinkArticoloProposta($id);
						$titolo=$list[$i]->titolo;
						$data_inizio=$this->getDateEuropeanFormat($list[$i]->data_inizio);
						$stato=$this->getStatoLabel($list[$i]->stato);
						?>
						<tr>
							<td><a href="<?php echo $linkArticolo ;?>"><?php echo $titolo ;?></a></td>
							<td><?php echo $data_inizio ;?></td>
							<td>
								<form  method="post" >
									<input type="submit" value="Partecipanti" name="partecipanti"/>
									<input type="hidden" name="option" value="com_cai"> <input
										type="hidden" name="task" value="statistiche.singolaAttivita">
									<input type="hidden" value="<?php echo $id;?>" name="idProposta"/>
								</form>
							</td>
						</tr>
						<?php
					}
				}
				?>
			</table>

			<br/><br/>
			<h2>Attività in programma</h2>
			<table class="listaProposte">
				<tr style="width:100%;">
					<th>Titolo</th>
					<th>Data</th>
					<th>Iscritti</th>
				</tr>
				<?php
				$list=$this->attivitaApprovate;
				if(isset($list)){
					for ($i=0; $i<count($list); $i++){
						$id=$list[$i]->id;
						$titolo=$list[$i]->titolo;
						$linkArticolo=$this->getLinkArticoloProposta($id);
						$titolo=$list[$i]->titolo;
						$data_inizio=$this->getDateEuropeanFormat($list[$i]->data_inizio);
						$stato=$this->getStatoLabel($list[$i]->stato);
						?>
						<tr>
							<td><a href="<?php echo $linkArticolo ;?>"><?php echo $titolo ;?></a></td>
							<td><?php echo $data_inizio ;?></td>
							<td>
								<form  method="post" >
									<input type="submit" value="Iscritti" name="partecipanti"/>
									<input type="hidden" name="option" value="com_cai"> <input
										type="hidden" name="task" value="statistiche.singolaAttivita">
									<input type="hidden" value="<?php echo $id;?>" name="idProposta"/>
								</form>
							</td>
						</tr>
						<?php
					}
				}
				?>
			</table>
		</td>
	</tr>
</table>

