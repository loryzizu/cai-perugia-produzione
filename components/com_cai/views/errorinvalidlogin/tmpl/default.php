<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>

<div class="bigMessageContainer">
	<i class="material-icons big grey">lock</i>
	<div class="greyMessage">I dati che hai immesso non sono validi, ritenta o prova con il recupero password.</div>
</div>
