<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<script type="text/javascript" src="media/com_cai/js/calendar/calendar.js"></script>


<script >

	function popupConferma(){
		var answer = confirm ("Sicuro di voler cancellare definitivamente la proposta?")
		if (answer){
			return true;
		}
		else{
			return false;
		}

	}
</script>
<h1>Attività non approvate</h1>
<?php
$this->stampaMsg();
?>
<form method="GET">
	<input type="hidden" name="view" value="attivitaDaApprovareAdmin">
	<div>Filtra:</div>
	<br/>
	<input type="text" placeholder="Nome" name="name" value="<?php echo $this->name;?>"/><br/><br/>

	<input type="text" placeholder="Da..."  name="data_inizio" value="<?php echo $this->data_inizio;?>" onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<input type="text" placeholder="A..."  name="data_fine"  value="<?php echo $this->data_fine;?>"onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<br/><br/>
	<div>Elementi per pagina:</div>
	<select name="size" >
		<option value="10" <?php if($this->size==10) echo "selected"; ?>>10</option>
		<option value="25" <?php if($this->size==25) echo "selected"; ?>>25</option>
		<option value="50" <?php if($this->size==50) echo "selected"; ?>>50</option>
		<option value="0"  <?php if($this->size==0) echo "selected"; ?>>Tutti</option>
	</select>
	<br>
	<br>
	<input type="submit" name="filtra" value="Filtra">
	<input type="submit" name="reset" value="Annulla Filtro" ">
</form>
<br/><br/>
<table class="listaProposte">
	<tr style="width:100%;">
		<th>Titolo</th>
		<th>Data</th>
		<th>Stato</th>
		<th>Modifica</th>
		<th>Cancella</th>
	</tr>
	<?php
	$list=$this->list;
	if(isset($list)){
		for ($i=0; $i<count($list); $i++){
			$titolo=$list[$i]->titolo;
			$data_inizio=$this->getDateEuropeanFormat($list[$i]->data_inizio);
			$stato=$this->getStatoLabel($list[$i]->stato);
			$id=$list[$i]->id;
			?>
			<tr>
				<td><?php echo $titolo ;?></td>
				<td><?php echo $data_inizio ;?></td>
				<td><?php echo $stato ;?></td>
				<td>
					<form  method="post" >
						<input type="submit" value="Modifica" name="modifica"/>
						<input type="hidden" name="id" value="<?php echo  $id;?>"/>
						<input name="task" type="hidden" value="proposta.modifica" />
						<input name="option" type="hidden" value="com_cai" />
					</form>
				</td>
				<td>
					<form  method="post" >
						<input type="submit" value="Cancella" name="cancella" onclick="return popupConferma();">
						<input type="hidden" name="id" value="<?php echo  $id;?>"/>
						<input name="task" type="hidden" value="proposta.cestina" />
						<input name="option" type="hidden" value="com_cai" />
					</form>
				</td>
			</tr>
			<?php
		}
	}
	?>
</table>

<center>
	<?php
	for ($i=1; $i<=$this->pagesCount;$i++){
		?>
		<a class="pagination" href="<?php echo $this->getPageURI($i);?>" <?php if($this->page==$i) echo "style=\"font-weight: bold;\" "; ?>><?php echo $i;?></a>
		<?php
	}

	?>
</center>

<script>
	var divMSg=document.getElementById("msg");
	divMSg.style.fontSize='x-large';
	divMSg.style.width='400px';
	//divMSg.style.overflow='hidden';
	divMSg.style.height='30';
	divMSg.style.backgroundColor='white';
	divMSg.style.opacity='0.9';
	divMSg.style.border= '5px solid #3b5998';
	divMSg.style.borderRadius= '60px';
	divMSg.style.position='fixed';
	divMSg.style.top='35%';
	divMSg.style.left='0';
	divMSg.style.right='0';
	divMSg.style.marginLeft='auto';
	divMSg.style.marginRight='auto';
	document.addEventListener('click',function(){
		var divMsg=document.getElementById("msg");
		divMsg.style.display='none';
	});


</script>