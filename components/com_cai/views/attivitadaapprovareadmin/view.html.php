<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewAttivitaDaApprovareAdmin extends JViewLegacy{

	public $list=null;
	public $name;
	public $data_inizio;
	public $data_fine;

	public $pagesCount;
	public $page=1;
	public $size=10;


	function display($tpl = null) {
		$reset=JRequest::getVar("reset", null);
		if($reset==null){
			$this->name=JRequest::getVar("name", null);
			$this->data_inizio=JRequest::getVar("data_inizio", null);
			$this->data_fine=JRequest::getVar("data_fine", null);$this->page=JRequest::getVar("page", 1);
			$this->size=JRequest::getVar("size", 10);
		}
		$this->setList();
		$this->pagesCount=$this->getPagesCount();
		parent::display ( $tpl );
	}

	private function setList(){
		$propostaModel=$this->getModel("Proposta");
		$list=$propostaModel->getDaApprovare($this->name, $this->data_inizio, $this->data_fine,$this->page,$this->size);
		$this->list=$list;
	}

	private function getPagesCount(){
		$propostaModel=$this->getModel("Proposta");
		return (int)$propostaModel->getDaApprovarePages($this->name, $this->data_inizio, $this->data_fine,$this->size);
	}


	public function getPageURI($page){
		$uri = JUri::current();
		$params=array("name"=>$this->name,
			"data_inizio"=>$this->data_inizio,
			"data_fine"=>$this->data_fine,
			"page"=>$page,
			"size"=>$this->size,
			"view"=>"attivitaDaApprovareAdmin");
		$query=JUri::buildQuery($params);
		return $uri."?".$query;
	}

	public function getStatoLabel($stato){
		$propostaModel=$this->getModel("Proposta");
		$label=$propostaModel->getStatoLabel($stato);
		if($label==null){
			$label="Stato non trovato";
		}
		return $label;
	}

	public function getDateEuropeanFormat($date){
		$propostaModel=$this->getModel("Proposta");
		$newDate=$propostaModel->getDateEuropeanFormat($date);
		return $newDate;
	}
	
	public function stampaMsg(){
		$msg=JRequest::getVar("msg", null);
		if($msg!=null){
			echo"<div id=\"msg\" ><div style=\"position:relative; top:30%; margin:20px; text-align:center; line-height:1em; \">$msg</div></div>";
		}
	}
}
