<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>

<h1> Iscrizione all'attività "<?php echo $this->proposta->titolo; ?>"</h1>
<br />
<br />
<?php if($this->isModificabile){?>
<span class="iscrizione">Selezionare un socio da iscrivere:</span>
<?php }else{?>
<span class="iscrizione">Selezionare il tuo nome o uno dei tuoi
	familiari per iscriverli a questa attività:</span>
<?php }?>
<br />
<form method="POST">
<?php if($this->iscritto ==true){?>
	<span class="iscrizione">Utente iscritto con successo!</span>
<?php
}elseif($this->cancellato){
	?>
	<span class="iscrizione">Iscrizione annullata con successo!</span>
	<?php
} else {
	?>
 <span class="error"><?php echo $this->errore;?></span>
 <?php
  }?>
  <br/>
	<select name="idUtenteSelezionato">

										<?php
										$listaUtenti = $this->listaUtenti ();
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;
											?>
											<option value="<?php echo $id; ?>"
			<?php

if ($this->idUtenteSelezionato == $id) {
												echo "selected";
											}
											?>>
							 					<?php echo"$cognome"." "."$nome"; ?>
							 				</option>
										<?php
										}
										?>
	</select> <input type="submit" value="Iscrivi" name="submit"/> <input type="submit" value="Cancella" name="submit"/> <input type="hidden"
		name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
		type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
	<input name="task" type="hidden" value="iscrizione.iscrizioneSocio" /> <input
		name="option" type="hidden" value="com_cai" />
</form>

<br>
<br>
<?php
$sociIscritti = $this->getSociIscritti ();
$sociEsterni = $this->getSociDiAltreSezioniIscritti ();
$nonSoci = $this->getNonSociIscritti ();
?>

<span class="iscrizione">Numero di <b>soci</b> partecipanti:
</span>
<span><?php echo count($sociIscritti);?></span>
<br />
<br />
<span class="iscrizione">Numero di <b>soci di altre sezioni</b>
	partecipanti:
</span>
<span><?php echo count($sociEsterni);?></span>
<br />
<br />
<span class="iscrizione">Numero di <b>non soci</b> partecipanti:
</span>
<span><?php echo count($nonSoci)?></span>
<br />
<br />
<span class="iscrizione">Numero di partecipanti <b>totali</b>:
</span>
<span><?php echo count($sociIscritti)+ count($sociEsterni) +count($nonSoci);?></span>
<br />
<br />
<?php if($this->isModificabile===true){?>

<form>
	<span class="iscrizione"> Iscrivi un socio di un'altra sezione: </span>
	<br> <br>
	<div class="iscrizione">
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Nome:</div>
				<div>
					<input type="text" name="nome" placeholder="Nome...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Cognome:</div>
				<div>
					<input type="text" name="cognome" placeholder="Cognome...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Data di nascita:</div>
				<div>
					<input type="text" name="data" placeholder="Data di Nascita...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Codice Fiscale:</div>
				<div>
					<input type="text" name="cf" placeholder="Codice Fiscale...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Cellulare:</div>
				<div>
					<input type="text" name="cellulare" placeholder="Cellulare...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Email:</div>
				<div>
					<input type="text" name="email" placeholder="Email...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Sezione:</div>
				<div>
					<textarea name="sezione" placeholder="Sezione..." rows="5"
						cols="20"></textarea>
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Note:</div>
				<div>
					<textarea name="note" placeholder="Note..." rows="5" cols="20"></textarea>
				</div>
			</div>
		</div>

		<br> <br> <input type="submit" value="Iscrivi"> <input type="hidden"
		name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
		type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
	<input name="task" type="hidden" value="iscrizione.iscriviSocioEsterno" /> <input
		name="option" type="hidden" value="com_cai" />

	</div>

</form>

<br>
<br>


<form>
	<span class="iscrizione"> Iscrivi un NON socio a questa attività: </span>
	<br> <br>
	<div class="iscrizione">
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Nome:</div>
				<div>
					<input type="text" name="nome" placeholder="Nome...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Cognome:</div>
				<div>
					<input type="text" name="cognome" placeholder="Cognome...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Data di nascita:</div>
				<div>
					<input type="text" name="data" placeholder="Data di Nascita...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Codice Fiscale:</div>
				<div>
					<input type="text" name="cf" placeholder="Codice Fiscale...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Cellulare:</div>
				<div>
					<input type="text" name="cellulare" placeholder="Cellulare...">
				</div>
			</div>
			<div style="display: inline-block; width: 49%;">
				<div>Email:</div>
				<div>
					<input type="text" name="email" placeholder="Email...">
				</div>
			</div>
		</div>
		<div>
			<div style="display: inline-block; width: 49%;">
				<div>Note:</div>
				<div>
					<textarea name="note" placeholder="Note..." rows="5" cols="20"></textarea>
				</div>
			</div>
		</div>

		<br> <br> <input type="submit" value="Iscrivi"> <input type="hidden"
		name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
		type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
	<input name="task" type="hidden" value="iscrizione.iscriviNonSocio" /> <input
		name="option" type="hidden" value="com_cai" />

	</div>
</form>

<br />
<br />
<span class="iscrizione">Elenco soci iscritti a questa attività:</span>
<br />
<br />
<table class=tabSoci>
	<tr class=tabSociTitle>
		<td class=tabSociTitle>Nome</td>
		<td class=tabSociTitle>Profilo</td>
		<td class=tabSociTitle>Email</td>
		<td class=tabSociTitle>Telefono</td>
		<td class=tabSociTitle>Cellulare</td>
		<td class=tabSociTitle>Data iscrizione</td>
	</tr>
	<?php

	foreach ( $sociIscritti as $socio ) {
		?>
	<tr class=tabSoci>

		<td class=tabSoci><?php echo $socio->cognome. " ".$socio->nome; ?></td>
		<td class=tabSoci>
			<form method="post">
				<input type="submit" value="Profilo" name="profilo"> <input
					type="hidden" name="id" value="<?php echo  $socio->id;?>" /> <input
					name="task" type="hidden" value="profilo.profilo" /> <input
					name="option" type="hidden" value="com_cai" />
			</form>
		</td>
		<td class=tabSoci><?php echo $socio->email ;?></td>
		<td class=tabSoci><?php echo $socio->telefono ;?></td>
		<td class=tabSoci><?php echo $socio->cellulare ;?></td>
		<td class=tabSoci><?php echo $socio->date ;?></td>
	</tr>
	<?php }?>
</table>
<br />
<br />
<span class="iscrizione">Elenco soci di altre sezioni iscritti a questa
	attività:</span>
<br>
<br>
<table class=tabSoci>
	<tr class=tabSociTitle>

		<td class=tabSociTitle>Nome</td>
		<td class=tabSociTitle>Data di Nascita</td>
		<td class=tabSociTitle>Cellulare</td>
		<td class=tabSociTitle>Email</td>
		<td class=tabSociTitle>Codice Fiscale</td>
		<td class=tabSociTitle>Sezione</td>
		<td class=tabSociTitle>Note</td>
		<td class=tabSociTitle>Cancella</td>
	</tr>
	<?php
	foreach ( $sociEsterni as $socio ) {
		?>
	<tr class=tabSoci>

		<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
		<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
		<td class=tabSoci><?php echo $socio->cellulare ;?></td>
		<td class=tabSoci><?php echo $socio->email ;?></td>
		<td class=tabSoci><?php echo $socio->cf ;?></td>
		<td class=tabSoci><?php echo $socio->sezione ;?></td>
		<td class=tabSoci><?php echo $socio->note ;?></td>
		<td class=tabSoci>
			<form method="post">
			<input type="submit" value="Cancella"> <input type="hidden"
		name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
		type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
		<input
		type="hidden" name="id" value="<?php echo  $socio->id?>" />
	<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneSocioEsterno" /> <input
		name="option" type="hidden" value="com_cai" />

			</form>
		</td>

	</tr>
	<?php }?>
</table>
<br />
<br />

<br />
<br />
<span class="iscrizione">Elenco non soci iscritti a questa attività:</span>
<br>
<br>
<table class=tabSoci>
	<tr class=tabSociTitle>

		<td class=tabSociTitle>Nome</td>
		<td class=tabSociTitle>Data di Nascita</td>
		<td class=tabSociTitle>Cellulare</td>
		<td class=tabSociTitle>Email</td>
		<td class=tabSociTitle>Codice Fiscale</td>
		<td class=tabSociTitle>Note</td>
		<td class=tabSociTitle>Cancella</td>
	</tr>
	<?php
	foreach ( $nonSoci as $socio ) {
		?>
	<tr class=tabSoci>

		<td class=tabSoci><?php echo $socio->cognome." ".$socio->nome; ?></td>
		<td class=tabSoci><?php echo $socio->data_nascita; ?></td>
		<td class=tabSoci><?php echo $socio->cellulare ;?></td>
		<td class=tabSoci><?php echo $socio->email ;?></td>
		<td class=tabSoci><?php echo $socio->cf ;?></td>
		<td class=tabSoci><?php echo $socio->note ;?></td>
		<td class=tabSoci>
			<form method="post">
			<input type="submit" value="Cancella"> <input type="hidden"
		name="idProposta" value="<?php echo  $this->proposta->id;?>" /> <input
		type="hidden" name="idUtente" value="<?php echo  $this->idUtente;?>" />
		<input
		type="hidden" name="id" value="<?php echo  $socio->id?>" />
	<input name="task" type="hidden" value="iscrizione.cancellaIscrizioneNonSocio" /> <input
		name="option" type="hidden" value="com_cai" />

			</form>
		</td>

	</tr>
	<?php }?>
</table>
<br />
<br />

	<a class="export_link"
	   href="index.php?option=com_cai&format=raw&task=exportStatistiche.exportSingolaAttivita&idProposta=<?php echo $this->proposta->id;?>"
	   target="blank">Esporta in formato csv</a>

<?php }?>