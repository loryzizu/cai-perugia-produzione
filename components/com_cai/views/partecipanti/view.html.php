<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewPartecipanti extends JViewLegacy{
	public $isModificabile=false;
	public $idUtente;
	public $proposta;

	public $idUtenteSelezionato=null;

	public $iscritto=false;
	public $cancellato=false;
	public $errore=null;

	function display($tpl = null) {
		$this->idUtente=JRequest::getVar('idUtente', null);
		$this->idUtenteSelezionato=JRequest::getVar('idUtenteSelezionato', null);
		$id=JRequest::getVar('idProposta', null);
		$this->isModificabile=$this->getModel("Proposta")->isPropostaModificabileByCurrentUser($id);
		$this->proposta=$this->getModel("proposta")->getPropostaById($id);
		parent::display ( $tpl );
	}

	public function listaUtenti(){
		$modelUtente=$this->getModel("Utente");
		$modelProposta=$this->getModel("Proposta");
		if($this->isModificabile && $modelProposta->isPropostaModificabileByCurrentUser($this->proposta->id)){
			return $modelUtente->getUtenteList();
		}
		else{
			return $modelUtente->getNucleoFamiliare($this->idUtente);
		}
	}

	public function getSociIscritti(){
		$modelIscrizione=$this->getModel("Iscrizione");
		return $modelIscrizione->getSociIscrittiEDDGAttivita($this->proposta->id);
	}
	public function getSociDiAltreSezioniIscritti(){
		$modelIscrizione=$this->getModel("Iscrizione");
		$r=$modelIscrizione->getSociDiAltreSezioniIscrittiAttivita($this->proposta->id);
		return $r;
	}
	public function getNonSociIscritti(){
		$modelIscrizione=$this->getModel("Iscrizione");
		$r=$modelIscrizione->getNonSociIscrittiAttivita($this->proposta->id);
		return $r;
	}
}
