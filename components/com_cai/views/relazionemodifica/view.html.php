<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewRelazioneModifica extends JViewLegacy{

	public $id;
	public $titolo;
	public $relazionePrivata="";
	public $relazionePubblica="";
	public $salvataPrivata=false;
	public $salvataPubblica=false;

	function display($tpl = null) {
		$this->id = JRequest::getVar ( "id", null );
		if ($this->id != null) {
			$proposta = $this->getModel ( "Proposta" )->getPropostaById( $this->id );
			$this->titolo=$proposta->titolo;
			$jinput = JFactory::getApplication ()->input;
			$relazionePrivataA = $jinput->get ( 'relazionePrivata', array(0=>""), 'ARRAY' );
			$this->relazionePrivata=$relazionePrivataA[0];
			$relazionePubblicaA = $jinput->get ( 'relazionePubblica', array(0=>""), 'ARRAY' );
			$this->relazionePubblica=$relazionePubblicaA[0];
			$this->salvataPrivata=JRequest::getVar("salvataPrivata", false);
			$this->salvataPubblica=JRequest::getVar("salvataPubblica", false);
		}
		parent::display ( $tpl );
	}
}
