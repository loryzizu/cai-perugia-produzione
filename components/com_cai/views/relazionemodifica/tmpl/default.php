<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({
    selector: "textarea.editor",
    language_url : "<?php echo JUri::base();?>/media/com_cai/lib/tinyMCE_lang/it.js",
	language : "it",
	plugins: [
	          "advlist autolink link  lists charmap print preview hr anchor  spellchecker",
	          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime  nonbreaking",
	          "save table contextmenu directionality emoticons  paste textcolor"
	    ],
	    toolbar: " insertfile undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect",

	});
</script>
	<h1>Relazione</h1>
	<h2>Attività: <?php echo $this->titolo; ?></h2>
	<div style=" font-weight:bold; color:#3b5998">PROMEMORIA! Ricordati di scrivere e salvare una relazione per volta.</div>

    <br>
    <form  method="POST">
        <h2>Relazione riservata: </h2> <br>
        <?php if($this->salvataPrivata){?>
        <span>Relazione riservata salvata con successo!</span><br>
        <?php }?>
        <textarea class="editor" name="relazionePrivata" placeholder="Scrivi relazione..." cols="60" rows="20"><?php echo $this->relazionePrivata; ?></textarea> <br>
        <input type="submit" name="salvaPrivata" value="Salva"> <br><br>
        <h2>Relazione pubblica: </h2> <br>
		 <?php if($this->salvataPubblica){?>
        <span>Relazione pubblica salvata con successo!</span><br>
        <?php }?>
        <textarea class="editor" name="relazionePubblica" placeholder="Scrivi relazione..." cols="60" rows="20"><?php echo $this->relazionePubblica; ?></textarea> <br>
        <input type="hidden" name="id" value="<?php echo $this->id; ?>"/> <br>
        <input type="submit" name="salvaPubblica" value="Salva"> <br>
        <input type="hidden" name="option" value="com_cai"/>
        <input type="hidden" name="task" value="relazione.salva"/>
    </form>