<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class CAIViewExportStatistiche extends JViewLegacy
{
	public $stato;
    function display($tpl = null)
    {
    $this->stato=JRequest::getVar("stato", 0);
		if($this->stato==1){
			$id=JRequest::getVar("utente" , null);
			if($id != null){
				$propostaModel = $this->getModel ( "Proposta" );
				$utente=$this->getModel("utente")->getUserById($id);
				$proposte=$propostaModel->getProposteOrganizzateListAllByUserId (  null, null, null, $id );
				$partecipazioni=$propostaModel->getProposteListByUserId ( "Svolta", null, null, null, $id );
				$iscrizioni=$propostaModel->getProposteListByUserId ( "Approvata", null, null, null, $id );
				$this->getModel ( "Export" )->inviaFileSingoloUtente($proposte, $partecipazioni, $iscrizioni, $utente->cognome."_".$utente->nome);
			}
		}
		elseif($this->stato==2){
			$propostaModel = $this->getModel ( "Proposta" );
			$id=JRequest::getVar("idProposta" , null);
			if($id != null){
				$modelIscrizione=$this->getModel("Iscrizione");
				$proposta=$propostaModel->getPropostaById($id);
				$soci=$modelIscrizione->getSociIscrittiEDDGAttivita($id);
				$sociEsterni=$modelIscrizione->getSociDiAltreSezioniIscrittiAttivita($id);
				$nonSoci=$modelIscrizione->getNonSociIscrittiAttivita($id);
				@$this->getModel ( "Export" )->inviaFileSingolaAttivita($soci, $sociEsterni, $nonSoci, $proposta->titolo."_".$propostaModel->getDateEuropeanFormat($proposta->data_inizio));
			}
		}
		elseif($this->stato==3){
			$this->tipologia = JRequest::getVar ( "tipologia", null );
			$this->data_inizio = JRequest::getVar ( "data_inizio", null );
			$this->data_fine = JRequest::getVar ( "data_fine", null );
			$modelIscrizione=$this->getModel("Iscrizione");
			$this->uniciTotali=JRequest::getVar("uniciTotali", "unici");
			$propostaModel = $this->getModel ( "Proposta" );
			if($this->uniciTotali=="unici"){
				$soci=$modelIscrizione->getSociIscrittiEDDGByTipologiaEDataUnici($this->tipologia, $this->data_inizio, $this->data_fine);
				$sociEsterni=$modelIscrizione->getSociDiAltreSezioniIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
				$nonSoci=$modelIscrizione->getNonSociIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
				@$this->getModel ( "Export" )->inviaFileCategoriaEDataUnici($soci, $sociEsterni, $nonSoci, "UNICI_".$this->tipologia."__".$this->data_inizio."__".$this->data_fine);

			}
			elseif($this->uniciTotali=="totali"){
				$soci=$modelIscrizione->getSociIscrittiEDDGByTipologiaEDataAll($this->tipologia, $this->data_inizio, $this->data_fine);
				$sociEsterni=$modelIscrizione->getSociDiAltreSezioniIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
				$nonSoci=$modelIscrizione->getNonSociIscrittiByTipologiaEData($this->tipologia, $this->data_inizio, $this->data_fine);
				@$this->getModel ( "Export" )->inviaFileCategoriaEDataTotali($soci, $sociEsterni, $nonSoci, "TOTALI_".$this->tipologia."__".$this->data_inizio."__".$this->data_fine);

			}
		}
    }





}

