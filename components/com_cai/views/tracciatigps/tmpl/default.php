<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<script type="text/javascript" src="media/com_cai/js/calendar/calendar.js"></script>
<h1>Tracciati GPS</h1>

<p><span style="font-size: 12pt; font-family: georgia, palatino;"><img style="display: block; margin-left: auto; margin-right: auto;" src="images/classic_30326f5cfc1d4dd68af4d1f35a85c169.jpg" alt="" width="194" height="194" /></span></p>
<p style="text-align: justify;"><span style="font-family: georgia, palatino; font-size: 12pt;">In questa pagina sono riportate tutte le iniziative escursionistiche svolte (a piedi, con gli sci o in bicicletta) che hanno in allegato <span style="color: #ff0000;"><strong>un tracciato GPS</strong></span>. Cliccando sulla denominazione della iniziativa si aprirà una videata contenente la locandina dell'iniziativa a suo tempo comparsa sul sito. Si scorre la locandina fino ad arrivare a "<em>Allegati</em>", si clicca sull'allegato con estensione .GPX avviando il download del file di tracciato. Tutti i file di tracciato sono in formato GPX.</span></p>

<form method="GET">
	<input type="hidden" name="view" value="tracciatigps">
	<div>Filtra:</div>
	<br/>
	<input type="text" placeholder="Nome" name="name" value="<?php echo $this->name;?>"/><br/><br/>

	<input type="text" placeholder="Da..."  name="data_inizio" value="<?php echo $this->data_inizio;?>" onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<input type="text" placeholder="A..."  name="data_fine"  value="<?php echo $this->data_fine;?>"onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<br/><br/>
	<div>Elementi per pagina:</div>
	<select name="size" >
		<option value="10" <?php if($this->size==10) echo "selected"; ?>>10</option>
		<option value="25" <?php if($this->size==25) echo "selected"; ?>>25</option>
		<option value="50" <?php if($this->size==50) echo "selected"; ?>>50</option>
		<option value="0"  <?php if($this->size==0) echo "selected"; ?>>Tutti</option>
	</select>
	<br>
	<br>
	<input type="submit" name="filtra" value="Filtra">
	<input type="submit" name="reset" value="Annulla Filtro" ">
	<br/>
</form>
<br/><br/>

<table class="listaProposte">
	<tr style="width:100%;">
		<th>Attività con tracciato GPS</th>
		<th>Data</th>
	</tr>
<?php
	$list=$this->list;
	for ($i=0; $i<count($list); $i++){
?>
	<tr>
		<td>
			<a href="<?php echo $list[$i]->linkArticolo;?>"><?php echo $list[$i]->titolo;?></a>
		</td>
		<td><?php echo $this->getDateEuropeanFormat($list[$i]->data_inizio); ?></td>
	</tr>
<?php
	}
?>

</table>

<center>
	<?php
	for ($i=1; $i<=$this->pagesCount;$i++){
		?>
		<a class="pagination" href="<?php echo $this->getPageURI($i);?>" <?php if($this->page==$i) echo "style=\"font-weight: bold;\" "; ?>><?php echo $i;?></a>
		<?php
	}

	?>
</center>
