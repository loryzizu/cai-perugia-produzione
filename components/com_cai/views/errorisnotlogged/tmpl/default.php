<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>

<div class="bigMessageContainer">
	<i class="material-icons big grey">lock</i>
	<div class="greyMessage">Esegui il login per avere accesso a queste funzionalità esclusive dei soci CAI.</div>
</div>
