<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
?>

<h1>Rinnovo iscrizioni alla sezione</h1>

<div>
	<a
		href="index.php?option=com_cai&view=rinnovoIscrizioniAdmin&mode=aggiorna">Aggiorna
		iscritti</a>
</div>

<div>
	<a
		href="index.php?option=com_cai&view=rinnovoIscrizioniAdmin&mode=cambiaAnno">Effettua
		il passaggio al nuovo anno di iscrizioni</a>
</div>

<br />
<br />

<?php if(isset($this->rinnovato) && $this->rinnovato== true ){?>
<span class="iscrizione">Rinnovo utenti avvenuto con successo</span>
<br />
<br />

<span>Righe lette dal file: <?php echo $this->righe;?> </span>
<br />
<span>Nuovi utenti aggiunti: <?php echo $this->nuovi;?> </span>
<br />
<span>Utenti del file che risultavano già iscritti: <?php echo $this->aggiornati;?> </span>
<br />
<br />
<?php }elseif(isset($this->rinnovato)  && $this->rinnovato== false ){?>
<span class="error">Errore nella lettura del file inserito, ricordati di
	inserire un file csv, del formato corretto, avente come separatore: ","
	o ";" o "\t". Il file potrebbe essere di dimensioni troppo grandi per
	il server, prova a suddividere il file in file più piccoli. E' importante che la prima riga contenga l'intestazione dei campi.</span>

<?php
}

if (isset ( $this->mode ) && $this->mode == 'aggiorna') {
	?>
<h3>Aggiornamento lista utenti.</h3>
<div>Inserire il file in formato csv per l'aggiornamento dell'elenco dei
	soci.</div>
<form method="post" enctype="multipart/form-data">
	<label for="file">Filename:</label> <input type="file" name="file"
		id="file" /> <input type="hidden" value="com_cai" name="option"> <input
		type="hidden" name="task" value="rinnovo.aggiorna" /> <br> <input
		type="submit" name="submit" value="Aggiorna">
</form>
<?php
} elseif (isset ( $this->mode ) && $this->mode == 'cambiaAnno') {
	?>
<h3>Passaggio all'anno successivo.</h3>
<div>Inserire il file in formato csv per l'aggiornamento dell'elenco dei
	soci. Solo i soci presenti nel file saranno memorizzati nel database,
	mentre gli utenti che non hanno rinnovato verranno eliminati.</div>
<form method="post" enctype="multipart/form-data">
	<label for="file">Filename:</label> <input type="file" name="file"
		id="file" /> <input type="hidden" value="com_cai" name="option"> <input
		type="hidden" name="task" value="rinnovo.cambiaAnno" /> <br> <input
		type="submit" name="submit" value="Aggiorna">
</form>
<?php
}
?>
<br />
<br />



<div style="display: ; max-width: 95% !important;">
	<span class="iscrizione">Elenco soci iscritti:</span>
	<table class="listaProposte fixed soci" >
		<tr style="width: 100%;">
			<th>Nome</th>
			<th>Profilo</th>
			<th>Data di nascita</th>
			<th>Codice Fiscale</th>
			<th>Email</th>
			<th>Telefono</th>
			<th>Cellulare</th>
			<th>Titoli</th>
			<th>Professione</th>
			<th>Note</th>
		</tr>
<?php

foreach ( $this->sociIscritti as $socio ) {
	?>
		<tr>

			<td><div><?php echo $socio->cognome. " ".$socio->nome; ?></div></td>
			<td>
				<form method="post">
					<input type="submit" value="Profilo"
						name="<?php echo $socio->username;?>"> <input type="hidden"
						name="id" value="<?php echo  $socio->id;?>" /> <input name="task"
						type="hidden" value="profilo.profilo" /> <input name="option"
						type="hidden" value="com_cai" />
				</form>
			</td>
			<td><div><?php echo $socio->data_nascita;?>	</div></td>
			<td><div><?php echo $socio->CF;?>			</div></td>
			<td><div><?php echo $socio->email ;?>		</div></td>
			<td><div><?php echo $socio->telefono ;?>	</div></td>
			<td><div><?php echo $socio->cellulare ;?>	</div></td>
			<td><div><?php echo $socio->titoli;?>		</div></td>
			<td><div><?php echo $socio->professione ;?>	</div></td>
			<td><div><?php echo $socio->note ;?>		</div></td>
		</tr></div>
<?php }?>
	</table>
</div>
<br />
<br />

<input type="submit" value="Mostra vecchi iscritti"
	onclick="mostraVecchi()" />

<br />
<br />

<div style="display:none ; max-width: 95% !important;" id="vecchi">
	<span class="iscrizione">Elenco vecchi iscritti:</span>
	<table class="listaProposte fixed soci" >
		<tr style="width: 100%;">
			<th>Nome</th>
			<th>Profilo</th>
			<th>Data di nascita</th>
			<th>Codice Fiscale</th>
			<th>Email</th>
			<th>Telefono</th>
			<th>Cellulare</th>
			<th>Titoli</th>
			<th>Professione</th>
			<th>Note</th>
		</tr>
<?php

foreach ( $this->sociVecchi as $socio ) {
	?>
				<tr>

			<td><div><?php echo $socio->cognome. " ".$socio->nome; ?></div></td>
			<td>
				<form method="post">
					<input type="submit" value="Profilo"
						name="<?php echo $socio->username;?>"> <input type="hidden"
						name="id" value="<?php echo  $socio->id;?>" /> <input name="task"
						type="hidden" value="profilo.profilo" /> <input name="option"
						type="hidden" value="com_cai" />
				</form>
			</td>
			<td><div><?php echo $socio->data_nascita;?>	</div></td>
			<td><div><?php echo $socio->CF;?>			</div></td>
			<td><div><?php echo $socio->email ;?>		</div></td>
			<td><div><?php echo $socio->telefono ;?>	</div></td>
			<td><div><?php echo $socio->cellulare ;?>	</div></td>
			<td><div><?php echo $socio->titoli;?>		</div></td>
			<td><div><?php echo $socio->professione ;?>	</div></td>
			<td><div><?php echo $socio->note ;?>		</div></td>
		</tr>
<?php }?>
</table>
</div>
<br />
<br />
<script>
	function mostraVecchi(){
			var display=document.getElementById("vecchi").style.display;
			if(display=="none"){
				document.getElementById("vecchi").style.display="";

				}
			else{
				document.getElementById("vecchi").style.display="none";
				}
		}
</script>

