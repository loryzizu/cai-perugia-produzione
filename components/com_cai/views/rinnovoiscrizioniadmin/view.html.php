<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewRinnovoIscrizioniAdmin extends JViewLegacy{

	public $rinnovato=null;

	public $mode=null;
	public $righe=null;
	public $nuovi=null;
	public $aggiornati=null;
	public $aggiornatoStato=null;

	public $sociIscritti;
	public $sociVecchi;
	
	function display($tpl = null) {
		$this->sociIscritti=$this->getModel("Utente")->getUtenteList();
		$this->sociVecchi=$this->getModel("Utente")->getUtenteListOld();
		$this->mode=JRequest::getVar("mode", null);
		$this->rinnovato=JRequest::getVar("rinnovato", null);
		if(isset($this->rinnovato) && $this->rinnovato==true){
			$this->mode=null;
            $this->righe=JRequest::getVar("righe", null);
			$this->nuovi=JRequest::getVar("nuovi", null);
			$this->aggiornati=JRequest::getVar("aggiornati", null);
			$this->aggiornatoStato=JRequest::getVar("aggiornatoStato", null);
		}
		parent::display ( $tpl );
	}
}
