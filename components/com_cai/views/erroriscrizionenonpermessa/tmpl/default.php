<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<div class="bigMessageContainer">
	<i class="material-icons big grey">mood_bad</i>
	<div class="greyMessage">L'attività alla quale vuoi iscriverti è già stata svolta o è stata annullata.</div>
</div>
