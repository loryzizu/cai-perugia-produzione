<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );


if($this->recupero==true){
?>
<div class="bigMessageContainer">
	<i class="material-icons big grey">mood</i>
	<div class="greyMessage">La tua nuova password è stata inviata al tuo indirizzo email.</div>
</div>
<?php }else{?>

	<h2>Recupero password</h2>
	<div>Inserisci uno dei seguenti campi per recuperare la password del tuo account, ti verrà inviata una nuova password all'indirizzo email fornito in fase di iscrizione.</div>
    <?php if($this->error===true){?>
    <div class="error">Inserisci un username o un codice fiscale valido.</div>
    <?php }?>
    <form style="width:100%">
    	<div style="width:30%">
	   	 	<label><b>Username:</b></label> <input type="text" name="username" style="float:right"/><br style="clear:both"><br>
	   	 	<label><b>Codice Fiscale: </b></label> <input type="text" name="cf" style="float:right"/><br><br>
	   	 	<input type="submit" value="Invia"/><br>
	   	 	<input type="hidden" name="option" value="com_cai"/>
	   	 	<input type="hidden" name="task" value="utente.recuperoPassword"/>
   	 	</div>
    </form>
    <?php }?>