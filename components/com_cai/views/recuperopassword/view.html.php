<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewRecuperoPassword extends JViewLegacy{

	public $recupero=false;
	public $error=false;
	
	function display($tpl = null) {
		$this->recupero=JRequest::getVar("recupero", false);
		$this->error=JRequest::getVar("error", false);
		parent::display ( $tpl );
	}
}
