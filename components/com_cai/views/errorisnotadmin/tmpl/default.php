<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<div class="bigMessageContainer">
	<i class="material-icons big grey">lock</i>
	<div class="greyMessage">Ops! Come sei finito qui? Mi dispiace ma se non sei un amministratore non puoi vedere quello che c'è qui sotto. TOP SECRET!</div>
</div>
