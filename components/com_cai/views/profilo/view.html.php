<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewProfilo extends JViewLegacy{
	public $id = null;
	public $nomeUtente = "";
	public $fotoPath = "";
	public $stato = "0";
	public $isSegretario = false;

	// filtro
	public $name;
	public $data_inizio;
	public $data_fine;
	public $pagesCount;
	public $page=1;
	public $size=10;
	function display($tpl = null) {
		$this->isSegretario = $this->isSegretario ();

		if ($this->isSegretario) {
			$this->id = JRequest::getVar ( "id", null );
		}
		$this->nomeUtente = $this->getNomeUtente ();
		$this->fotoPath = "";
		$this->stato = JRequest::getVar ( "stato", "0" );
		$reset = JRequest::getVar ( "reset", null );
		if ($reset == null) {
			$this->name = JRequest::getVar ( "name", null );
			$this->data_inizio = JRequest::getVar ( "data_inizio", null );
			$this->data_fine = JRequest::getVar ( "data_fine", null );
			$this->page=JRequest::getVar("page", 1);
			$this->size=JRequest::getVar("size", 10);
		}
		switch ($this->stato){
			case 2:
				$this->pagesCount=$this->getPagesCountSvolte();
				break;
			case 3:
				$this->pagesCount=$this->getPagesCountPrenotate();
				break;
			case 4:
				$this->pagesCount=$this->getPagesCountProposte();
				break;
		}
		parent::display ( $tpl );
	}
	private function getNomeUtente() {
		if ($this->isSegretario && $this->id != null) {
			$u = $this->getModel ( "Utente" )->getUserById ( $this->id );
			if (isset ( $u )) {
				return "$u->cognome $u->nome";
			}
		} else {
			return $this->getModel ( "Utente" )->getCurrentUserCognomeNome ();
		}
	}
	private function isSegretario() {
		$modelUtente=$this->getModel("Utente");
		return  $modelUtente->isSegretario ();
	}
	public function getUtente() {
		if ($this->isSegretario && $this->id != null) {
			return $this->getModel ( "Utente" )->getUserById ( $this->id );
		} else {
			return $this->getModel ( "Utente" )->getCurrentUser ();
		}
	}
	public function getStatoLabel($stato) {
		$propostaModel = $this->getModel ( "Proposta" );
		$label = $propostaModel->getStatoLabel ( $stato );
		if ($label == null) {
			$label = "Stato non trovato";
		}
		return $label;
	}
	public function getStatoUtenteLabel($stato) {
		$propostaModel = $this->getModel ( "Utente" );
		$label = $propostaModel->getStatoLabel ( $stato );
		if ($label == null) {
			$label = "Stato non trovato";
		}
		return $label;
	}
	public function getListSvolte() {
		$propostaModel = $this->getModel ( "Proposta" );
		if ($this->isSegretario && $this->id != null) {
			$list = $propostaModel->getProposteListByUserId ( "Svolta", $this->name, $this->data_inizio, $this->data_fine, $this->id ,$this->page,$this->size);
		} else {
			$list = $propostaModel->getProposteListByCurrentUser ( "Svolta", $this->name, $this->data_inizio, $this->data_fine ,$this->page,$this->size);
		}
		return $list;
	}

	public function getPagesCountSvolte(){
		$propostaModel=$this->getModel("Proposta");
		if ($this->isSegretario && $this->id != null) {
			return (int) $propostaModel->getProposteListByUserIdPages ( "Svolta", $this->name, $this->data_inizio, $this->data_fine, $this->id ,$this->size);
		} else {
			return (int) $propostaModel->getProposteListByCurrentUserPages ( "Svolta", $this->name, $this->data_inizio, $this->data_fine ,$this->size);
		}
	}


	public function getPageURISvolte($page){
		$uri = JUri::current();
		$params=array("name"=>$this->name,
			"data_inizio"=>$this->data_inizio,
			"data_fine"=>$this->data_fine,
			"page"=>$page,
			"size"=>$this->size,
			"task"=>"profilo.profilo",
			"stato"=>2
			);
		$query=JUri::buildQuery($params);
		return $uri."?".$query;
	}

	public function getListPrenotate() {

		if ($this->isSegretario && $this->id != null) {
			$iscrizioniModel=$this->getModel( "Iscrizione" );
			$list = $iscrizioniModel->getIscrizioniNonSvolteListByUserId ( $this->name, $this->data_inizio, $this->data_fine , $this->id,$this->page,$this->size);
		} else {
			$iscrizioniModel=$this->getModel( "Iscrizione" );
			$list = $iscrizioniModel->getIscrizioniNonSvolteListByCurrentUser ( $this->name, $this->data_inizio, $this->data_fine,$this->page,$this->size );
		}
		return $list;
	}

	public function getPagesCountPrenotate(){
		if ($this->isSegretario && $this->id != null) {
			$iscrizioniModel=$this->getModel( "Iscrizione" );
			return (int) $iscrizioniModel->getIscrizioniNonSvolteListByUserIdPages ( $this->name, $this->data_inizio, $this->data_fine , $this->id,$this->size);
		} else {
			$iscrizioniModel=$this->getModel( "Iscrizione" );
			return (int) $iscrizioniModel->getIscrizioniNonSvolteListByCurrentUserPages ( $this->name, $this->data_inizio, $this->data_fine,$this->size );
		}
	}


	public function getPageURIPrenotate($page){
		$uri = JUri::current();
		$params=array("name"=>$this->name,
			"data_inizio"=>$this->data_inizio,
			"data_fine"=>$this->data_fine,
			"page"=>$page,
			"size"=>$this->size,
			"task"=>"profilo.profilo",
			"stato"=>3
		);
		$query=JUri::buildQuery($params);
		return $uri."?".$query;
	}
	public function getListOrganizzateAll() {
		$propostaModel = $this->getModel ( "Proposta" );
		if ($this->isSegretario && $this->id != null) {
			$list = $propostaModel->getProposteOrganizzateListAllByUserId (  $this->name, $this->data_inizio, $this->data_fine, $this->id,$this->page,$this->size );
		} else {
			$list = $propostaModel->getProposteOrganizzateListAllByCurrentUser ( $this->name, $this->data_inizio, $this->data_fine,$this->page,$this->size );
		}
		return $list;
	}

	public function getPagesCountProposte() {
		$propostaModel = $this->getModel ( "Proposta" );
		if ($this->isSegretario && $this->id != null) {
			return (int) $propostaModel->getProposteOrganizzateListAllByUserIdPages (  $this->name, $this->data_inizio, $this->data_fine, $this->id,$this->size );
		} else {
			return (int) $propostaModel->getProposteOrganizzateListAllByCurrentUserPages ( $this->name, $this->data_inizio, $this->data_fine,$this->size );
		}
	}

	public function getPageURIProposte($page){
		$uri = JUri::current();
		$params=array("name"=>$this->name,
			"data_inizio"=>$this->data_inizio,
			"data_fine"=>$this->data_fine,
			"page"=>$page,
			"size"=>$this->size,
			"task"=>"profilo.profilo",
			"stato"=>4
		);
		$query=JUri::buildQuery($params);
		return $uri."?".$query;
	}
	
	public function getDateEuropeanFormat($date) {
		$propostaModel = $this->getModel ( "Proposta" );
		$newDate = $propostaModel->getDateEuropeanFormat ( $date );
		return $newDate;
	}
	public function getLinkArticoloProposta($idProposta) {
		return $this->getModel ( "Proposta" )->getLinkArticoloProposta ( $idProposta );
	}
	public function mostraRelazioneOIscrizioneONiente($proposta) {
		$label = $this->getStatoLabel ( $proposta->stato );
		if ($label == "Svolta") {
			return "relazione";
		} elseif ($label == "Approvata" || $label == "Approvata con modifiche") {
			return "iscrizione";
		}
		return "Niente";
	}
	public function mostraEmailONiente($proposta) {
		$label = $this->getStatoLabel ( $proposta->stato );
		if ($label == "Svolta" || $label == "Approvata" || $label == "Approvata con modifiche") {
			return "email";
		}
		return "Niente";
	}
	
	public function stampaMsg(){
		$msg=JRequest::getVar("msg", null);
		if($msg!=null){
			echo"<div id=\"msg\" ><div style=\"position:relative; top:30%; margin:20px; text-align:center; line-height:1em; \">$msg</div></div>";
		}
 	}
 	
 	public function getErrorCambiaPassword(){
 		return JRequest::getVar("error","");
 	}
 	
 	public function cambiataPassword(){
 		return JRequest::getVar("cambiata", false);
 	}
}
