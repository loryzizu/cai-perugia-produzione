<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
?>

<h2>Statistiche</h2>

<div id="statistiche">
	<div class="divBulo rientrato">Attività a cui ho partecipato: <?php echo count($this->getListSvolte()) ; ?> </div>

	<div class="divBulo rientrato">Attività che ho organizzato: <?php echo count($this->getListOrganizzateAll()) ; ?> </div>

	<div class="divBulo rientrato">Prenotazioni: <?php echo count($this->getListPrenotate()) ; ?> </div>
</div>

<h2>Informazioni</h2>

<div id="informazioni">
<?php $utente= $this->getUtente();?>
	<div class="divBulo rientrato">Email: <?php echo $utente->email ; ?> </div>

	<div class="divBulo rientrato">Username: <?php echo $utente->username ; ?> </div>

	<div class="divBulo rientrato">Telefono: <?php echo $utente->telefono ; ?> </div>

	<div class="divBulo rientrato">Cellulare: <?php echo $utente->cellulare ; ?> </div>

	<div class="divBulo rientrato">Stato: <?php echo $this->getStatoUtenteLabel($utente->stato) ; ?> </div>
</div>

