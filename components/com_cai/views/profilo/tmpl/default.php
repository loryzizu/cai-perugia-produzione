<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
?>
<?php 
	$this->stampaMsg();
	?>	 

<!-- <img src="<?php echo $this->fotoPath?>" class="immagineProfilo"
	height="100px" width="100px"> -->
	<div class="smallMessageContainer">
	<i class="material-icons small grey">person</i>
	<div class="greyMessage"> <?php echo $this->nomeUtente?></div>
</div>
<br>
<table width="100%" class="menuProfilo fontBulo">
	<tr>
		<td width="50%"><a
			href="index.php?option=com_cai&task=profilo.profilo<?php if($this->isSegretario) echo "&id=$this->id";?>&stato=0" style="color: white; font-family: Arial, Helvetica, sans-serif; font-weight: bolder;"><center>Dati</center></a></td>
		<td><a href="index.php?option=com_cai&task=profilo.profilo<?php if($this->isSegretario) echo "&id=$this->id";?>&stato=1" style="color: white; font-family: Arial, Helvetica, sans-serif; font-weight: bolder;"><center>Impostazioni</center></a></td>
	</tr>
</table>
<br>


<table class="menuProfilo fontBulo" >
	<tr>
		<td width="33%"><center>
				<a
					href="index.php?option=com_cai&task=profilo.profilo<?php if($this->isSegretario) echo "&id=$this->id";?>&stato=2" style="color: white; font-family: Arial, Helvetica, sans-serif; font-weight: bolder;">
					Attività
					Svolte</a>
			</center></td>
		<td><center>
				<a
					href="index.php?option=com_cai&task=profilo.profilo<?php if($this->isSegretario) echo "&id=$this->id";?>&stato=3" style="color: white; font-family: Arial, Helvetica, sans-serif; font-weight: bolder;">Prenotazioni</a>
			</center></td>
		<td width="33%"><center>
				<a
					href="index.php?option=com_cai&task=profilo.profilo<?php if($this->isSegretario) echo "&id=$this->id";?>&stato=4" style="color: white; font-family: Arial, Helvetica, sans-serif; font-weight: bolder;">Proposte</a>
			</center></td>
	</tr>
</table>
<br>

<?php
	if(trim($this->stato)=="0"){
		include_once JPATH_ROOT.'/components/com_cai/views/profilo/tmpl/datiUtente.php';
	}
	elseif(trim($this->stato)=="1"){
		include_once JPATH_ROOT.'/components/com_cai/views/profilo/tmpl/impostazioni.php';
	}
	elseif(trim($this->stato)=="2"){
		include_once JPATH_ROOT.'/components/com_cai/views/profilo/tmpl/attivitaSvolte.php';
	}
	elseif(trim($this->stato)=="3"){
		include_once JPATH_ROOT.'/components/com_cai/views/profilo/tmpl/prenotazioni.php';
	}
	elseif(trim($this->stato)=="4"){
		include_once JPATH_ROOT.'/components/com_cai/views/profilo/tmpl/proposte.php';
	}
?>

<script>
var divMSg=document.getElementById("msg");
divMSg.style.fontSize='x-large';
divMSg.style.width='400px';
//divMSg.style.overflow='hidden';
divMSg.style.height='30';
divMSg.style.backgroundColor='white';
divMSg.style.opacity='0.9';
divMSg.style.border= '5px solid #3b5998';
divMSg.style.borderRadius= '60px';
divMSg.style.position='fixed';
divMSg.style.top='35%';
divMSg.style.left='0';
divMSg.style.right='0';
divMSg.style.marginLeft='auto';
divMSg.style.marginRight='auto';
document.addEventListener('click',function(){
    var divMsg=document.getElementById("msg");
    divMsg.style.display='none';
});


</script>