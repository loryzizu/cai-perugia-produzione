<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );
?>
<script>
	
</script>
<h2>Impostazioni</h2>


<h3>Cambia Password</h3>
<br/>
<span class="error" id="errorPassword"><?php echo $this->getErrorCambiaPassword();?></span><br/>
<?php if($this->cambiataPassword()){?>
<span class="positiveMessage">Password cambiata con successo!</span><br/>
<?php } ?>
<form  >
	<div style="width:300px">
		<div style="width:100%; clear:both; margin-bottom:10px;">Vecchia password: <input type="password"  id="old"  name="old" style="float:right;" /></div>
		<div style="width:100%; clear:both; margin-bottom:10px;">Nuova password: <input type="password" id="new" name="new" style="float:right"/></div>
		<div style="width:100%; clear:both; margin-bottom:10px;">Conferma password: <input type="password" id="newC" name="newC" style="float:right"/></div>
		<br/>
		<input type="submit" value="Invia"/>
		<input type="hidden" value="<?php echo $this->getUtente()->id;?>" name="id"/>
		<input type="hidden" name="option" value="com_cai"/>
		<input type="hidden" name="task" value="utente.cambiaPassword"/>
	</div>
</form>
