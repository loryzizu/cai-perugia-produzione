<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );

?>
<script type="text/javascript" src="media/com_cai/js/calendar/calendar.js"></script>
<h2>Proposte</h2>
<br/>
<form method="GET">
	<input type="hidden" name="task" value="profilo.profilo">
	<input type="hidden" name="stato" value="4">
	<div>Filtra:</div>
	<br/>
	<input type="text" placeholder="Nome" name="name" value="<?php echo $this->name;?>"/><br/><br/>

	<input type="text" placeholder="Da..."  name="data_inizio" value="<?php echo $this->data_inizio;?>" onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<input type="text" placeholder="A..."  name="data_fine"  value="<?php echo $this->data_fine;?>"onclick="Calendar.show(this, '%d/%m/%Y', false)" onfocus="Calendar.show(this, '%d/%m/%Y', false)" onblur="Calendar.hide()" readonly  />
	<br/><br/>
	<div>Elementi per pagina:</div>
	<select name="size" >
		<option value="10" <?php if($this->size==10) echo "selected"; ?>>10</option>
		<option value="25" <?php if($this->size==25) echo "selected"; ?>>25</option>
		<option value="50" <?php if($this->size==50) echo "selected"; ?>>50</option>
		<option value="0"  <?php if($this->size==0) echo "selected"; ?>>Tutti</option>
	</select>
	<br>
	<br>
	<input type="submit" name="filtra" value="Filtra">
	<input type="submit" name="reset" value="Annulla Filtro" >
</form>
<br/><br/>
<table class="listaProposte">
	<tr style="width:100%;">
		<th>Titolo</th>
		<th>Data</th>
		<th>Stato</th>
		<th>Modifica</th>
		<th>Iscrivi/Relazione</th>
		<th>Invia Email Iscritti</th>
	</tr>
	<?php
	$list=$this-> getListOrganizzateAll();
	if(isset($list)){
		for ($i=0; $i<count($list); $i++){
			$id=$list[$i]->id;
			$titolo=$list[$i]->titolo;
			$linkArticolo=$this->getLinkArticoloProposta($id);
			$data_inizio=$this->getDateEuropeanFormat($list[$i]->data_inizio);
			$statoLabel=$this->getStatoLabel($list[$i]->stato);
			$stato=$list[$i]->stato;
			$mostraRelazioneOIscrizioneONiente=$this->mostraRelazioneOIscrizioneONiente($list[$i]);
			$mostraEmailONiente=$this->mostraEmailONiente($list[$i]);
			?>
			<tr>
				<td><?php if($linkArticolo){?><a href="<?php echo $linkArticolo ;?>"><?php }echo $titolo ;?></a></td>
				<td><?php echo $data_inizio ;?></td>
				<td><?php echo $statoLabel ;?></td>
				<td>
					<?php
					if($stato!=6){
						?>
						<form  method="post" >
							<input type="submit" value="Modifica" name="modifica"/>
							<input type="hidden" name="id" value="<?php echo  $id;?>"/>
							<input name="task" type="hidden" value="proposta.modifica" />
							<input name="option" type="hidden" value="com_cai" />
						</form>
						<?php
					}?>
				</td>
				<td>
					<?php
					if($mostraRelazioneOIscrizioneONiente=="iscrizione"){
						?>
						<form  method="post" >
							<input type="submit" value="Iscrivi" name="partecipanti">
							<input type="hidden" name="id" value="<?php echo  $id;?>"/>
							<input name="task" type="hidden" value="iscrizione.display" />
							<input name="option" type="hidden" value="com_cai" />
						</form>
						<?php
					}
					elseif($mostraRelazioneOIscrizioneONiente=="relazione"){
						?>
						<form  method="post" >
							<input type="submit" value="Scrivi Relazione" name="relazione">
							<input type="hidden" name="id" value="<?php echo  $id;?>"/>
							<input name="task" type="hidden" value="relazione.modifica" />
							<input name="option" type="hidden" value="com_cai" />
						</form>
						<form  method="post" >
							<input type="submit" value="Iscrizioni" name="partecipanti">
							<input type="hidden" name="id" value="<?php echo  $id;?>"/>
							<input name="task" type="hidden" value="iscrizione.display" />
							<input name="option" type="hidden" value="com_cai" />
						</form>
						<?php
					}?>
				</td>

				<td>
					<?php
					if($mostraEmailONiente=="email"){
						?>
						<form  method="post" >
							<input type="submit" value="Email" name="email">
							<input type="hidden" name="id" value="<?php echo  $id;?>"/>
							<input name="task" type="hidden" value="email.display" />
							<input name="option" type="hidden" value="com_cai" />
						</form>
						<?php
					}
					?>
				</td>

			</tr>
			<?php
		}
	}
	?>
</table>
<center>
	<?php
	for ($i=1; $i<=$this->pagesCount;$i++){
		?>
		<a class="pagination" href="<?php echo $this->getPageURIProposte($i);?>" <?php if($this->page==$i) echo "style=\"font-weight: bold;\" "; ?>><?php echo $i;?></a>
		<?php
	}

	?>
</center>
