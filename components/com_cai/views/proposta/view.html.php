<?php

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted access' );

// import Joomla view library
jimport ( 'joomla.application.component.view' );
class CAIViewProposta extends JViewLegacy{
	
	public $idUtente='1';
	public $proposta;
	public $gruppi;
	public $propostaErrors;
	public $mostraFiles=false;
    public $mode="crea";

	public $showIdProposta=false;
	public $showCreaButton=false;
	public $showSalvaModificheButton=false;
	public $showRipristinaButton=false;
	public $showRipristinaAdminButton=false;
	public $showRipristinaSvoltaAdminButton=false;
	public $showApprovaButton=false;
	public $showApprovaESalvaModificheButton=false;
	public $showNonApprovareMaSalvaModificheButton=false;
	public $showNonApprovareButton=false;

    public $segretarioEditing=false;

	function display($tpl = null) {
		$this->mode=JRequest::getVar("mode","crea");
		$this->setTask();
		$this->gruppi=$this->getGruppi();
		if($this->mode=="crea"){
			$this->proposta = $this->get('PropostaFromRequest');
		}
		else{//sono in modifica o visualizza, ho la proposta caricata dal db alla prima botta, poi appena faccio un submit la prendo dalla request
			$this->showIdProposta=true;
			$this->mostraFiles=true;
			if (!isset($this->proposta)){
				$this->proposta = $this->get('PropostaFromRequest');
			}
		}

		if(!isset($this->propostaErrors)){
			$this->propostaErrors= new PropostaErrors();
		}
        $this->setSegretarioEditing();
		parent::display ( $tpl );
	}

	function getGruppi(){
		$model=$this->getModel("Proposta");
		return $model->getGruppi();
	}

	function organizzatori($ddgIdx){
	    /**@var $model CAIModelUtente*/
        $model=$this->getModel("Utente");
        $listaUtenti=array();
        $listaUtenti[0]=new stdClass();
        $listaUtenti[0]->id="";
        $listaUtenti[0]->cognome="scegli Direttore di gita #".$ddgIdx;
        $listaUtenti[0]->nome="";
	    if($this->mode=='crea'){
            if($ddgIdx==1 && ! $model->isSegretario()){
                $listaUtenti[0]=$this->get("CurrentUser","Utente");
            }
            else{

                $listaUtenti=array_merge($listaUtenti,$this->get("UtenteList","Utente"));
            }
        }
        else{
            $isOneOfDdg=false;
            $currentDdg=null;
            switch ($ddgIdx){
                case 1:$currentDdg=$this->proposta->ddg1;
                    break;
                case 2:$currentDdg=$this->proposta->ddg2;
                    break;
                case 3:$currentDdg=$this->proposta->ddg3;
                    break;
                case 4:$currentDdg=$this->proposta->ddg4;
                    break;
                case 5:$currentDdg=$this->proposta->ddg5;
                    break;
            }
            $isOneOfDdg=$currentDdg==$this->get("CurrentUser","Utente")->id?true:false;
            if($ddgIdx==1 && ! $model->isSegretario()){
                $listaUtenti[0]=$model->getUserById($currentDdg);
            }
            else if($isOneOfDdg && ! $model->isSegretario()) {
                $listaUtenti[0] = $this->get("CurrentUser", "Utente");
            }
            else{
                $listaUtenti=array_merge($listaUtenti,$this->get("UtenteList","Utente"));
            }
        }



		return $listaUtenti;
	}

	function setTask(){
		$mode=trim(JRequest::getVar("mode","crea"));
		if($mode=="crea"){
			$this->task="proposta.crea";
			$this->showCreaButton=true;
		}
		elseif($mode=="modifica"){
			$this->task="proposta.salvaModifica";
			$this->showSalvaModificheButton=true;
			$this->showRipristinaButton=true;
		}
		elseif($mode=="modificaAdmin"){
			$this->task="proposta.salvaModifica";
			$this->showApprovaButton=true;
			$this->showNonApprovareButton=true;
			$this->showApprovaESalvaModificheButton=true;
			$this->showNonApprovareMaSalvaModificheButton=true;
			$this->showRipristinaAdminButton=true;
		}
		elseif($mode=="modificaSvoltaAdmin"){
			$this->task="proposta.salvaModifica";
			$this->showSalvaModificheButton=true;
			$this->showNonApprovareButton=true;
			$this->showNonApprovareMaSalvaModificheButton=true;
			$this->showRipristinaSvoltaAdminButton=true;
		}
	}

	function getAllegati(){
		$model=$this->getModel("proposta");
		$allegati=$model->getAllegatiById($this->proposta->id);
		return $allegati;
	}

	function getImmagini(){
		$model=$this->getModel("proposta");
		$immagini=$model->getImmaginiById($this->proposta->id);
		return $immagini;
	}

	function getIndiceUltimoDDG(){
		if($this->proposta->ddg5 != '0' && $this->proposta->ddg5 != ''){
			return 5;
		}
		if($this->proposta->ddg4 != '0' && $this->proposta->ddg4 != ''){
			return 4;
		}
		if($this->proposta->ddg3 != '0' && $this->proposta->ddg3 != ''){
			return 3;
		}
		if($this->proposta->ddg2 != '0' && $this->proposta->ddg2 != ''){
			return 2;
		}
		return 1;
	}

	function setSegretarioEditing(){
        $model=$this->getModel("Utente");
        $this->segretarioEditing=$model->isSegretario();
    }

}
