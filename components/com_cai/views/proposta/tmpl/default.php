<?php
defined ( '_JEXEC' ) or die ( 'Restricted access' );


?>
<?php JHtml::_('script', JUri::base() .'media/com_cai/js/calendar/calendar.js', false, true);?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({
		selector: "textarea.editor",
		language_url : "<?php echo JUri::base();?>/media/com_cai/lib/tinyMCE_lang/it.js",
	language : "it",
	plugins: [
	          "advlist autolink link  lists charmap print preview hr anchor  spellchecker",
	          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime  nonbreaking",
	          "save table contextmenu directionality emoticons  paste textcolor"
	    ],
	    toolbar: " insertfile undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect",

	});
</script>


<span class="error"><?php echo $this->propostaErrors->erroreGenerico;?></span>
<form method="post" enctype="multipart/form-data" id="propostaForm" style="width:100%">

	<?php echo JHtml::_( 'form.token' ); ?>
	<?php
	if ($this->showIdProposta) {
		?>
		<input name="idProposta" type="hidden"
		value="<?php echo $this->proposta->id;?>" />
		<?php
	}
	?>
	<input name="task" type="hidden" value="<?php echo $this->task;?>" /> <input
		name="option" type="hidden" value="com_cai" />
	<div class="proposta" style="width: 90%">
		<div
			style="text-align: center; color: #3b5998; font-weight: 800; font-size: x-large; margin-bottom: 10px;">Titolo</div>
		<span class="error"><?php echo $this->propostaErrors->titolo;?></span>
		<div class="editorContainer">
			<textarea name="titolo" placeholder="Titolo"
				style="text-align: center; color: #3b5998; font-weight: 800; font-size: x-large; width: 99%"><?php echo $this->proposta->titolo;?></textarea>
		</div>
		<span class="error"><?php echo $this->propostaErrors->data;?></span>
		<div style="width: 95%">
			<div
				style="display: inline-block; width: 25%; padding-top: 2em; padding-bottom: 2em;">
				<input name="data_inizio"
					value="<?php echo $this->proposta->data_inizio;?>"
					style="width: 99%;" type="text" placeholder="Data Iniziale"
					onclick="Calendar.show(this, '%d/%m/%Y', false)"
					onfocus="Calendar.show(this, '%d/%m/%Y', false)"
					onblur="Calendar.hide()" readonly />
			</div>
			<div style="display: inline-block; width: 49%">
				<span style="vertical-align: middle; padding-left: 3px;"><input
					name="abilita_data_fine"
					<?php if($this->proposta->abilita_data_fine=='on') echo 'checked';?>
					id="manyDays" type="checkbox" onclick="disabilitaDataF()" />
					Attività che si svolge su più giorni</span>
			</div>

			<div
				style="display: inline-block; width: 25%; padding-top: 2em; padding-bottom: 2em; position: relative; float: right;">
				<input name="data_fine"
					value="<?php echo $this->proposta->data_fine;?>" id="dataFinaleId"
					style="width: 98%; align: right;" type="text"
					placeholder="Data Finale"
					onclick="Calendar.show(this, '%d/%m/%Y', false)"
					onfocus="Calendar.show(this, '%d/%m/%Y', false)"
					onblur="Calendar.hide()" readonly />
			</div>

		</div>
		<div class="error"><?php echo $this->propostaErrors->tipologia;?></div>
		<br />
		<div>
			<div style="width: 100%; clear: both; margin-bottom: 10px;">
				<div style="display: inline-block; width: 54%;">
					<div style="display: inline-block; width: 28%;">Tipologia:</div>
					<div style="display: inline-block; width: 68%;">
						<select name="tipologia" style="height: 100%; width: 100%;">
							<option value=""
								<?php if ($this->proposta->tipologia=="NULL" ) echo"selected"; ?>>Scegli
								Tipologia</option>
							<option value="0"
								<?php if ($this->proposta->tipologia=="0" ) echo"selected"; ?>>Nulla</option>
							<option value="1"
								<?php if ($this->proposta->tipologia=="1" ) echo"selected"; ?>>Alpinismo</option>
							<option value="2"
								<?php if ($this->proposta->tipologia=="2" ) echo"selected"; ?>>Alpinismo
								Giovanile</option>
							<option value="3"
								<?php if ($this->proposta->tipologia=="3" ) echo"selected"; ?>>Cultura</option>
							<option value="4"
								<?php if ($this->proposta->tipologia=="4" ) echo"selected"; ?>>Eventi</option>
							<option value="5"
								<?php if ($this->proposta->tipologia=="5" ) echo"selected"; ?>>Escursionismo</option>
							<option value="6"
								<?php if ($this->proposta->tipologia=="6" ) echo"selected"; ?>>Speleologia</option>
							<option value="7"
								<?php if ($this->proposta->tipologia=="7" ) echo"selected"; ?>>Arrampicata</option>
							<option value="8"
								<?php if($this->proposta->tipologia=="8") echo"selected"; ?>>Cicloescursionismo</option>
							<option value="9"
								<?php if($this->proposta->tipologia=="9") echo"selected"; ?>>Sciescursionismo</option>
							<option value="10"
								<?php if($this->proposta->tipologia=="10") echo"selected"; ?>>Scialpinismo</option>
							<option value="11"
								<?php if($this->proposta->tipologia=="11") echo"selected"; ?>>CAI
								Perugia</option>
							<option value="12"
								<?php if($this->proposta->tipologia=="12") echo"selected"; ?>>Torrentismo</option>
						</select>
					</div>
				</div>
				<div
					style="display: inline-block; position: relative; float: right; width: 44%;">
					<div
						style="display: inline-block; position: relative; float: left; width: 25%;">Gruppo:</div>
					<div
						style="display: inline-block; position: relative; float: right; width: 71%;">
						<select name="gruppo" style="height: 100%; width: 100%; position: relative; float: right;">
							<?php
							for($i=0; $i< count($this->gruppi); $i++){
							?>
							<option value="<?php echo $this->gruppi[$i]->id;?>"
								<?php if ( $this->proposta->gruppo==$this->gruppi[$i]->id ) echo"selected"; ?>>
								<?php echo $this->gruppi[$i]->nome;?>
							</option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div>
			<div>Descrizione:</div>
			<div id="descrizioneProposta" class="editorContainer">
				<textarea name="descrizione" class="editor"
					placeholder="Descrizione..." style="width: 100%;" rows="10"><?php echo $this->proposta->descrizione;?></textarea>
			</div>
		</div>

		<br /> <br />
		<!-- ORGANIZZATORI -->
		<div class="error"><?php echo $this->propostaErrors->ddg;?></div>
		<br />
		<div
			style="width: 100%; clear: both; margin-bottom: 10px; overflow: auto;">
			<div
				style="display: inline-block; width: 30%; height: 100%; position: relative; float: left;">
				<span style="vertical-align: middle;">Organizzatori:</span>
			</div>
			<div style="display: inline-block; width: 68%;">
				<div style="width: 100%; position: relative; float: right;"
					id="ddg1">
					<select name="ddg1" style="width: 100%">
										<?php
										$listaUtenti = $this->organizzatori ( 1 );
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;
											?>
											<option value="<?php echo $id; ?>"
							<?php if($this->proposta->ddg1==$id){echo "selected";} ?>> <?php echo"$cognome"." "."$nome"; ?> </option>
										<?php
										}
										?>
					</select>
				</div>
				<div style="width: 100%" id="ddg2">
					<select name="ddg2" style="width: 100%">
										<?php
										$listaUtenti = $this->organizzatori (2);
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;
											$tagId="";
											if($id=="") $tagId="id=\"select2\"";
											?>
											<option value="<?php echo $id; ?>"
							<?php echo $tagId; if($this->proposta->ddg2==$id){echo "selected";} ?>> <?php echo"$cognome"." "."$nome"; ?> </option>
										<?php
										}
										?>
					</select>
				</div>

			<!--	<script>
				function setOptions(str, selectId, optionId)
				{
					var select=document.getElementById(selectId);
					var firstOpt= document.getElementById(optionId);
					if (str.length<3) {
					    select.innerHTML="";
					    select.add(firstOpt);
					    return;
					} else {
					    var xmlhttp=new XMLHttpRequest();
					    xmlhttp.onreadystatechange=function() {
					        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					        	select.innerHTML="";
							    select.add(firstOpt);
					            select.innerHTML= select.innerHTML+xmlhttp.responseText;
					        }
					    }
					    xmlhttp.open("GET","index.php?option=com_cai&task=Ajax.listaUtenti&format=raw&text="+str,true);
					    xmlhttp.send();
					}
				}
	</script>

				<div style="width: 100%" id="ddg2">
					<input type="text" id="ddg2search" name="ddg2search" onkeyup="setOptions(this.value, 'ddg2Select', 'select2')">
					<select name="ddg2" style="width: 100%" id="ddg2Select" >
						<option value="0" id="select2">scegli Direttore di gita #2</option>
					</select>
				</div>
				 -->

				<div style="width: 100%" id="ddg3">
					<select name="ddg3" style="width: 100%">
										<?php
										$listaUtenti = $this->organizzatori (3);
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;
											$tagId="";
											if($id=="") $tagId="id=\"select3\"";
											?>
											<option value="<?php echo $id; ?>"
												<?php echo $tagId; if($this->proposta->ddg3==$id){echo "selected";} ?>> <?php echo"$cognome"." "."$nome"; ?> </option>
										<?php
										}
										?>
					</select>
				</div>
				<div style="width: 100%" id="ddg4">
					<div style="width: 100%" id="ddg2">
						<select name="ddg4" style="width: 100%">
										<?php
										$listaUtenti = $this->organizzatori (4);
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;$tagId="";
											if($id=="") $tagId="id=\"select4\"";
											?>
											<option value="<?php echo $id; ?>"
												<?php echo $tagId; if($this->proposta->ddg4==$id){echo "selected";} ?>> <?php echo"$cognome"." "."$nome"; ?> </option>
										<?php
										}
										?>
					</select>
					</div>
				</div>
				<div style="width: 100%" id="ddg5">
					<div style="width: 100%" id="ddg2">
						<select name="ddg5" style="width: 100%">
										<?php
										$listaUtenti = $this->organizzatori (5);
										for($i = 0; $i < count ( $listaUtenti ); $i ++) {
											$id = $listaUtenti [$i]->id;
											$nome = $listaUtenti [$i]->nome;
											$cognome = $listaUtenti [$i]->cognome;$tagId="";
											if($id=="") $tagId="id=\"select5\"";
											?>
											<option value="<?php echo $id; ?>"
												<?php echo $tagId; if($this->proposta->ddg5==$id){echo "selected";} ?>> <?php echo"$cognome"." "."$nome"; ?> </option>
										<?php
										}
										?>
					</select>
					</div>
				</div>
				<br style="clear: both;" /> <input type="button" name="bottone+"
					value="+" id="bottone+"
					style="margin-bottom: 5px; display: inline-block; width: 20px; height: 20px; border-radius: 50px; background-color: #3b5998; color: white; box-shadow: 0px; border: 0px; font-weight: bold;">
				<input type="button" name="bottone-" value="-" id="bottone-"
					style="margin-bottom: 5px; display: inline-block; width: 20px; height: 20px; border-radius: 50px; background-color: #3b5998; color: white; box-shadow: 0px; border: 0px; font-weight: bold;">
			</div>

		</div>



		<!-- FINE ORGANIZZATORI -->

		<div>
			<div>Contatti:</div>
			<div id="contattiProposta">
				<textarea name="contatti" class="editor" name="contatti"
					placeholder="Contatti degli organizzatori(email, cellulare)..."
					style="height: 100%; width: 100%;" rows="5"><?php echo $this->proposta->contatti;?></textarea>
			</div>
		</div>


		<!-- Insert File -->
		<br /> <span class="error"><?php echo $this->propostaErrors->allegati;?></span>
		<br /> <br /> <span class="error"><?php echo $this->propostaErrors->immagini;?></span>
		<div style="max-width: 100%;">
			<br>
			<div style="display: inline-block;">
				Allegati: <input style="margin-left: 10px;" type="file"
					id="allegati" name="allegati[]" multiple="multiple">
			</div>
			<div
				style="display: inline-block; margin-bottom: 10px; margin-top: 20px;">
				Immagini: <input type="file" id="immagini" name="immagini[]"
					multiple="multiple">
			</div>
			<br> <br>

			<script>
			function hideFile(id) {
				document.getElementById(id).style.display = 'none';
			}
			</script>

				<?php if ($this->mostraFiles){?>
				<div
				style="max-width: 100%; border: 2px solid #3b5998; padding: 5px;">


				<div>File allegati alla proposta:</div>
				<br>
<?php

					$allegati = $this->getAllegati ();
					for($i = 0; $i < count ( $allegati ); $i ++) {
						$id_proposta=$this->proposta->id;
						$nome_allegato=$allegati[$i]->nome_allegato;
						?>
				<div id="<?php echo "a$i"?>"><a style="padding-left: 15px;" target="_blank"
					href="<?php echo "media/com_cai/allegati_proposte/$id_proposta/$nome_allegato";?>"><?php echo $nome_allegato;?></a>
				<a target="_blank" onClick="hideFile(<?php echo "'a$i'"?>)"
					href="<?php echo "index.php/?option=com_cai&task=proposta.cancellaAllegato&id_proposta=$id_proposta&nome_allegato=$nome_allegato";?>">Cancella</a>
					</div>
						<?php
					}
					?>

        <?php
				?><br>
				<div>Immagini allegate alla proposta:</div>
				<br><?php
					$immagini = $this->getImmagini ();
					for($i = 0; $i < count ( $immagini ); $i ++) {
						$id_proposta=$this->proposta->id;
						$nome_immagine=$immagini[$i]->nome_immagine;
					?>
        <div id="<?php echo "i$i"?>"> <a style="padding-left: 15px;" target="_blank"
					href="<?php echo "media/com_cai/immagini_proposte/$id_proposta/$nome_immagine";?>"><?php echo $nome_immagine;?><img
					style="max-width: 100%;"
					src="<?php echo "media/com_cai/immagini_proposte/$id_proposta/$nome_immagine";?>"></a>
				<br> <a target="_blank" onClick="hideFile(<?php echo "'i$i'"?>)"
					href="<?php echo "index.php/?option=com_cai&task=proposta.cancellaImmagine&id_proposta=$id_proposta&nome_immagine=$nome_immagine";?>">Cancella</a>
				</div>
         <?php
				}
				?>
    			<br>
			</div>
				<?php
				}
				?>
			</div>






		<br> <br> <br> <br>


		<div>
			<div>Difficoltà:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="difficolta" placeholder="Difficolta"
					style="height: 100%; width: 100%;" rows="5"><?php echo $this->proposta->difficolta;?></textarea>
			</div>
		</div>

		<div>
			<div>Dislivello:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="dislivello" placeholder="Dislivello"
					style="height: 100%; width: 100%;" rows="5"><?php echo $this->proposta->dislivello;?></textarea>
			</div>
		</div>

		<div>
			<div>Lunghezza Percorso:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="lunghezza" placeholder="Lunghezza Percorso"
					style="height: 100%; width: 100%;" rows="5"><?php echo $this->proposta->lunghezza;?></textarea>
			</div>
		</div>
		<div>
			<div>Durata stimata:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="durata"
					placeholder="Tempo percorso, ora prevista rientro..."
					style="height: 100%; width: 100%;" rows="5"><?php echo $this->proposta->durata;?></textarea>

			</div>
		</div>

		<div>
			<div>Indicazioni degli organizzatori:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="indicazioni"
					placeholder="Raccomandazioni, abbigliamento, materiali, alimentazione, ecc..."
					rows="5" style="width: 100%;"><?php echo $this->proposta->indicazioni;?></textarea>

			</div>
		</div>

		<div>
			<div>Partenza:</div>
			<div style="display: inline-block; width: 100%"
				class="editorContainer">
				<textarea class="editor" name="appuntamento"
					placeholder="ora, luogo, mappa..."
					style="width: 100%; height: 100%;"><?php echo $this->proposta->appuntamento;?></textarea>

			</div>
		</div>

		<br>

		<div>
			<div>Modalità del viaggio:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="modalita" rows="5"
					placeholder="Mezzi propri, pullman ed eventuali modalità..."
					style="width: 100%; height: 100%;"><?php echo $this->proposta->modalita;?></textarea>

			</div>
		</div>
		<div>
			<div>Quota di partecipazione:</div>
			<div style="display: inline-block; width: 100%">
				<textarea name="costo" placeholder="Costo"
					style="height: 100%; width: 100%;"><?php echo $this->proposta->costo;?></textarea>

			</div>
		</div>
		<br>
		<div>
			<div>Approfondimenti:</div>
			<div style="display: inline-block; width: 100%"
				class="editorContainer">
				<textarea class="editor" name="approfondimenti"
					placeholder="Altre informazioni di dettaglio, riferimenti web, bibliografie, cartografia, ecc...."
					style="width: 100%; height: 100%;" rows="5"><?php echo $this->proposta->approfondimenti;?></textarea>

			</div>
		</div>
		<div>
			<br>
			<div>Prenotazioni:</div>
			<div style="display: inline-block; width: 100%"
				class="editorContainer">
				<textarea class="editor" rows="5" name="prenotazione"
					placeholder="Eventuali modalità prenotazione, in particolare per i “NON SOCI”, termine entro cui prenotare..."
					rows="5" style="width: 100%;"><?php echo $this->proposta->prenotazione;?></textarea>

			</div>
		</div>
<!--		--><?php
//		if($this->segretarioEditing){
//		?>
<!--		<div>-->
<!--			<br>-->
<!--			<div>Data pubblicazione homepage:</div>-->
<!--			<div style="display: inline-block; width: 100%"-->
<!--				class="editorContainer">-->
<!--				<input type="text" placeholder="Data pubblicazione homepage"-->
<!--					name="pubblicazioneHomepage"-->
<!--					value="--><?php //echo $this->proposta->pubblicazione_homepage;?><!--"-->
<!--					onclick="Calendar.show(this, '%d/%m/%Y', false)"-->
<!--					onfocus="Calendar.show(this, '%d/%m/%Y', false)"-->
<!--					onblur="Calendar.hide()" readonly />-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		--><?php
//		}
//		?>
	</div>

	<?php if($this->showCreaButton){
		?>
		<input type="submit" value="Crea" name="crea">
		<?php
	}?>
	<?php if($this->showSalvaModificheButton){
		?>
		<input type="submit" value="Salva Modifiche" name="salvaModifiche">
		<?php
	}?>

	<?php if($this->showApprovaESalvaModificheButton){
		?>
		<input type="submit" value="Approva e salva modifiche"
		name="approvaESalvaModifiche">
		<?php
	}?>

	<?php if($this->showApprovaButton){
		?>
		<input type="submit" value="Approva" name="approva">
		<?php
	}?>

	<?php if($this->showNonApprovareMaSalvaModificheButton){
		?>
		<input type="submit" value="Non approvare ma salva modifiche"
		name="nonApprovareMaSalvaModifiche">
		<?php
	}?>

	<?php if($this->showNonApprovareButton){
		?>
		<input type="submit" value="Non approvare" name="nonApprovare">
		<?php
	}?>




	<?php if($this->showRipristinaButton){
		?>
		<input type="submit" value="Visualizza i dati originali"
		name="ripristina">
		<?php
	}?>
	<?php if($this->showRipristinaAdminButton){
		?>
		<input type="submit" value="Visualizza i dati originali"
		name="ripristinaAdmin">
		<?php
	}?>

	<?php if($this->showRipristinaSvoltaAdminButton){
		?>
		<input type="submit" value="Visualizza i dati originali"
		name="ripristinaSvoltaAdmin">
		<?php
	}?>




</form>

<script>
//document.getElementById("dataFinaleId").disabled=true;
//document.getElementById("manyDays").checked==false;
function disabilitaDataF(){

	if(document.getElementById("manyDays").checked==false){
		document.getElementById("dataFinaleId").disabled=true;
		document.getElementById("dataFinaleId").value="";
	}
	else{
		document.getElementById("dataFinaleId").disabled=false;
	}

}
disabilitaDataF();

<?php
$i=$this->getIndiceUltimoDDG();
echo "var indiceDDG=$i";
?>

var pigiato=0;
	document.getElementById("ddg2").style.display= 'none';
	document.getElementById("ddg3").style.display= 'none';
	document.getElementById("ddg4").style.display= 'none';
	document.getElementById("ddg5").style.display= 'none';
	document.getElementById("bottone-").style.display= 'none';
	if(indiceDDG>=2){
		document.getElementById("ddg2").style.display= 'block';
		document.getElementById("bottone-").style.display= 'inline-block';
		pigiato=pigiato+1;
	}
	if(indiceDDG>=3){
		document.getElementById("ddg3").style.display= 'block';
		pigiato=pigiato+1;
		}
	if(indiceDDG>=4){
		document.getElementById("ddg4").style.display= 'block';
		pigiato=pigiato+1;
		}
	if(indiceDDG>=5){
		document.getElementById("ddg5").style.display= 'block';
		document.getElementById("bottone+").style.display='none';
		pigiato=pigiato+1;
		}





    document.getElementById("bottone+").onclick= function(){
		if(pigiato==0){
			document.getElementById("ddg2").style.display= 'block';
			document.getElementById("bottone-").style.display= 'inline-block';
		}
		if(pigiato==1){
			document.getElementById("ddg3").style.display= 'block';

		}
		if(pigiato==2){
			document.getElementById("ddg4").style.display= 'block';

		}
		if(pigiato==3){
			document.getElementById("ddg5").style.display= 'block';
			document.getElementById("bottone+").style.display='none';
		}
		if(pigiato<4){
        	pigiato=pigiato+1;
		}
    };

    document.getElementById("bottone-").onclick= function(){

		if(pigiato==1){
			document.getElementById("ddg2").style.display= 'none';
			document.getElementById("bottone-").style.display= 'none';
			document.getElementById("select2").selected=true;
		}
		if(pigiato==2){
			document.getElementById("ddg3").style.display= 'none';
			document.getElementById("select3").selected=true;

		}
		if(pigiato==3){
			document.getElementById("ddg4").style.display= 'none';
			document.getElementById("select4").selected=true;

		}
		if(pigiato==4){
			document.getElementById("ddg5").style.display= 'none';
			document.getElementById("bottone+").style.display='inline-block';
			document.getElementById("select5").selected=true;
		}
		if(pigiato>0){
        	pigiato=pigiato-1;
		}
    };

    var rhukMain= document.getElementById('maincolumn');
    if(rhukMain !=null){
	    var widthMain = rhukMain.offsetWidth;
	    //var widthMain = document.getElementById("propostaForm").parentNode.offsetWidth;
	    var formWidth  = widthMain;
	    var w= formWidth.toString()+"px";
	    var form= document.getElementById('propostaForm');
	    form.style.width=w;
    }


</script>
