<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class CAIViewAjaxListaUtenti extends JViewLegacy
{
	public $utenti;
    function display($tpl = null)
    {
    	$utenteModel = $this->getModel("utente");
    	$text = JRequest::getVar ( "text", "" );
    	$this->utenti = $utenteModel->getUtentiByText ( $text );
        parent::display($tpl);
    }
}

