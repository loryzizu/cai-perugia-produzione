<?php
/*
 * @version             1.0.0
 * @package             JoomlAir
 * @copyright			Copyright (C) 2015 schefa.com. All rights reserved.
 * @license				Creative Commons Attribution-Share Alike 3.0 Unported
*/

defined('_JEXEC') or die;

?>
<?php
//get template params
$templateparams	= JFactory::getApplication()->getTemplate(true)->params;
$logo			= $templateparams->get("logo");
$sidebarWidth 	= $templateparams->get("sidebarWidth", 20);
	
    if ( strlen ($logo) >= 4 ) {
        $logo_url = JURI::base() . $logo;
    } else {
        $logo_url = JURI::base() . "templates/joomlair/images/logo.jpg";
    }
	
$app 		= JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?php echo $this->language; ?>" />
    
    <title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>

	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/custom.css" type="text/css" />
        
    <style type="text/css">
    	.dq-left{ width:<?php echo $sidebarWidth; ?>%;} 
    	.dq-right{ width:<?php echo (100 - $sidebarWidth); ?>%;} 
		
		#errorboxbody{margin:30px}
		#errorboxbody h2{font-weight:normal;font-size:1.5em}
		#searchbox{background:#eee;padding:10px;margin-top:20px;border:solid 1px #ddd}
		
		.error{margin-top:30%;margin-bottom:30%;}
	</style>
</head>

<body>

<div id="main_top"></div>

<div id="middle">

    <div class="container">
    <div class="background">
    
    	<div id="left_out" class="dq-left">
        
            <div id="logo">
                <div align="center"><a href="<?php echo $this->baseurl; ?>"><img src="<?php echo $logo_url; ?>" /></a></div>
            </div>
            
            <div class="sidebar">
                <?php echo $doc->getBuffer('modules', 'left', array('style' => 'xhtml')); ?>
            </div> 
            
   	    </div>
        
        
    	<div id="right_out" class="dq-right">
            <div id="maincontent">
        
                <div id="content">
                    <div id="component">
                        <div class="error">
                
                <div id="outline">
                <div id="errorboxoutline">
                    <div id="errorboxheader"> <?php echo $this->title; ?></div>
                    <div id="errorboxbody">
                    <p><strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong></p>
                        <ol>
                            <li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
                            <li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
                        </ol>
                    <p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong></p>
        
                        <ul>
                            <li><a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></li>
                            <li><a href="<?php echo $this->baseurl; ?>/index.php?option=com_search" title="<?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></a></li>
        
                        </ul>
                    <div id="techinfo">
                    <p><?php echo $this->error->getMessage(); ?></p>
        
                    <p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
        
                    <p>
                        <?php if ($this->debug) :
                            echo $this->renderBacktrace();
                        endif; ?>
                    </p>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            </div>
            
            </div>
        </div>
    
    </div>
    </div>
    
</div>


<div id="footer">
    <div class="container">
        <span class="sitetitle">&copy; <?php echo JHTML::Date( 'now', 'Y' ); ?> <?php echo $app->getCfg('sitename'); ?></span>
        <span class="footer copyright">
			<?php 
            /*
            * License: 
            * Creative Commons Attribution-Share Alike 3.0 Unported
            *
            * WARNING: You are not allowed to remove the link to the copyright owner.
            */
            ?>
            <br />Design by <a href="http://www.schefa.com">schefa.com</a>
        </span>
    </div>
</div>



</body>
</html>
