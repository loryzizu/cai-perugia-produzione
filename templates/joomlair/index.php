<?php 

/*
 * @version             1.0.0
 * @package             JoomlAir
 * @copyright			Copyright (C) 2015 schefa.com. All rights reserved.
 * @license				Creative Commons Attribution-Share Alike 3.0 Unported
*/

defined( '_JEXEC' ) or die( 'Access to this location is RESTRICTED.' );	

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head>
    
    <jdoc:include type="head" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    
    <?php 
    
    $app 		= JFactory::getApplication();
    $doc		= JFactory::getDocument(); 
    
    $logo			= $this->params->get("logo");
    $sidebarWidth 		= $this->params->get("sidebarWidth", 20);
    $sidebarWidthStyle	= $this->params->get("sidebarWidthStyle", "proz");
  
	if($sidebarWidthStyle == 'proz') {
			
		$doc->addStyleDeclaration('
		 @media screen and (min-width: 768px) {
			 .dq-left{ width:'.$sidebarWidth.'%; }
			 .dq-right{ width:'. (100 - $sidebarWidth) .'%;}
		 }');
		
	} else {
		
		$doc->addStyleDeclaration('
		 @media screen and (min-width: 768px) {
			 .dq-left{ width:'.$sidebarWidth.'px; }
			 .dq-right{ max-width:'. (750 - $sidebarWidth) .'px;}
			 @media screen and (min-width: 992px) { .dq-right{ max-width:'. (970 - $sidebarWidth) .'px;}  }
			 @media screen and (min-width: 1200px) { .dq-right{ max-width:'. (1170 - $sidebarWidth) .'px;}  }
		 }
		');
	}
	
	
    if ( strlen ($logo) >= 4 ) {
        $logo_url = JURI::base() . $logo;
    } else {
        $logo_url = JURI::base() . "templates/joomlair/images/logo.png";
    }
    
    $doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/bootstrap.min.css', $type = 'text/css');
    $doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/template.css', $type = 'text/css');
    $doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/custom.css', $type = 'text/css');
    
    $topmodulewidth="";
    if($this->countModules("position-1")&&$this->countModules("position-2")) {$topmodulewidth="2";}
    if(!$this->countModules("position-1")&&$this->countModules("position-2")) {$topmodulewidth="1";}
    if($this->countModules("position-1")&&!$this->countModules("position-2")) {$topmodulewidth="1";}
    
	$bottommodulewidth = "";
    if($this->countModules("content-bottom-1")&&$this->countModules("content-bottom-2")) {$bottommodulewidth="2";}
    if(!$this->countModules("content-bottom-1")&&$this->countModules("content-bottom-2")) {$bottommodulewidth="1";}
    if($this->countModules("content-bottom-1")&&!$this->countModules("content-bottom-2")) {$bottommodulewidth="1";}
    
	
    $position_width = 0;
    if($this->countModules("position-3")) { $position_width += 1; }
    if($this->countModules("position-4")) { $position_width += 1; }
    if($this->countModules("position-5")) { $position_width += 1; }
    if($this->countModules("position-6")) { $position_width += 1; }
    
    $doc->addScriptDeclaration('function joomlairToggle() {
        var left = document.getElementById("left_out");
        var middle = document.getElementById("middle");
        if ( left.style.display == "block" ) {
            left.style.display = "none";
            middle.className = "";
        } else {
            left.style.display = "block";
            middle.className = "toggle";
        }
    }');
	
    ?>



    <!-- Inserito da Lorenzo Ranucci -->				<script>		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');		  ga('create', 'UA-67558589-1', 'auto');		  ga('send', 'pageview');		</script>

    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/jquery.cookiebar.css" />
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/jquery.cookiebar.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.cookieBar({
            });
        });
    </script>

</head>


<!-- Inserito da Lorenzo Ranucci -->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : 1586498158235465,
            xfbml      : true,
            version    : 'v2.4'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<body>

<div id="main_top"></div>

<div id="middle">
<div class="container">

    <div id="left_out" class="dq-left">
    
        <div id="logo">
            <div align="center"><a href="<?php echo $this->baseurl; ?>"><img src="<?php echo $logo_url; ?>" /></a></div>
        </div>
        
        <div class="sidebar"><jdoc:include type="modules" name="left" style="xhtml" /></div> 
        
    </div>
    
    
    <div id="right_out" class="dq-right">
    
        <div id="header">
        
            <div id="sitename" class="clearfix">
            
            <a id="dq-toggle" onclick="joomlairToggle();" href="#"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
            <?php echo $app->getCfg('sitename'); ?>
            
            </div>
            <div id="navigation"><jdoc:include type="modules" name="mainmenu" /></div>
        </div>
        
        <?php if($this->countModules('banner')) : ?> 
            <div class="banner"><jdoc:include type="modules" name="banner" style="xhtml" /></div>
        <?php endif; ?>
                    
        <?php if($this->countModules('breadcrumbs') or $this->countModules('search')) {	?> 
        <div id="pathway_out" class="clearfix">
            <div id="pathway"><jdoc:include type="modules" name="breadcrumbs" /></div>
            <?php if($this->countModules('search')) { ?>
                <div id="search"><jdoc:include type="modules" name="search" /></div>
            <?php } ?>
        </div>
        <?php } ?>
        
        <div class="row">
            <div class="col-md-<?php if($this->countModules('right')) { echo 9; } else { echo 12; }?>">
                <?php if($this->countModules('position-1 or position-2')) : ?>
                <div class="box_content">
                    <?php if($this->countModules('position-1')) : ?> 
                    <div class="top_module_<?php echo $topmodulewidth; ?>" style="float:left;">
                    <div class="user"><jdoc:include type="modules" name="position-1" style="xhtml" /></div>
                    </div>
                    <?php endif; ?>
                    <?php if($this->countModules('position-2')) : ?> 
                    <div class="top_module_<?php echo $topmodulewidth; ?>" style="float:right;">
                    <div class="user"><jdoc:include type="modules" name="position-2" style="xhtml" /></div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <div id="content">
                    <div id="component" class="clearfix">
                        <jdoc:include type="message" />
                        <jdoc:include type="component" />
                    </div>
                </div>
                <?php if($this->countModules('content-bottom-1 or content-bottom-2')) : ?>
                <div class="box_content">
                    <?php if($this->countModules('content-bottom-1')) : ?> 
                    <div class="top_module_<?php echo $bottommodulewidth; ?>" style="float:left;">
                    <div class="user"><jdoc:include type="modules" name="content-bottom-1" style="xhtml" /></div>
                    </div>
                    <?php endif; ?>
                    <?php if($this->countModules('content-bottom-2')) : ?> 
                    <div class="top_module_<?php echo $bottommodulewidth; ?>" style="float:right;">
                    <div class="user"><jdoc:include type="modules" name="content-bottom-2" style="xhtml" /></div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php if($this->countModules('right')) { ?>
                <div class="col-md-3 content-right">
                    <div class="sidebar"><jdoc:include type="modules" name="right" style="xhtml" /></div>
                </div>
          <?php } ?>
        </div>
        
    </div>
    
</div>
</div>

<?php if($this->countModules('position-3 or position-4 or position-5 or position-6')) { ?>
<div id="bottom">
    <div class="container">
 	 	<div id="userbottom">
            <?php if($this->countModules('position-3')) : ?>
				<div class="user<?php echo $position_width; ?>"><jdoc:include type="modules" name="position-3" style="xhtml" /></div>
            <?php endif; ?>
 			<?php if($this->countModules('position-4')) : ?>
             	<div class="separator"></div>
 				<div class="user<?php echo $position_width; ?>"><jdoc:include type="modules" name="position-4" style="xhtml" /></div>
	 		<?php endif; ?>
     		<?php if($this->countModules('position-5')) : ?>
        		<div class="separator"></div>
           		<div class="user<?php echo $position_width; ?>"><jdoc:include type="modules" name="position-5" style="xhtml" /></div>
			 <?php endif; ?>
            <?php if($this->countModules('position-6')) : ?>
             	<div class="separator"></div>
               	<div class="user<?php echo $position_width; ?>"><jdoc:include type="modules" name="position-6" style="xhtml" /></div>
            <?php endif; ?>
		</div>
    </div>
</div>
<?php } ?>

<div id="footer">
    <div class="container">
        <span class="sitetitle">&copy; <?php echo JHTML::Date( 'now', 'Y' ); ?> <?php echo $app->getCfg('sitename'); ?></span>
        <span class="footer copyright">
			<?php 
            /*
            * License: 
            * Creative Commons Attribution-Share Alike 3.0 Unported
            *
            * WARNING: You are not allowed to remove the link to the copyright owner.
            */
            ?>
            <br />Powered by <a href="http://lorenzoranucci.com" target="_blank">Lorenzo Ranucci</a>
            <br />Design by <a href="http://www.schefa.com">schefa.com</a>
        </span>
    </div>
</div>

</body>
</html>