<?php 
/*
 * @version             1.0.0
 * @package             joomlair
 * @copyright			Copyright (C) 2015 schefa.com. All rights reserved.
 * @license				Creative Commons Attribution-Share Alike 3.0 Unported
*/

defined( '_JEXEC' ) or die( 'Access to this location is RESTRICTED.' );	

	$logo				    			= $this->params->get("logo");
	$width 								= $this->params->get("width");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head> 

	<jdoc:include type="head" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />

</head>
<body>


	<div id="content">
    	<div id="component">
        	<jdoc:include type="message" />
            <jdoc:include type="component" />
		</div>
	</div>
    

</body>
</html>