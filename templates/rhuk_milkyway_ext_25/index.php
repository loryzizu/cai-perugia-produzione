<?php
/**
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined('_JEXEC') or die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/<?php echo $this->params->get('colorVariation'); ?>.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/<?php echo $this->params->get('backgroundVariation'); ?>_bg.css" type="text/css" />
<!--[if lte IE 6]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php if($this->direction == 'rtl') : ?>
	<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_rtl.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		<!-- Inserito da Lorenzo Ranucci -->
		<?php JHtml::_('jquery.framework'); ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/jquery.cookiebar.css" />
		<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/jquery.cookiebar.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery.cookieBar({
				});
			});
		</script>

</head>


<!-- Inserito da Lorenzo Ranucci -->
<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : 1586498158235465,
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
<body id="page_bg" class="color_<?php echo $this->params->get('colorVariation'); ?> bg_<?php echo $this->params->get('backgroundVariation'); ?> width_<?php echo $this->params->get('widthStyle'); ?>">
<a name="up" id="up"></a>
<div class="center" align="center">
	<div id="wrapper">
		<div id="wrapper_r">
			<div id="header">
				<div id="header_l">
					<div id="header_r">
						<div id="logo"></div>
						<jdoc:include type="modules" name="top" />
						<jdoc:include type="modules" name="position-12" />

					</div>
				</div>
			</div>

			<div id="tabarea">
				<div id="tabarea_l">
					<div id="tabarea_r">
						<div id="tabmenu">
						<table cellpadding="0" cellspacing="0" class="pill">
							<tr>
								<td class="pill_l">&nbsp;</td>
								<td class="pill_m">
								<div id="pillmenu">
									<jdoc:include type="modules" name="user3" />
									<jdoc:include type="modules" name="position-1" />
								</div>
								</td>
								<td class="pill_r">&nbsp;</td>
							</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div id="search">
				<jdoc:include type="modules" name="user4" />
				<jdoc:include type="modules" name="position-0" />
			</div>

			<div id="pathway">
				<jdoc:include type="modules" name="breadcrumb" />
				<jdoc:include type="modules" name="position-2" />
			</div>

			<div class="clr"></div>

			<div id="whitebox">
				<div id="whitebox_t">
					<div id="whitebox_tl">
						<div id="whitebox_tr"></div>
					</div>
				</div>

				<div id="whitebox_m">
					<div id="area">
									<jdoc:include type="message" />

						<div id="leftcolumn">
						<?php if ($this->countModules('left')
								or $this->countModules('position-7')
						) : ?>
							<jdoc:include type="modules" name="left" style="rounded" />
							<jdoc:include type="modules" name="position-7" style="rounded" />
						<?php endif; ?>
						</div>

						<?php if ($this->countModules('left')
									or $this->countModules('position-7')
						) : ?>
						<div id="maincolumn">
						<?php else: ?>
						<div id="maincolumn_full">
						<?php endif; ?>
							<?php if ($this->countModules('user1')  or  $this->countModules('user2')
							or ($this->countModules('position-9')  or  $this->countModules('position-10') ) ) : ?>
								<table class="nopad user1user2">
									<tr valign="top">
										<?php if ($this->countModules('user1') or $this->countModules('position-9')) : ?>
											<td>
												<jdoc:include type="modules" name="user1" style="xhtml" />
												<jdoc:include type="modules" name="position-9" style="xhtml" />
											</td>
										<?php endif; ?>
										<?php if ($this->countModules('user1') or $this->countModules('position-9')
										and $this->countModules('user2') or $this->countModules('position-10')) : ?>
											<td class="greyline">&#160;</td>
										<?php endif; ?>
										<?php if ($this->countModules('user2') or $this->countModules('position-10')) : ?>
											<td>
												<jdoc:include type="modules" name="user2" style="xhtml" />
												<jdoc:include type="modules" name="position-10" style="xhtml" />
											</td>
										<?php endif; ?>
									</tr>
								</table>

								<div id="maindivider"></div>
							<?php endif; ?>

							<table class="nopad">
								<tr valign="top">
									<td>
										<jdoc:include type="component" />
										<jdoc:include type="modules" name="footer" style="xhtml"/>
										<jdoc:include type="modules" name="position-5" style="xhtml" />
										<jdoc:include type="modules" name="position-8"  style="xhtml" />
										<jdoc:include type="modules" name="position-11"  style="xhtml" />
										</td>
									<?php if (($this->countModules('right') or
											$this->countModules('position-3') or
											$this->countModules('position-4')
											)
									and JRequest::getCmd('layout') != 'form') : ?>
										<td class="greyline">&nbsp;</td>
										<td width="170">
											<jdoc:include type="modules" name="right" style="xhtml"/>
											<jdoc:include type="modules" name="position-3" style="xhtml"/>
											<jdoc:include type="modules" name="position-4" style="xhtml"/>
											</td>
									<?php endif; ?>
								</tr>
							</table>

						</div>
						<div class="clr"></div>
					</div>
					<div class="clr"></div>
				</div>

				<div id="whitebox_b">
					<div id="whitebox_bl">
						<div id="whitebox_br"></div>
					</div>
				</div>
			</div>

			<div id="footerspacer"></div>
		</div>

		<div id="footer">
			<div id="footer_l">
				<div id="footer_r">
					<p id="syndicate">
						<jdoc:include type="modules" name="syndicate" />
						<jdoc:include type="modules" name="position-14" style="xhtml" />
					</p>
					<p id="power_by">Power by 
						<a href="http://www.joomla.org">Joomla!&#174;</a>
						modificato da <a href="http://www.joomlacsszengarden.com/" target="_blank">tonicopi</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<jdoc:include type="modules" name="debug" />

</body>
</html>
